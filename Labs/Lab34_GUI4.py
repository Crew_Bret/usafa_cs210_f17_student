#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""
import random

__author__ = "Bret Crew"
__instructor__ = "Dr Crew"
__date__ = "16 Nov 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	No online reading in course textbook
-   Refer to the tk.Canvas options here: http://effbot.org/tkinterbook/canvas.htm

Lesson Objectives
-	Be able to draw shapes on a tk.Canvas
-   Be able to manipulate and animate shapes on a tk.Canvas

"""

import tkinter as tk


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0a()
    # exercise0b()
    exercise1()
    # exercise2()
    # exercise3()


def exercise0a():
    """
    Playing with Shapes - This exercise helps you understand how to create a GUI app
    that uses the tk.Canvas widget in order to draw in your window.
    The sequence of steps for creating a GUI app is worth mimicking when creating your own apps.

    Best Practice: The following is a recommended sequence of steps when building a
    GUI application.

        1. Sketch out what you want the GUI to look like.
        2. Define the Model that represents your application.
        3. Create the View and Controller widgets in the GUI.
        4. Stub out callback functions for any Control widgets.
        5. Make connections to monitor changes to the model as needed
           ("trace" in tk vocabulary).
        6. Stub out callback functions for traces.
        7. Set initial values for your model.
        8. Flesh out the functions for complete functionality.

    TODO:  Study the code associated with each step, in sequence.
    """
    print_exercise_name()

    # Create the entire GUI program
    program = SimpleShapesApp()

    # Start the GUI event loop
    program.window.mainloop()


class SimpleShapesApp():
    """
    STEP 1: Sketch out what you want the GUI to look like.
    The app draws a rectangle, a circle, and some text.

            +-----------------------------------------------------------+
            |  Simple Shapes                                [_] [ ] [X] |
            +-----------------------------------------------------------+
            |                          My Art                           |
            |   ______________________________________________________  |
            |  |                                                      | |
            |  |     ____          Hello                              | |
            |  |    |    |                                            | |
            |  |    |____|       _                                    | |
            |  |                (_)                                   | |
            |  |______________________________________________________| |
            +-----------------------------------------------------------+

    TODO:  Study the code associated with each step, in sequence.
    """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Simple Shapes")

        # STEP 2. Define the Model that represents your application.
        # Data (Model)
        # NO STEP 2 IN THIS PROBLEM

        # View / Control
        self.canvas = None  # type: tk.Canvas
        self.create_widgets()

        # STEP 5. Make connections to monitor changes to the model as needed
        #        ("trace" in tk vocabulary).
        # NO STEP 5 IN THIS PROBLEM

        # STEP 7. Set initial values for your model.
        # NO STEP 7 IN THIS PROBLEM

    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.

        # My Art title
        lbl = tk.Label(self.window, text="My Art")
        lbl.pack()

        # Canvas
        self.canvas = tk.Canvas(self.window, bg="white")
        self.canvas.pack(fill=tk.BOTH)
        self.create_canvas()

    def create_canvas(self):
        width = int(self.canvas["width"])  # Yes, it's weird how you retrieve the canvas size
        height = int(self.canvas["height"])

        # Rectangle, somewhere
        x1 = 20
        y1 = 40
        x2 = 50
        y2 = 95
        self.canvas.create_rectangle(x1, y1, x2, y2)

        # Circle, centered
        # Not center and radius; it's two corners of a rectangle
        x1 = width * 0.5 - 30
        y1 = height * 0.5 - 30
        x2 = width * 0.5 + 30
        y2 = height * 0.5 + 30
        self.canvas.create_oval(x1, y1, x2, y2)

        # Text, centered
        x = width * 0.5
        y = height * 0.5
        self.canvas.create_text(x, y, text="Python")

        # Try adding some more shapes
        # http://effbot.org/tkinterbook/canvas.htm

    pass
    # STEP 4. Stub out callback functions for any Control widgets.
    # NO STEP 4 IN THIS PROBLEM

    pass
    # STEP 6. Stub out callback functions for traces.
    # NO STEP 6 IN THIS PROBLEM


def exercise0b():
    """
    Growing Shapes - This exercise helps you understand how to create a GUI app
    that uses the tk.Canvas widget in order to draw in your window.
    The sequence of steps for creating a GUI app is worth mimicking when creating your own apps.

    Best Practice: The following is a recommended sequence of steps when building a
    GUI application.

        1. Sketch out what you want the GUI to look like.
        2. Define the Model that represents your application.
        3. Create the View and Controller widgets in the GUI.
        4. Stub out callback functions for any Control widgets.
        5. Make connections to monitor changes to the model as needed
           ("trace" in tk vocabulary).
        6. Stub out callback functions for traces.
        7. Set initial values for your model.
        8. Flesh out the functions for complete functionality.

    TODO:  Study the code associated with each step, in sequence.
    """
    print_exercise_name()

    # Create the entire GUI program
    program = GrowingShapesApp()

    # Start the GUI event loop
    program.window.mainloop()


class GrowingShapesApp():
    """
    STEP 1: Sketch out what you want the GUI to look like.
    The app draws several randomly-placed squares and circles on a tk.Canvas.
    The size of the squares change as the slider (tk.Scale) is adjusted).
    The artist's name is written on the tk.Canvas in the lower right corner.

            +-----------------------------------------------------------+
            |  Growing Shapes                               [_] [ ] [X] |
            +-----------------------------------------------------------+
            | Artist name:  [ Joe                                     ] |
            |                        15                                 |
            | Square size:  [        |^|                              ] |
            |   ______________________________________________________  |
            |  |                          ____                        | |
            |  |     ____                |    |       _               | |
            |  |    |    |               |____|      (_)              | |
            |  |    |____|       _                                    | |
            |  |                (_)                                   | |
            |  |__________________________________________________Joe_| |
            +-----------------------------------------------------------+

    NOTE: At this point you do not need to have alignment and all the details
    perfect, especially if you are sketching it out in code comment as with
    the above example.  For more complicated apps, you may want to sketch
    out your app in PowerPoint or some other drawing tool.

    """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Growing Shapes")

        # STEP 2. Define the Model that represents your application.
        # Data (Model)
        self.name = tk.StringVar()
        self.square_size = tk.IntVar()

        # View / Control
        self.canvas = None  # type: tk.Canvas
        self.create_widgets()

        # STEP 5. Make connections to monitor changes to the model as needed
        #        ("trace" in tk vocabulary).
        self.name.trace("w", self.name_changed)
        self.square_size.trace("w", self.square_size_changed)

        # STEP 7. Set initial values for your model.
        self.name.set("Your Name")  # Initial value
        self.square_size.set(10)  # Initial value

    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.

        # Name
        lbl_name = tk.Label(self.window, text="Artist name:")
        lbl_name.grid(row=0, column=0, sticky=tk.W)
        txt_name = tk.Entry(self.window, textvariable=self.name)
        txt_name.grid(row=0, column=1, sticky=tk.W + tk.E)
        self.window.grid_columnconfigure(1, weight=1)  # Column 1 will expand more
        # Try commenting out the gid_columnconfigure line to see how that differs

        # Square Size
        lbl_square = tk.Label(self.window, text="Square size:")
        lbl_square.grid(row=1, column=0, sticky=tk.W)
        scale_square = tk.Scale(self.window, orient=tk.HORIZONTAL, variable=self.square_size)
        scale_square.grid(row=1, column=1, sticky=tk.W + tk.E)

        # Canvas
        # We will need to reference the canvas later, so save it in self.canvas.
        # Be sure to go back to your __init__ function and add self.canvas = None.
        self.canvas = tk.Canvas(self.window, bg="white")
        self.canvas.grid(row=2, column=0, columnspan=2, sticky=tk.W + tk.E)
        # The Canvas is complicated enought that we will create it in another function
        # to help keep create_widgets from getting to big to manage.
        self.create_canvas()

    def create_canvas(self):
        width = int(self.canvas["width"])  # We can find the canvas width after it's created,
        height = int(self.canvas["height"])  # or we could have specified it when we created it.

        # Random circles
        radius = 10
        for _ in range(10):
            rand_x = random.randrange(0, width)
            rand_y = random.randrange(0, height)
            self.canvas.create_oval(rand_x, rand_y, rand_x + 2 * radius, rand_y + 2 * radius)

        # Random squares
        size = self.square_size.get()
        for i in range(10):
            rand_x = random.randrange(0, width)
            rand_y = random.randrange(0, height)
            self.canvas.create_rectangle(rand_x, rand_y, rand_x + size, rand_y + size,
                                         fill="blue", outline="green",
                                         tags=("square", "square_{}".format(i)))
            # NOTE: We add a tag, which allows us to grab the squares from the tk.Canvas
            # object later when responding to the model changing (the size of the square).
            # NOTE: We can use the fill and outline parameters to color the squares.
            # NOTE: All the squares can be accessed by the tag "square", and individual
            # squares can be accessed by their unique tags "square_0" "square_1" etc.

        # Artist name in lower right corner
        self.canvas.create_text(width, height - 6, anchor=tk.E,
                                text="TBD", fill="gray", tags=("artist",))
        # NOTE: Adjust the x and y so the text shows up nicely in the corner.
        # Use the anchor parameter to right justify the text.  That means that when we
        # specify an x and a y, we are specifying the righthand edge of the text,
        # centered top to bottom.
        # NOTE: Again we tag this element so we can retrieve it later to change the text.
        # NOTE: The tags= parameter accepts a tuple of tags, and the way you do
        # a tuple of one thing is with a comma and then nothing: ("one",)

    # STEP 4. Stub out callback functions for any Control widgets.
    # NO STEP 4 IN THIS APP
    # This app has no controls that require callbacks.
    # The name and the sliders are bound directly to our model, and they generate
    # change events through their traces.

    # STEP 6. Stub out callback functions for traces.
    # NOTE: When a trace callback function is called, it passes three arguments
    # that do not tend to be of much use.  Feel free to use argument names
    # such as the underscores shown below, to indicate to someone reading your
    # code that you do not care about those parameters.
    def name_changed(self, _, __, ___):
        # print("name_changed")  # Stubbed out initially
        text_id = self.canvas.find_withtag("artist")
        artist_name = self.name.get()
        self.canvas.itemconfig(text_id, text=artist_name)
        # NOTE: Use the tk.Canvas find_withtag function to retrieve a list
        # of items with that tag value.  If there is only one item with that
        # tag, that is OK.
        # NOTE: Use the tk.Canvas itemconfig function to make a modification
        # to an element's parameters.

    def square_size_changed(self, _, __, ___):
        # print("square_size_changed")  # Stubbed out initially
        square_ids = self.canvas.find_withtag("square")
        size = self.square_size.get()
        for id in square_ids:
            # Change Size
            x1, y1, x2, y2 = self.canvas.coords(id)
            self.canvas.coords(id, x1, y1, x1 + size, y1 + size)

            # Change another property, just for kicks
            if size < 50:
                color = "blue"
            else:
                color = "red"
            self.canvas.itemconfig(id, fill=color)


def exercise1():
    """
    Drawing Something on Campus - In this exercise you will draw something on campus.
    You will not have any data to track or events to respond to.  It is just drawing.


            +---------------------------------------+
            |  Campus                   [_] [ ] [X] |
            +---------------------------------------+
            |               My Art                  |
            |     __________________________        |
            |    |  []  []  []  []  []  []  |       |
            |    | Fairchild                |       |
            |    |     ______       _____   |       |
            |    |____|      |_____|     |__|       |
            |   ^^^^^^^|    |^^^^^^^|   |^^^^^^^^   |
            |          /    /       |   |           |
            |         /    /        |   |           |
            |        /    /         |   |           |
            |                                       |
            +---------------------------------------+

    Functionality:
    When the app launches, it draws the building on a tk.Canvas.  You do not need
    a great level of detail, but use several shape types and text.

    In the space "TODO" draw something on campus.  Use several types of shapes and text.
    """
    print_exercise_name()

    # Create the entire GUI program
    program = DrawCampusApp()

    # Start the GUI event loop
    program.window.mainloop()


class DrawCampusApp:
    """ An app that draws something on campus. """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Campus")

        # View / Control
        self.canvas = None  # type: tk.Canvas
        self.create_widgets()

    def create_widgets(self):
        # My Art title
        lbl = tk.Label(self.window, text="My Art")
        lbl.pack()

        # Canvas
        self.canvas = tk.Canvas(self.window, bg="white")
        self.canvas.pack(fill=tk.BOTH)
        self.create_canvas()

    def create_canvas(self):
        # TODO: Complete this function as described
        self.canvas.create_text(10, 10, text="Draw something here", anchor=tk.W)
        self.canvas.create_oval(200, 100, 250, 200)
        self.canvas.create_line(200,150,225,50)
        self.canvas.create_line(250,150, 225, 50)
        self.canvas.create_line(225, 50, 225, 0)
        self.canvas.create_line(200, 150, 200, 350)
        self.canvas.create_line(250, 150, 250, 350)
        self.canvas.create_line(200, 225, 150, 350)
        self.canvas.create_line(250, 225, 300, 350)
        self.canvas.create_line(-100, 400, 200, 225)
        self.canvas.create_line(550, 400, 250, 225)
        self.canvas.create_text(10, 150, text="Crude Sketch: F-16", anchor=tk.W)
        self.canvas.create_text(110, 20, text="And if you drive the Viper\nThen you gotta drink it fast\n'Cause this ain't the time to loiter\nAnd we ain't got the gas.\n--Dos Gringos", anchor= tk.N)




def exercise2():
    """
    Password Strength - In this exercise you will make a graphical app that
    has a field for a user to type in a password and to indicate how strong
    that password is.  The user can also click on a button to generate a
    strong password.

            +---------------------------------------+
            |  Building                 [_] [ ] [X] |
            +---------------------------------------+
            | Password:  [ monkey                 ] |
            | Strength:  =======                    |
            |   +----------------------------+      |
            |   |  Generate Random Password  |      |
            |   +----------------------------+      |
            +---------------------------------------+

    Functionality:
    Your app has a field where a password can be typed in.  Below that field
    is a colored bar that grows and changes color according to how good the
    password is.  There is a button that when clicked generates a good
    random password.

    Remember the steps:

        1. Sketch out what you want the GUI to look like.
        2. Define the Model that represents your application.
        3. Create the View and Controller widgets in the GUI.
        4. Stub out callback functions for any Control widgets.
        5. Make connections to monitor changes to the model as needed
           ("trace" in tk vocabulary).
        6. Stub out callback functions for traces.
        7. Set initial values for your model.
        8. Flesh out the functions for complete functionality.

    a.  In the space "TODO 2a", define the model for your application.
        Use tk.IntVar, tk.StringVar, tk.BooleanVar, etc as needed to aid in
        monitoring changes to the model.

    b.  In the two spaces "TODO 2b", create the GUI widgets that act as the View and
        Controllers for your application, and stub out any callback functions that you
        need to create for the Control widgets (like buttons).

        Hint: You will need to create a small tk.Canvas on which to draw the
        password strength meter.

    c.  In the two spaces "TODO 2c", connect traces to any Model variables that need to
        be monitored, and stub out the related callback functions.

        Note: Use the self.calculate_strength function to calculate the strength
        of the password.

    d.  Finish the application by fleshing out the callback functions.

    e.  In the space "TODO 2e", improve the self.calculate_strength function
        to do a better job estimating password quality.  For ideas, visit
        http://www.passwordmeter.com
    """
    print_exercise_name()

    # Create the entire GUI program
    program = PasswordStrengthApp()

    # Start the GUI event loop
    program.window.mainloop()


class PasswordStrengthApp:
    """ An app that counts up and down. """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Password Strength")

        # TODO 2a: Create the variable(s) that represent the data model
        # Model (Data)
        pass

        # View/Control
        self.strength_canvas = None  # type: tk.Canvas
        self.create_widgets()

        # TODO 2c: Add traces as needed to any model variables, and set initial values
        pass

    def create_widgets(self):
        # TODO 2b: Create the widgets that make up the Views and Controls
        pass

    # TODO 2b: Stub out any callback functions that you may need to create
    pass

    # TODO 2c: Stub out any callback functions you registered with trace()
    pass

    def calculate_strength(self, password):
        """
        Calculates the strength of a password, rating it on a scale of 0 to 100.

        For ideas visit http://www.passwordmeter.com

        :param str password: the password to check
        :return: the strength of the password
        :rtype: int
        """
        strength = 0
        # TODO 1e: Improve this function as described
        strength += len(password) * 3

        return strength


def exercise3():
    """
    Bouncing Balls App - In this exercise you will be building a graphical app that
    has a number of balls that bounce around the screen.

    Here is a sketch of the GUI:

            +------------------------------------+
            |  Bouncing Balls        [_] [ ] [X] |
            +------------------------------------+
            |                        o           |
            |                                    |
            |       o                            |
            |                                    |
            |             o                      |
            |                     o              |
            |       o                            |
            |                                    |
            +------------------------------------+

    Functionality:
    Your app will create between 10 and 20 random balls (circles) each with a
    random radius between 10 and 20 pixels.  Each ball will move in a random
    direction at a random speed of roughly 2 to 8 pixels of movement at each
    iteration.

    In order to simulate motion, you will use a timer that automatically fires
    many times per second

    a.  In the space "TODO 3a", define the model for your application.
        Use tk.IntVar, tk.StringVar, tk.BooleanVar, etc as needed to aid in
        monitoring changes to the model.

    b.  In the two spaces "TODO 3b", create the GUI widgets that act as the View and
        Controllers for your application, and stub out any callback functions that you
        need to create for the Control widgets (like buttons).

    c.  In the two spaces "TODO 3c", connect traces to any Model variables that need to
        be monitored, and stub out the related callback functions.

    d.  Finish the application by fleshing out the callback functions.
    """
    print_exercise_name()

    # Create the entire GUI program
    program = BouncingBallsApp()

    # Start the GUI event loop
    program.window.mainloop()


class BouncingBallsApp:
    """ App whose window grows when the button is clicked. """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Bouncing Balls")

        # TODO 3a: Create the variable(s) that represent the data model
        # Data (Model)
        pass

        # View / Control
        self.canvas = None  # type: tk.Canvas
        self.create_widgets()

        # TODO 3c: Add traces as needed to any model variables, and set initial values
        self.window.after(1, self.move_balls)  # Call self.move_balls 1 millisecond from now
        pass

    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.
        pass

    def move_balls(self):
        pass

        self.window.after(10, self.move_balls)  # Call it again 10 milliseconds from now


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
