#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

import easygui
import random
import turtle
import math

__author__ = "Bret Crew"
__instructor__ = "Dr Bower"
__date__ = "13 Sep 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Review the previous lesson 11 material
Lesson Objectives
-	Reinforce functions, parameters, return values, and selection statements
-	Reinforce definite iteration with the for loop
-	Introduce indefinite iteration with the while loop
"""

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = False  # Set to True for fast, non-animated turtle movement.


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # high_low()
    # turtle_race()
    olympic_rings()


def exercise0():
    """
    Example code.

    Refer to the previous lesson for a review of for loops and while loops.
    """
    print_exercise_name()

    # Count by 3s with for loop
    for i in range(3, 20, 3):
        print(i, " ", end="")
    print()

    # Equivalent count by 3s with while loop
    n = 3
    while n < 20:
        print(n, " ", end="")
        n = n + 3
    print()

    # Ways to iterate over a list
    phonetic = ["alpha", "bravo", "charlie", "delta", "echo"]
    print("Contents of phonetic:", phonetic)
    print()
    for sound in phonetic:  # Variable sound takes on each value in the list
        print(sound)
    print()
    for i in range(len(phonetic)):  # Variable i takes on the position in the list
        sound = phonetic[i]  # Extract a particular sound from the list
        print(sound)
    print()
    i = 0
    while i < len(phonetic):  # Another way of marching through the position in the list
        sound = phonetic[i]
        print(sound)
        i = i + 1
    print()
    while len(phonetic) > 0:
        sound = phonetic.pop(0)  # Actually remove the first item from the list
        print(sound)
    print()
    print("What's left in phonetic:", phonetic)


def high_low():
    """
    High Low – For this exercise you do not need to write an additional function;
    all of the necessary code is to be written directly in the existing high_low
    function, in the location indicated by the comment "TODO 6".

    a.	Add a condition to the while loop so the game stops when the user's
        guess is correct.

    b.	Add a selection statement after the user enters their guess so they are told
        if the guess is too high or too low.

    c.	Add code after the loop to tell the user they have won the game and show
        the secret number.

    d.	Add code to count the number of guesses and show this information to the user.

    e.	Add to the loop condition so the user only has seven guesses to find a value
        in the range [0,100].

    f.	Add code after the loop to tell the user they have won or lost the game
        (be careful to properly handle the situation where the user is correct on
        their last allowed guess).

    g.	Add code to keep track of the two numbers most closely bounding what the user has guessed
        so far, showing this information each time the user is prompted for their next guess.

    h.	A complete interaction with the program is shown below.

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |    Enter a guess between 1 and 100:   |
            |  [ 50                            ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |   Your guess of 50 is too high.       |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |    Enter a guess between 1 and 49:    |
            |  [ 25                            ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |   Your guess of 25 is too low.        |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |    Enter a guess between 26 and 49:   |
            |  [ 37                            ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +-----------------------------------------+
            |                             [_] [ ] [X] |
            +-----------------------------------------+
            |  You win! You guessed 37 in 3 guesses.  |
            |             +------+                    |
            |             |  OK  |                    |
            |             +------+                    |
            +-----------------------------------------+

    """
    print_exercise_name()

    # Create a random number in the range [1,100] for the user to guess.
    secret_number = 37  # random.randint( 1, 100 )
    # For debugging purposes only, it's nice to know the secret.
    print(secret_number, flush=True)  # Add the flush to ensure there's no buffering.
    guess_count = 0
    guess = 0
    high = 100
    low = 0
    # TODO 6: Implement the High Low guessing game as described
    while guess != secret_number:
        guess_count += 1
        guess = easygui.integerbox("Enter a guess between {} and {}".format(high, low), "Input", lowerbound=1,
                                   upperbound=100)
        if guess > secret_number:
            high = guess - 1
            easygui.msgbox("Your guess of {} is too high".format(guess))
        elif guess < secret_number:
            low = guess + 1
            easygui.msgbox("Your guess of {} is too low".format(guess))
        if guess is None or guess == secret_number or guess_count == 7:
            break  # Exit while loop

    if guess == secret_number:
        easygui.msgbox("You win! The answer was {}. It took you {} guesses".format(guess, guess_count))
    else:
        easygui.msgbox("You lose! You must guess the number in 7 or fewer guesses.")


def turtle_race():
    """
    Turtle Race – For this exercise you do not need to write an additional function;
    all of the necessary code is to be written directly in the existing turtle_race
    function, in the location indicated by the comment "TODO 7".

    a.	The following code stores the current location of the turtle named raphael
        in the variables x and y:

            x, y = raphael.position()

        Use this to add code necessary to stop the race when one of the turtles is
        within MARGIN of the right edge.

    b.	Add code after the loop to display the winning turtle's name on the turtle
        graphics screen.

    c.	Add code such that the turtles randomly choose to continue straight (60%),
        turn slightly left (20%), or turn slightly right (20%) each time they move forward.

        i.	Hint 1: A slight turn is perhaps 5 degrees; probably no more than 10 degrees.

        ii.	Hint 2: Can you make use of the random.choice function to implement this?

    d.	Add code to end the race if either turtle goes within MARGIN of the top or
        bottom of the screen; that turtle loses.

    e.	Add code to ensure the turtles never collide; if this happens the race is a tie.

    f.	Add any other code you can think of to make the race more exciting!
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # Rename the artist turtle and move her to the left, above the x-axis.
    flojo = artist  # Flo-Jo, https://en.wikipedia.org/wiki/Florence_Griffith_Joyner
    flojo.shape("turtle")
    flojo.color("blue")  # USA!
    flojo.penup()
    flojo.setposition(-WIDTH // 2 + MARGIN, MARGIN * 2)
    flojo.setheading(0)
    flojo.pendown()

    # Create a new turtle, below the x-axis, to race against the turtle formerly known as artist.
    usain = turtle.Turtle()  # Usain Bolt, https://en.wikipedia.org/wiki/Usain_Bolt
    usain.shape("turtle")
    usain.color("green")  # Jamaica
    usain.penup()
    usain.setposition(-WIDTH // 2 + MARGIN, -MARGIN * 2)
    usain.setheading(0)
    usain.pendown()

    # TODO: Implement the turtle race as described
    usain_x = 0
    flojo_x = 0
    tie = False
    writer.goto(-200, -200)
    disqualify = False
    writer.write("And they're off . . .", align="center", font=("Times", FONT_SIZE, "bold"))
    while usain_x < WIDTH / 2 - 2 * MARGIN and flojo_x < WIDTH / 2 - 2 * MARGIN:
        direction = random.randint(0, 5)
        if direction == 4:
            flojo.right(3)
        if direction == 5:
            flojo.left(3)
        direction = random.randint(0, 5)
        if direction == 4:
            usain.right(3)
        if direction == 5:
            usain.left(3)
        usain.forward(random.randint(MARGIN // 4, MARGIN))
        flojo.forward(random.randint(MARGIN // 4, MARGIN))
        usain_x, usain_y = usain.position()
        flojo_x, flojo_y = flojo.position()
        if usain_y > HEIGHT / 2 or usain_y < -HEIGHT / 2 or flojo_y > HEIGHT / 2 or flojo_y < -HEIGHT / 2:
            break
        if math.fabs(usain_y - flojo_y) <= 10 and math.fabs(usain_x - flojo_x) <= 10:
            tie = True
            break
    writer.clear()
    if usain_x < WIDTH / 2 - 2 * MARGIN and flojo_x < WIDTH / 2 - 2 * MARGIN:
        if flojo_y > HEIGHT / 2 or flojo_y < -HEIGHT / 2:
            loser = "Flojo"
            disqualify = True
        elif usain_y > HEIGHT / 2 or usain_y < -HEIGHT / 2:
            loser = "Usain"
            disqualify = True
    if usain_x > flojo_x:
        winner = "Usain"
    else:
        winner = "Flojo"
    if disqualify:
        writer.write("Oh no! {} loses!".format(loser), font=("Ariel", 15, "normal"))
    elif tie:
        writer.write("It's a tie!", font=("Ariel", 15, "normal"))
    else:
        writer.write("{} Wins!".format(winner), font=("Ariel", 15, "normal"))
    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def olympic_rings():
    """
    You will need to retrieve the Lab12_exercise_3.pdf file for detailed instructions
    and detailed screenshots of the images you will be producing in this exercise.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles
    screen, artist, writer = turtle_setup()

    # TODO 3b
    r = (WIDTH - MARGIN * 4) / 6
    x = -WIDTH / 2 + MARGIN + 2 * r
    # ...
    for color in ["blue", "yellow", "black", "green", "red"]:
        if color == "blue" or color == "black" or color == "red":
            y = (-1 / 3) * r
        else:
            y = -r
        artist.color(color)
        draw_circle(artist, x, y, r)
        x += r + MARGIN/2
    # ...

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# TODO 3a
def draw_circle(ring_boi, x, y, r):
    """

    :param turtle.Turtle ring_boi:
    :param int x:
    :param int y:
    :param int r:
    :return:
    """
    ring_boi.up()
    ring_boi.goto(x-r, y)
    ring_boi.down()
    ring_boi.circle(r)


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
