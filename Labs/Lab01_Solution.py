#!/usr/bin/env python3
"""
CS 210, Introduction to Programming, Lab 01 - Simple Python Data.
"""

__author__ = "Dr Bower"
__instructor__ = "Dr Bower"
__date__ = "10 Aug 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
- Read Lesson 0: Pre-Reading and Lesson 1: Simple Python Data from our online textbook
- Watch the videos embedded in the above reading

Lesson Objectives
- Introduce Python data, variables, statements, and operations

Note: Unless specifically requested, functions in lab exercises do not require docstrings.
"""


def main():
    """Main program to call each individual lab exercise."""

    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise1()
    exercise2()
    exercise3()
    exercise4()


def exercise1():
    """
    Add separate print() statements to display each of the following.
        Your name
        Your hometown
        Your squadron number and nickname
        Your class schedule for the Fall 2017 semester
        Anything else you would like your instructor to know
    """
    print_exercise_name()

    print( "Hello, World!" )


def exercise2():
    """
    In the workspace below, copy and paste the two lines that use an expression
    to calculate and display a new value for the variable answer so the lines
    appear four additional times in the code (a total of six). Modify the four
    new calculations such that each results in the value 42 being assigned to
    answer, but each calculation uses different operators. Specifically, use
    each of the following:
        Multiplication
        Division
        A combination of addition, subtraction, multiplication, and division.
        Modulus
    """
    print_exercise_name()

    # Assign and show the answer.
    answer = 42
    print("The answer to the Ultimate Question is", answer)

    # Assign and show the answer using an expression.
    # Copy/paste four copies of the following two lines.
    answer = 37 + 5
    print("The answer to the Ultimate Question is", answer)

    answer = 6 * 7
    print("The answer to the Ultimate Question is", answer)

    answer = 85 // 2
    print("The answer to the Ultimate Question is", answer)

    answer = 9 * ( 8 + 7 ) // 3 - 3
    print("The answer to the Ultimate Question is", answer)

    answer = 192 % 50
    print("The answer to the Ultimate Question is", answer)


def exercise3():
    """
    In the workspace below, use the formula for the distance between two points
    to calculate the distance between (x1,y1) and (x2,y2).

    Hint: The answer should be 5, but be sure to use the formula and the variables
    x1, y1, x2, and y2, to calculate the answer (Google the formula, if necessary).

    Hint: Python provides the ** operator to perform exponentiation and the square
    root of a value can be calculated by raising the value to the power 0.5.
    """
    print_exercise_name()

    x1, y1 = 37, 42
    x2, y2 = 40, 38

    d = ( ( x1 - x2 ) ** 2 + ( y1 - y2 ) ** 2 ) ** 0.5

    print( "The distance between (x1,y1) and (x2,y2) is", d )


def exercise4():
    """
    In the workspace below, use the input() function to ask the user for their
    name and the year they were born. Then print messages saying how old they
    will be on their birthday in 2017, 2020, and 2025.
    """
    print_exercise_name()

    name = input( "Please enter your name: " )
    year = int( input( "What year were you born? " ) )

    print( name, ", in 2017 you will be", 2017 - year, "years old.")
    print( name, ", in 2020 you will be", 2020 - year, "years old.")
    print( name, ", in 2025 you will be", 2025 - year, "years old.")


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
