#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""
import os
from tkinter import filedialog

__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 33: Model View Controller

Lesson Objectives
-	Identify the model (data) used in a program
-   Manipulate the model through controllers
-   Reflect changes to the model through viewers


"""

import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext  # For use with challenge problem


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    exercise1()
    exercise2()
    challenge()


def exercise0():
    """
    Creating a MadLib app - This exercise helps you understand how to create a GUI app
    by stepping you through the code in the order it was created.  This sequence of
    steps for creating a GUI app is worth mimicking when creating your own apps.

    Best Practice: The following is a recommended sequence of steps when building a
    GUI application.

        1. Sketch out what you want the GUI to look like.
        2. Define the Model that represents your application.
        3. Create the View and Controller widgets in the GUI.
        4. Stub out callback functions for any Control widgets.
        5. Stub out callback functions for changes to model data.
        6. Connect model data to their callback functions
           (called "traces" in Python and Tk).
        7. Set initial values for your model.
        8. Flesh out the functions for complete functionality.

    TODO:  Study the code associated with each step, in sequence.
    """
    print_exercise_name()

    # Create the entire GUI program
    # program = MadLibAppInClass()
    program = MadLibAppCompleted()

    # Start the GUI event loop
    program.window.mainloop()


class MadLibAppInClass:
    """
    STEP 1: Sketch out what you want the GUI to look like.

            +-----------------------------------------------------------+
            |  MadLib                                       [_] [ ] [X] |
            +-----------------------------------------------------------+
            | Noun:       [ banana                   ]                  |
            | Scooby Doo tripped over the banana and tackled the ghost. |
            |  +--------+           +--------+                          |
            |  |  Clear |           |  Quit  |                          |
            |  +--------+           +--------+                          |
            +-----------------------------------------------------------+

    NOTE: At this point you do not need to have alignment and all the details
    perfect, especially if you are sketching it out in code comment as with
    the above example.  For more complicated apps, you may want to sketch
    out your app in PowerPoint or some other drawing tool.

    """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("MadLib")

        # STEP 2: Data (model)
        pass

        # View / Control
        self.create_widgets()

        # STEP 6: Connect model data to their callback functions
        pass

        # STEP 7: Set initial values for your model.
        pass

    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.
        pass

    # STEP 4: Stub out callback functions for control widgets
    pass

    # STEP 5: Stub out callback functions for changes to model data
    pass


class MadLibAppCompleted():
    """
    STEP 1: Sketch out what you want the GUI to look like.

            +-----------------------------------------------------------+
            |  MadLib                                       [_] [ ] [X] |
            +-----------------------------------------------------------+
            | Noun:       [ banana                   ]                  |
            | Scooby Doo tripped over the banana and tackled the ghost. |
            |  +--------+           +--------+                          |
            |  |  Clear |           |  Quit  |                          |
            |  +--------+           +--------+                          |
            +-----------------------------------------------------------+

    NOTE: At this point you do not need to have alignment and all the details
    perfect, especially if you are sketching it out in code comment as with
    the above example.  For more complicated apps, you may want to sketch
    out your app in PowerPoint or some other drawing tool.

    """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("MadLib")

        # STEP 2. Define the Model that represents your application.
        # Data (Model)
        self.madlib_blank = "Scooby Doo tripped over the {} and tackled the ghost."
        self.madlib_full = tk.StringVar()  # Will contain the filled-in madlib
        self.word1 = tk.StringVar()  # Will contain the word the user types in

        # View / Control
        self.create_widgets()

        # STEP 5. Make connections to monitor changes to the model as needed
        #        ("trace" in tk vocabulary).
        self.word1.trace("w", self.word1_changed)

        # STEP 7. Set initial values for your model.
        self.word1.set("NOUN")  # Initial value

    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.

        # User input
        lbl_noun = tk.Label(self.window, text="Noun:")
        lbl_noun.grid(row=0, column=0, sticky=tk.W)
        txt_noun = tk.Entry(self.window, textvariable=self.word1)
        txt_noun.grid(row=0, column=1, sticky=tk.W + tk.E)

        # Completed madlib
        lbl_full = tk.Label(self.window, textvariable=self.madlib_full)
        lbl_full.grid(row=1, column=0, columnspan=2)

        # Buttons
        clear_button = tk.Button(self.window, text="Clear", command=self.clear_button_clicked)
        clear_button.grid(row=3, column=0)
        quit_button = tk.Button(self.window, text="Quit", command=self.window.destroy)
        quit_button.grid(row=3, column=1)
        # NOTE:
        # The destroy function that is built in to the tk.Tk() window quits an application,
        # and is a convenient callback function for a quit button IF you do not want
        # to first prompt the user to save any work, etc.
        # NOTE:
        # No parentheses after the command= callback function.  You are telling the
        # button which function you want to get called sometime in the future, not actually
        # calling the function this very moment.
        # NOTE:
        # These widgets are not saved with something like self.lbl_noun.  These are local
        # variables that disappear after the create_widgets() function ends.  Only save
        # widgets with a self.xxxx if you need to manipulate those widgets somewhere else
        # in your application.

    # STEP 4. Stub out callback functions for any Control widgets.
    # NOTE: Use meaningful function names such as this "clear_button_clicked" that
    # communicate WHAT was clicked and WHEN it was clicked (past tense) in relation
    def clear_button_clicked(self):
        # print("clear_button_clicked")  # Stubbed out initially
        self.word1.set("NOUN")

    # STEP 6. Stub out callback functions for traces.
    # NOTE: When a trace callback function is called, it passes three arguments
    # that do not tend to be of much use.  Feel free to use argument names
    # such as the underscores shown below, to indicate to someone reading your
    # code that you do not care about those parameters.
    def word1_changed(self, _, __, ___):
        # print("word1_changed")  # Stubbed out initially
        word = self.word1.get()  # Retrieve the string value from the tk.StringVar
        madlib_filled = self.madlib_blank.format(word)  # Plug in the new word
        self.madlib_full.set(madlib_filled)  # Set the final text


def exercise1():
    """
    Counter Application - In this exercise you will be building a graphical app that counts
    up and down as a user clicks on add/subtract buttons.  Here is a sketch of the GUI (STEP 1):

            +---------------------------------------+
            |  Count: 0                 [_] [ ] [X] |
            +---------------------------------------+
            |                   0                   |
            |        +----------------------+       |
            |        |  Add one to counter  |       |
            |        +----------------------+       |
            |    +-----------------------------+    |
            |    |  Subtract one from counter  |    |
            |    +-----------------------------+    |
            |               +--------+              |
            |               |  Quit  |              |
            |               +--------+              |
            +---------------------------------------+

    Functionality:
    When the button "Add one to counter" is clicked, the counter should increment by one,
    the label above the button should reflect the new value, and the title bar should
    change to read "Count: x" where x is the new value.  When the button "Subtract one
    from counter" is clicked, the counter should decrement by one, and the same changes
    should be reflected.

    Hint: Although a tk.Label can be automatically bound to a tk.IntVar (thank you, tkinter),
    the title bar cannot be similarly bound.  You will need to update the title bar with
    self.window.title("new title here") when the counter changes.  You can put a trace
    on the model's tk.IntVar to be notified of changes.

    Remember the steps:

        1. Sketch out what you want the GUI to look like.
        2. Define the Model that represents your application.
        3. Create the View and Controller widgets in the GUI.
        4. Stub out callback functions for any Control widgets.
        5. Make connections to monitor changes to the model as needed
           ("trace" in tk vocabulary).
        6. Stub out callback functions for traces.
        7. Set initial values for your model.
        8. Flesh out the functions for complete functionality.

    Complete each of the seven steps in order in the spaces marked below.
    """
    print_exercise_name()

    # Create the entire GUI program
    program = CounterApp()

    # Start the GUI event loop
    program.window.mainloop()


class CounterApp:
    """ An app that counts up and down. """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Counter")

        # STEP 2: Data (model)
        self.counter_var = tk.IntVar()

        # View / Control
        self.create_widgets()

        # STEP 6: Connect model data to their callback functions
        self.counter_var.trace("w", self.counter_changed)

        # STEP 7: Set initial values for your model.
        self.counter_var.set(0)  # Initial value


    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.

        # Make a label that is bound to the counter variable
        my_counter = tk.Label(self.window, textvariable=self.counter_var)
        my_counter.grid(row=0, column=0)

        # Increment
        increment_button = tk.Button(self.window, text="Add 1 to counter", command=self.increment_counter)
        increment_button.grid(row=1, column=0)

        # Decrement
        decrement_button = tk.Button(self.window, text="Subtract 1 from counter", command=self.decrement_counter)
        decrement_button.grid(row=2, column=0)

        # Quit
        quit_button = tk.Button(self.window, text="Quit", command=self.window.destroy)
        quit_button.grid(row=3, column=0)

    # STEP 4: Stub out callback functions for control widgets
    def increment_counter(self):
        # print("increment_counter")  # Stubbed out initially
        num = self.counter_var.get()
        num = num + 1
        self.counter_var.set(num)

    def decrement_counter(self):
        # print("decrement_counter")  # Stubbed out initially
        num = self.counter_var.get()
        num = num - 1
        self.counter_var.set(num)

    # STEP 5: Stub out callback functions for changes to model data
    def counter_changed(self, _, __, ___):
        # print("counter_changed")  # Stubbed out initially
        self.window.title("Count: {}".format(self.counter_var.get()))


def exercise2():
    """
    Grow App - In this exercise you will be building a graphical app that
    expands the window when the grow button is clicked.  The grow effect
    is achieved by changing the padding of the grow button.

    Here is a sketch of the GUI (STEP 1):

            +------------------------------------+
            |  Grow                  [_] [ ] [X] |
            +------------------------------------+
            |    +--------------------------+    |
            |    |  Make the window bigger  |    |
            |    +--------------------------+    |
            |            Padding: 5              |
            |            +--------+              |
            |            |  Quit  |              |
            |            +--------+              |
            +------------------------------------+

    Functionality:
    When the "Make the window bigger" button is clicked, the app should increase the value
    of a model variable representing the desired padding.  When the model variable changes,
    that should cause the app to reassign a padding value to the button.

    Hints:
        1. Model: Use a tk.IntVar to represent the padding value.
        2. View: Let a tk.Label be bound to this value (textvariable=self.xxxx) to reflect
           the current value ("Padding: 5" in the sketch above)
        3. Control: When the grow button is clicked, only modify the model's tk.IntVar.
           Do not make changes to the padding here.
        3. View: Use a trace on the tk.IntVar to notice when the model has changed, and update
           the button padding then.

           Example of how to change padding:
            self.grow_button.grid(pady=10, padx=10)

    Complete each of the seven steps in order in the spaces marked below.
    """
    print_exercise_name()

    # Create the entire GUI program
    program = GrowWindow()

    # Start the GUI event loop
    program.window.mainloop()


class GrowWindow:
    """ App whose window grows when the button is clicked. """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Grow")

        # STEP 2: Data (model)
        self.padding = tk.IntVar()

        # View / Control
        self.grow_button = None  # type: # tk.Button
        self.create_widgets()

        # STEP 6: Connect model data to their callback functions
        self.padding.trace("w", self.padding_changed)

        # STEP 7: Set initial values for your model.
        self.padding.set(5)

    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.

        # Grow button
        self.grow_button = tk.Button(self.window, text="Make the window bigger")
        self.grow_button.grid(row=0, column=0, columnspan=2)
        self.grow_button['command'] = self.grow_button_clicked  # Another way to register a callback

        # Report padding:
        lbl = tk.Label(self.window, text="Padding:")
        lbl.grid(row=1, column=0, sticky=tk.E)
        lbl_padding = tk.Label(self.window, textvariable=self.padding)
        lbl_padding.grid(row=1, column=1, sticky=tk.W)

        # Quit button
        quit_button = tk.Button(self.window, text="Quit")
        quit_button.grid(row=2, column=0, columnspan=2)
        quit_button['command'] = self.window.destroy

    # STEP 4: Stub out callback functions for control widgets
    def grow_button_clicked(self):
        # print("grow_button_clicked")  # Stubbed out initially
        self.padding.set(self.padding.get() + 10)

    # STEP 5: Stub out callback functions for changes to model data
    def padding_changed(self, _, __, ___):
        # print("padding_changed")  # Stubbed out initially
        self.grow_button.grid(pady=self.padding.get(), padx=self.padding.get())


"""
Challenge Exercise

Go back to the MadLib app and upgrade it to be able to read in MadLibs from
your Data folder and support multiple words being entered and the entire
MadLib being displayed.

Hint: For a multiline text box, use the BindableTextArea widget that we
have created below.  It supports multiline text and can be bound to a tk.StringVar.
If you do not want people editing the text, you can disable it like so:

    text_area = BindableTextArea(some_frame, width=40, height=5,
                                 state=tk.DISABLED, textvariable=some_variable)

"""


def challenge():
    print_exercise_name()

    # Create the entire GUI program
    program = MadLibChallengeApp()

    # Start the GUI event loop
    program.window.mainloop()


class MadLibChallengeApp():
    """
    STEP 1: Sketch out what you want the GUI to look like.

            +-----------------------------------------------------------+
            |  MadLib                                       [_] [ ] [X] |
            +-----------------------------------------------------------+
            |   +--------+          +--------+                          |
            |   |  Load  |          |  Quit  |                          |
            |   +--------+          +--------+                          |
            | + Words -----------------------------------------------+  |
            | |  Noun: [        ]                                    |  |
            | |  Verb: [        ]                                    |  |
            | |    ...                                               |  |
            | |                                                      |  |
            | |                                                      |  |
            | +------------------------------------------------------+  |
            | + Story -----------------------------------------------+  |
            | |  Blah blah blah                                      |  |
            | |                                                      |  |
            | |                                                      |  |
            | +------------------------------------------------------+  |
            +-----------------------------------------------------------+

    NOTE: At this point you do not need to have alignment and all the details
    perfect, especially if you are sketching it out in code comment as with
    the above example.  For more complicated apps, you may want to sketch
    out your app in PowerPoint or some other drawing tool.

    """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("MadLib Challenge")

        # STEP 2. Define the Model that represents your application.
        # Data (Model)
        self.madlib_src = tk.StringVar()  # From file with _noun_ or {}
        self.madlib_full = tk.StringVar()  # Will contain the filled-in madlib
        self.words = []  # type: [tk.StringVar]
        self.word_types = []  # type: str

        # View / Control
        self.words_frame = None  # type: tk.Frame
        self.create_widgets()

        self.madlib_src.trace("w", self.madlib_src_changed)

        # STEP 7. Set initial values for your model.
        self.madlib_src.set("Scooby Doo tripped over the _noun_ and tackled the ghost.")  # Initial value
        # self.load_madlib_file("../Data/MadLib_Original.txt")

    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.

        # Buttons
        clear_button = tk.Button(self.window, text="Load", command=self.load_button_clicked)
        clear_button.grid(row=0, column=0, pady=8)
        quit_button = tk.Button(self.window, text="Quit", command=self.window.destroy)
        quit_button.grid(row=0, column=1)

        # Words Tab
        # This part of the gui will be filled in whenever self.madlib_src changes
        self.words_frame = tk.LabelFrame(self.window, text="Words")
        self.words_frame.grid(row=1, column=0, columnspan=2, sticky=tk.N + tk.S + tk.E + tk.W)
        tk.Label(self.words_frame, text="Load a MadLib...").grid(row=0, column=0)

        # Story Tab
        story_frame = tk.LabelFrame(self.window, text="Story")
        story_frame.grid(row=2, column=0, columnspan=2, sticky=tk.N + tk.S + tk.E + tk.W)
        txt = BindableTextArea(story_frame, textvariable=self.madlib_full, state=tk.DISABLED, wrap=tk.WORD)
        txt.pack(fill=tk.BOTH)

    def create_words_tab_gui(self):

        # Remove old widgets
        for child in self.words_frame.winfo_children():
            child.destroy()

        # Make GUI
        cols = 3
        for i in range(len(self.word_types)):
            word_type = self.word_types[i]
            r = i // cols
            c = (i % cols) * 2
            lbl = tk.Label(self.words_frame, text="{}:".format(word_type))
            txt = tk.Entry(self.words_frame, textvariable=self.words[i])
            lbl.grid(row=r, column=c, sticky=tk.E, padx=4)
            txt.grid(row=r, column=c + 1, sticky=tk.W)

    # STEP 4. Stub out callback functions for any Control widgets.
    # NOTE: Use meaningful function names such as this "clear_button_clicked" that
    # communicate WHAT was clicked and WHEN it was clicked (past tense) in relation
    def load_button_clicked(self):
        # print("load_button_clicked")  # Stubbed out initially
        initial_dir = os.path.abspath(os.path.join(os.getcwd(), "../Data"))
        filename = filedialog.askopenfilename(initialdir=initial_dir, title="Load a MadLib file")
        if filename:
            self.load_madlib_file(filename)

    # STEP 6. Stub out callback functions for traces.
    # NOTE: When a trace callback function is called, it passes three arguments
    # that do not tend to be of much use.  Feel free to use argument names
    # such as the underscores shown below, to indicate to someone reading your
    # code that you do not care about those parameters.
    def madlib_src_changed(self, _, __, ___):
        # print("madlib_src_changed")  # Stubbed out initially

        # Extract all word types (parts of speech)
        src = self.madlib_src.get()
        src_words = [w.strip() for w in src.split()]
        word_types = []
        for src_word in src_words:
            if src_word.startswith("_") and src_word.endswith("_"):
                src = src.replace(src_word, "{}", 1)  # Replace _noun_ with {}
                word_types.append(src_word.strip("_"))  # Keep list of word types

        # Update model
        self.madlib_src.set(src)
        self.word_types = word_types
        self.words.clear()
        for _ in self.word_types:
            var = tk.StringVar()
            var.trace("w", self.word_changed)
            self.words.append(var)

        # Recreate GUI
        self.create_words_tab_gui()

        # Fire an update
        self.word_changed()

    def word_changed(self, _=None, __=None, ___=None):
        # print("word_changed")  # Stubbed out initially

        # Replacement words
        words = []
        for i in range(len(self.words)):
            w = self.words[i].get().upper()  # Word user typed in
            if w == "":
                w = "_{}_".format(self.word_types[i])  # If blank show part of speech
            words.append(w)
        src = self.madlib_src.get()  # This is a formattable string
        story = src.format(*words)  # Expand the list as the format arguments
        self.madlib_full.set(story)  # Set the new story text

    def load_madlib_file(self, filename):
        with open(filename, "r") as f:
            data = f.read()
        self.madlib_src.set(data)


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

class BindableTextArea(tk.scrolledtext.ScrolledText):
    """
    A multi-line tk widget that is bindable to a tk.StringVar.

    You will need to import ScrolledText like so:

        from tkinter import scrolledtext
    """

    class _SuspendTrace:
        """ Used internally to suspend a trace during some particular operation. """

        def __init__(self, parent):
            self.__parent = parent  # type: BindableTextArea

        def __enter__(self):
            """ At beginning of operation, stop the trace. """
            if self.__parent._trace_id is not None and self.__parent._textvariable is not None:
                self.__parent._textvariable.trace_vdelete("w", self.__parent._trace_id)

        def __exit__(self, exc_type, exc_val, exc_tb):
            """ At conclusion of operation, resume the trace. """
            self.__parent._trace_id = self.__parent._textvariable.trace("w", self.__parent._variable_value_changed)

    def __init__(self, parent, textvariable: tk.StringVar = None, **kw):
        tk.scrolledtext.ScrolledText.__init__(self, parent, **kw)
        self._textvariable = None  # type: tk.StringVar
        self._trace_id = None
        if textvariable is None:
            self.textvariable = tk.StringVar()
        else:
            self.textvariable = textvariable
        self.bind("<KeyRelease>", self._key_released)

    @property
    def textvariable(self):
        return self._textvariable

    @textvariable.setter
    def textvariable(self, new_var):
        # Delete old trace if we already had a bound textvariable
        if self._trace_id is not None and self._textvariable is not None:
            self._textvariable.trace_vdelete("w", self._trace_id)

        # Set up new textvariable binding
        self._textvariable = new_var
        self._trace_id = self._textvariable.trace("w", self._variable_value_changed)

    def _variable_value_changed(self, _, __, ___):

        # Must be in NORMAL state to respond to delete/insert methods
        prev_state = self["state"]
        self["state"] = tk.NORMAL

        # Replace text
        text = self._textvariable.get()
        self.delete("1.0", tk.END)
        self.insert(tk.END, text)

        # Restore previous state, whatever that was
        self["state"] = prev_state

    def _key_released(self, evt):
        """ When someone types a key, update the bound text variable. """
        text = self.get("1.0", tk.END)
        with BindableTextArea._SuspendTrace(self):  # Suspend trace to avoid infinite recursion
            if self.textvariable:
                self.textvariable.set(text)


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
