#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

__author__ = "Sierra Ernst, Bailey Compton, & Bret Crew"
__instructor__ = "Dr. Bower"
__date__ = "05 Oct 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 20: Dictionaries from our online textbook

Lesson Objectives
-	Use dictionaries for information storage and retrieval.
-	Add, delete, modify, and test dictionary entries.
-	Iterate over dictionary keys, values, and/or items (key/value pairs).

"""

import string
import easygui


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    # exercise2()
    # exercise3()
    exercise4()


def exercise0():
    """
    Dictionary Demo – Work with a partner to read, discuss, and understand the given code.
    If necessary, refer to the dictionary data type and mapping data type pages in the Python
    documentation.  Be sure to ask other classmates and/or your instructor if anything is unclear.

        https://docs.python.org/3/tutorial/datastructures.html#dictionaries
        https://docs.python.org/3/library/stdtypes.html#typesmapping
    """
    print_exercise_name()

    # TODO 0: Read, discuss, and understand the following code.

    # Lists are created with square brackets and can be thought of as
    # "mapping" an index, also called a "key", to a value.
    color_list = ["red", "green", "blue", "cyan", "yellow", "magenta"]
    # Here, the integer 2 is the key and the string "blue" is the value.
    print(color_list[2], end=" ")

    # Dictionaries are created with curly brackets and also map keys to values.
    color_dict = {0: "red", 1: "green", 2: "blue", 3: "cyan", 4: "yellow", 5: "magenta"}
    # Again, the integer 2 is the key and the string "blue" is the value.
    print(color_dict[2], end=" ")

    # The color_dict example illustrates mapping a key to a value, but is not very useful.
    # Consider this example which maps strings to strings.
    house_dict = {"bathroom": "yellow", "bedroom": "cyan", "kitchen": "blue", "yard": "green"}
    # Here, the string "kitchen" is the key and the string "blue" is the value.
    print(house_dict["kitchen"])

    # Dictionaries can be traversed in several ways; first, by keys.
    for key in house_dict.keys():
        print(key, end=" ")
    print()

    # Also by values:
    for value in house_dict.values():
        print(value, end=" ")
    print()

    # And finally by (key, value) pairs:
    for key, value in house_dict.items():
        print("{}:{}".format(key, value), end=" ")
    print()

    # Many familiar operations work with dictionaries:
    print("len( house_dict ) = {}.".format(len(house_dict)))
    # The min and max operations find the min and max key in the dictionary (not value).
    print("min( house_dict ) = {}, max( house_dict ) = {}.".format(
        min(house_dict),
        max(house_dict)))
    # Similarly, the "in" operator works with the dictionary keys, not values.
    print("'kitchen' in house_dict = {}, 'blue' in house_dict = {}".format(
        'kitchen' in house_dict,
        'blue' in house_dict))

    # The get( key ) method works the same as [ key ] if the key is in the dictionary.
    print("house_dict.get( 'kitchen' ) = {}".format(house_dict.get('kitchen')))
    # However, if the key is not in the dictionary, it returns None (whereas [ key ] causes an error).
    print("house_dict.get( 'basement' ) = {}".format(house_dict.get('basement')))
    # The get method also allows specifying a default value if the key is not in the dictionary.
    print("house_dict.get( 'den', 'white' ) = {}".format(house_dict.get('den', 'white')))

    # Finally, key/value pairs can be added and deleted from a dictionary.
    print(house_dict)
    house_dict["garage"] = "red"
    print(house_dict)
    del house_dict['yard']
    print(house_dict)
    print()


def exercise1():
    """
    Load Dictionary – In the space "TODO 1", complete the load_dictionary function such that
    it loads a dictionary from a file.  Specifically, it accepts a string as a parameter that
    is the name of a file where each line in the file contains two strings separated by a colon.
    The first string is a key and the second string is a value.  The function builds and returns
    a dictionary with the key/value pairs.
    """
    print_exercise_name()

    # Test the load_dictionary function with a small dictionary file.
    print(load_dictionary("../Data/House.txt"))

    # Test with a few larger dictionary files when the above test works.
    print(load_dictionary("../Data/Pirate.txt"))
    print(load_dictionary("../Data/Sms.txt"))

    print()


def load_dictionary(filename):
    """
    Load a dictionary from the given file.

    Each line in the given file must contain a key/value pair separated by a colon.

    :param str filename: The file name containing a dictionary.
    :return: A dictionary with key/value pairs from the file.
    :rtype: dict[str, str]
    """
    # Start with an empty dictionary.
    d = {}

    # TODO 1: In the space below, complete the function as described in the lab document.
    with open(filename, 'r') as file:
        for line in file:
            dict_values = line.split(':')
            key_data = dict_values[0]
            value_data = dict_values[1]
            value_data = value_data.strip()
            d[key_data] = value_data

    # Return the entire dictionary.
    return d


def exercise2():
    """
    Translate – In the space "TODO 2", complete the translate function such that it uses a
    dictionary to translate a sentence.  Specifically, it accepts a string and a dictionary
    as parameters.  The dictionary will contain key/value pairs (both strings) which are used
    to translate each word in the sentence.  If a word in the sentence is not in the dictionary,
    it remains unchanged in the translation.  The function returns a string that is the entire
    sentence translated.

    Note: Punctuation should be stripped from each individual word as it is translated, but
    retained in the fully translated sentence.
    """

    print_exercise_name()
    # I don't know right now, ask later okay?
    print(translate("Idk rn, ask l8r k?", load_dictionary("../Data/Sms.txt")))
    # Aye, Captain, th' grog be in th' galley.
    print(translate("Yes, Sir, the rum is in the kitchen.", load_dictionary("../Data/Pirate.txt")))
    print()


def translate(s, d):
    """
    Translates words from the sentence s using the dictionary d.

    Note: Punctuation is stripped from each individual word as it is translated,
    but is retained in the fully translated sentence.

    :param str s: The sentence/string to be translated.
    :param dict[str, str] d: The translation dictionary.
    :return: The translated sentence/string.
    :rtype: str
    """
    # TODO 2: In the space below, complete the function as described
    ban = s.split()
    s = ""
    for word in ban:
        word = word.strip(string.punctuation).lower()
        if word in d:
            s += d[word]
            s += " "
        else:
            s += word
            s += " "

    return s  # Remove the return statement (and this comment) when writing your own code.


def exercise3():
    """
    Word Count – In the space "TODO 3", complete the word_count function such that it
    builds and returns a dictionary with key/value pairs representing word counts in a file.
    Specifically, it accepts a string as a parameter that is the name of a text file.
    The function builds and returns a dictionary with key/value pairs that are of type
    string/integer where the string is a word in the file and the integer is the number
    of times the word occurs in the file.  The words used as keys should be all lowercase
    and should not have any punctuation on the ends of the word (though a contraction such
    as "can't" will have punctuation within the word).
    """
    print_exercise_name()

    # Test the word_count function with a small file.
    print(word_count("../Data/Test.txt"))

    # Test with a few larger files when the above test works.
    # print( word_count( "../Data/Captain.txt" ) )
    # print( word_count( "../Data/Address.txt" ) )
    print()


def word_count(filename):
    """
    Builds a dictionary of word counts in the given file.

    The key/value pairs in the dictionary are string/integer where the string
    is a word in the file and the integer is the number of times the word
    occurs in the file.

    For example, the Test.txt file from the Resources page
    of the course website would produce the following dictionary:
        {'a': 2, 'voice': 1, 'emergency': 1, 'have': 1, 'big': 1, 'is': 2,
        'this': 3, 'would': 1, 'an': 1, 'been': 1, 'only': 1, 'test': 2,
        'you': 1, 'heard': 1, 'actual': 1, 'had': 1}
    Note: The items in a dictionary are unordered, so yours may appear in a different order.

    Note: The words used as keys should be lower-case with no punctuation on the ends
          (though a contraction such as "can't" will have punctuation within the word).

    Note: Do NOT use one of Python's built-in count() methods (either string or list);
          doing so would result in a highly inefficient solution.

    :param str filename: The file name for which the words are to be counted.
    :return: A dictionary of word counts.
    :rtype: dict[str, int]
    """
    # Start with an empty dictionary.
    d = {}

    # TODO 3: In the space below, complete the function as described in the lab document.
    with open(filename, 'r') as file:
        for line in file:
            words = line.split()
            for word in words:
                word = word.strip(string.punctuation).lower()
                if word in d:
                    d[word] += 1
                else:
                    d[word] = 1


    # Return the entire dictionary.
    return d


def exercise4():
    """
    Show Largest – In the space "TODO 4", complete the show_largest function such that it
    shows the specified number of key/value pairs with the largest values.  Specifically,
    it accepts an integer and a dictionary as parameters with the integer indicating how
    many of the largest key/value pairs from the dictionary to show.

    Note: The dictionary contents may be changed while the function executes, but the
    dictionary must be restored to its original state before the function completes.
    """
    print_exercise_name()

    for filename in ["../Data/Test.txt", "../Data/Address.txt"]:
        # Get the word count dictionary.
        d = word_count(filename)

        # Print the file name and then show the 8 most common words in the file.
        print("{}:\n{}".format(filename, "=" * len(filename)))
        show_largest(8, d)

        # Make sure the dictionary was not changed by show_largest.
        if d != word_count(filename):
            print("ERROR: Dictionary changed by show_largest function!")

        print()


def show_largest(n, d):
    """
    Shows the n key/value pairs with the largest values.

    Note: If there is a tie for the nth most common word, arbitrarily choose one.

    Note: The values in the dictionary may be changed while this function executes,
          but the dictionary MUST be returned to its original state upon completion.

    Note: Refer to the following for a simple, concise, "Pythonic" method of finding
    the maximum value in a dictionary:
        http://stackoverflow.com/questions/14091636/get-dict-key-by-max-value

    :param int n: The number of key/value pairs to show.
    :param dict[str, int] d: The dictionary.
    :return: None
    """
    # TODO 4: In the space below, complete the function as described
    use_this_one = d.copy()
    counter = 0
    while counter < n:
        max_key = max(use_this_one, key=use_this_one.get)
        print(max_key, end='\t')
        print(use_this_one[max_key])
        del use_this_one[max_key]
        counter += 1



"""
Challenge Exercises:

1.	Complete unfinished exercises from any previous lab.

2.	Implement a reverse_lookup function that accepts a value and a dictionary as
    parameters and returns a key from the dictionary associated with the value.
    If the value occurs more than once, return the first key encountered.
    Test your function by loading a dictionary using your load_dictionary function.

8.	Use a dictionary to implement a flashcard type application where the user is shown
    a key value from the dictionary and selects the correct value associated with the key.
    Specifically, use an easygui.buttonbox to show the key and four possible values, one of
    which should be correct and three of which should be randomly selected from all dictionary
    values.

    Hint: It may be useful to make a list of the dictionaries values:

	    value_list = list( d.values() )

    and then use random.shuffle to randomly shuffle the list.
"""


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
