#!/usr/bin/env python3
"""
Lab22 Walkthrough: Secure Login System
CS 210, Introduction to Programming

Try to crack your password: https://md5hashing.net/hash/sha256/
"""
import hashlib
import os
import secrets
import binascii
import easygui
import sys

__author__ = "Robert Harder"
__instructor__ = "Lt Col Me"
__date__ = "16 Aug 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

PASSWORD_FILE = "passwords.txt"

# Grab some cryptographically random noise from https://www.grc.com/passwords.htm
SITE_SALT = "2C33671DF4A1F7FD34A31D2EFF92B40A60C43964CD0D9D33FA3C959E556AFC1C"

# An arbitrary, but really large number for use with final version of password hash.
ABSURD_ITERATIONS = 2020000


def main():
    """Main program to demonstrate password hashing."""
    print(__author__, __doc__)

    # Make shortcuts to the function versions we're using as we move through the demo.
    _create_user, _is_valid_user = create_user0, is_valid_user0  # 0: Plaintext password
    # _create_user, _is_valid_user = create_user1, is_valid_user1  # 1: Hashed password
    # _create_user, _is_valid_user = create_user2, is_valid_user2  # 2: Hashed (password + site_salt)
    # _create_user, _is_valid_user = create_user3, is_valid_user3  # 3: Hashed (password + user_salt + site_salt)
    # _create_user, _is_valid_user = create_user4, is_valid_user4  # 4: Key Derivation Functions (time/memory intensive)

    # Prompt the user for what they want to do.
    resp = easygui.buttonbox(msg="What would you like to do?",
                             choices=["Quit", "Create/Update Account", "Login"])

    if resp is None or resp == "Quit":
        print("Goodbye")
    else:
        # Get username and password from the user.
        name = easygui.enterbox(msg="Username")
        password = easygui.enterbox(msg="Password")
        # password = easygui.passwordbox(msg="Password")  # Hides input characters.

        # Create a new account, saving username and password in the passwords file.
        if resp == "Create/Update Account":
            _create_user(name, password)
            easygui.msgbox("Account created/updated.")

        # Login to an account, verifying username and password from passwords file.
        elif resp == "Login":
            if _is_valid_user(name, password):
                easygui.msgbox("Successful login!  Welcome, {}.".format(name))
            else:
                easygui.msgbox("Login failed; invalid credentials.")


def read_password_file():
    """
    Returns a dictionary with user names as keys and a list of
    the remaining fields on that line as the dictionary values.

    Example:
        users = {
            "alice": ["monkey"],
            "jdoe": ["123456"]
        }

    :return: the password data in a dictionary
    :rtype: dict[str, list[str]]
    """
    # Start with an empty dictionary.
    users = {}
    # Make sure the password file exists.
    if os.path.isfile(PASSWORD_FILE):
        # Open the file and read the contents into a list of lines.
        with open(PASSWORD_FILE, "r") as data_file:
            data_lines = data_file.read().splitlines()
        # Outside the above "with" statement (so the password file
        # closes as quickly as possible) process each line of data.
        for line in data_lines:
            # The line will test as false if it is empty.
            if line:
                # Start with an empty list of fields.
                fields = []

                # Split the data line on the tab characters.
                for field in line.split("\t"):
                    # Strip whitespace from the ends of the field and put it in the list.
                    fields.append(field.strip())

                # User name is always first field; remaining fields are data.
                user_name = fields[0]
                user_data = fields[1:]

                # Store the user data with user name as the key.
                users[user_name] = user_data
    else:
        # Display error message on standard error output.
        print("No password file:", PASSWORD_FILE, file=sys.stderr)

    # Return the (possibly empty) dictionary of user data.
    return users


def get_user_data(user_name):
    """
    Retrieves the fields in the password file (not including the name)
    associated with this user or returns None if the user cannot be found.

    :param str user_name: the username to retrieve
    :return: list of fields for the user or None if no such user
    :rtype: list[str]
    """
    users = read_password_file()
    return users.get(user_name)  # Get user data or None if no such user.


def add_user_data(user_name, *user_data):
    """
    Adds/updates the password file with the user_name and user_data.
    Overwrites/updates any existing user.

    :param str user_name: the username to add/update.
    :param str user_data: list of fields for the user
    :return: None
    """
    users = read_password_file()  # Get existing data
    users[user_name] = user_data  # Store user data keyed on username (first field)

    print("Password file updated:")
    with open(PASSWORD_FILE, "w") as data_file:
        for line_name, line_fields in users.items():
            print(line_name, *line_fields, sep="\t", file=data_file)
            print(line_name, *line_fields, sep="\t")
    print()


"""
Version 0:  Plaintext password

Stolen password file means everyone's password is known.
"""


def create_user0(username, password):
    """
    Creates/updates a new user with the given username and password.

    :param str username: the username to add/update
    :param str password: the password to save
    :return: None
    """
    add_user_data(username, password)  # Does nothing to secure the password


def is_valid_user0(username, password):
    """
    Determines if a given username and password are valid.

    :param str username: the username to validate
    :param str password: the password to validate
    :return: true if the username and password are valid; false otherwise.
    :rtype: bool
    """
    fields = get_user_data(username)

    # If user is not in the file, return False
    if fields is None:
        return False
    else:
        # If user is in the file, compare passwords
        password_in_file = fields[0]
        return password_in_file == password


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
