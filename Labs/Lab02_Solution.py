#!/usr/bin/env python3
"""
CS 210, Introduction to Programming, Lab 02 - Hello, Little Turtles!
"""

import turtle

__author__ = "Dr Bower"
__instructor__ = "Dr Bower"
__date__ = "14 Aug 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
- Read Lesson 2: Hello, Little Turtles from our online textbook
- Watch the videos embedded in the above reading

Lesson Objectives
- Introduce Python turtle graphics
- Introduce the for loop

Note: Unless specifically requested, functions in lab exercises do not require docstrings.
"""

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.


def main():
    """Main program to call each individual lab exercise."""

    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    # exercise1()
    # exercise2()
    # exercise3()
    # exercise4()
    # exercise5()
    # exercise6()
    # exercise7()


def exercise0():
    """Example code."""
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Draw Eyes", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Draw dots like eyes
    draw_dot(artist, -50, 0, 100, "white")  # Left eye
    draw_dot(artist, -50, -20, 30, "black")  # Left pupil
    draw_dot(artist, +50, 0, 100, "white")  # Right eye
    draw_dot(artist, +50, -20, 30, "black")  # Right pupil

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def draw_dot(tom, x, y, size, color):
    """
    Use the given turtle to draw a dot at a coordinate.

    Note that the parameter named "tom" does not need to have the same name
    as what is passed in from elsewhere in the program ("artist" in this case).

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int x: The x-coordinate of the center of the dot.
    :param int y: The y-coordinate of the center of the dot.
    :param str color: The color of the dot.
    :param int size: The radius of the dot.
    """
    # Lift the pen and move to the indicated position.
    tom.penup()
    tom.setposition(x, y)
    tom.pendown()

    # Draw the dot.
    tom.dot(size, color)


def exercise1():
    """
    In the workspace below, write a for loop with a print() statement to display
    each individual value in the values list on a separate line, as shown:
    """
    print_exercise_name()

    values = [42, 37, 50, 29, 67]
    for value in values:
        print( value )


def exercise2():
    """
    In the workspace below, re-write your solution to the previous problem such
    that each individual value in the values list is printed on a separate line
    along with its double and its triple, as shown:
    """
    print_exercise_name()

    values = [42, 37, 50, 29, 67]
    for value in values:
        print( value, value * 2, value * 3 )


def exercise3():
    """
    In the workspace below, modify the contents of the my_favorites list to
    contain the names of some of your favorite people. These can be characters
    from a television show, family members, etc. Then write a for loop with a
    print() statement to display a message to each of your favorites.

    For example, with the values in my_favorites shown below, your program might display:
    """
    print_exercise_name()

    my_favorites = ["Fred", "Wilma", "Barney", "Betty"]
    for name in my_favorites:
        print("In my favorite television show,", name, "is one of my favorites.")


def exercise4():
    """
    In the workspace below, draw an octagon.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the turtle named artist to draw an octagon.
    sides = [1, 2, 3, 4, 5, 6, 7, 8]

    for _ in sides:
        artist.forward( 64 )
        artist.left( 45 )

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise5():
    """
    In the workspace below, draw a pentagon, as shown:
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the turtle named artist to draw a pentagon.
    sides = [1, 2, 3, 4, 5]
    for _ in sides:
        artist.forward( 64 )
        artist.left( 72 )

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise6():
    """
    In the workspace below, draw a five-pointed star with strokes through the center
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Turn a bit before starting the loop so the star points up.
    artist.left( 36 )

    # Use the turtle named artist to draw a five-pointed star.
    sides = [1, 2, 3, 4, 5]
    for _ in sides:
        artist.forward( 64 )
        artist.left( 144 )

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise7():
    """
    In the workspace below, draw concentric squares.
    Use the for loop as much as possible.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Set variables for the size and number of squares.
    how_many = int( screen.numinput( "Input", "How many squares? " ) )
    size = int( screen.numinput( "Input", "Size of the inner square? " ) )

    for square in range( how_many ):
        # Move to the lower left corner of the next square.
        artist.penup()
        artist.back( size // 2 )
        artist.left( 90 )
        artist.back( size // 2 )
        artist.right( 90 )
        artist.pendown()

        # Draw a single square, multiplying the square number
        # by the spacing value so each square is bigger.
        for _ in range( 4 ):
            artist.forward( size + square * size )
            artist.left( 90 )

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# _____________________________________________________ #
#  _____          _   _       _     ______    _ _ _     #
# |  __ \        | \ | |     | |   |  ____|  | (_) |    #
# | |  | | ___   |  \| | ___ | |_  | |__   __| |_| |_   #
# | |  | |/ _ \  | . ` |/ _ \| __| |  __| / _` | | __|  #
# | |__| | (_) | | |\  | (_) | |_  | |___| (_| | | |_   #
# |_____/ \___/  |_| \_|\___/_\__| |______\__,_|_|\__|  #
#  / ____|        | |      |  _ \     | |               #
# | |     ___   __| | ___  | |_) | ___| | _____      __ #
# | |    / _ \ / _` |/ _ \ |  _ < / _ \ |/ _ \ \ /\ / / #
# | |___| (_) | (_| |  __/ | |_) |  __/ | (_) \ V  V /  #
#  \_____\___/ \__,_|\___| |____/ \___|_|\___/ \_/\_/   #
# _____________________________________________________ #


def turtle_setup():
    """
    Sets up the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
