#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

import math
import turtle


__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
- Read Lesson 5: Functions from our online textbook
- Watch the video on functions embedded in the above reading

Lesson Objectives
- Introduce functions and parameters
- Reinforce the range function and Python turtle graphics

Note: Unless specifically requested, functions in lab exercises do not require docstrings.
"""

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.


def main():
    """Main program to call each individual lab exercise."""

    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    # exercise2()
    # exercise3()
    exercise4()


def exercise0():
    """Example code."""
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Draw Eyes", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Draw dots like eyes
    draw_dot(artist, -50, 0, 100, "white")  # Left eye
    draw_dot(artist, -50, -20, 30, "black")  # Left pupil
    draw_dot(artist, +50, 0, 100, "white")  # Right eye
    draw_dot(artist, +50, -20, 30, "black")  # Right pupil

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def draw_dot(tom, x, y, size, color):
    """
    Use the given turtle to draw a dot at a coordinate.

    Note that the parameter named "tom" does not need to have the same name
    as what is passed in from elsewhere in the program ("artist" in this case).

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int x: The x-coordinate of the center of the dot.
    :param int y: The y-coordinate of the center of the dot.
    :param int size: The radius of the dot.
    :param str color: The color of the dot.
    """
    # Lift the pen and move to the indicated position.
    tom.penup()
    tom.setposition(x, y)
    tom.pendown()

    # Draw the dot.
    tom.dot(size, color)


def exercise1():
    """
    Use the draw_square function to draw the image shown below,
    which is made entirely of squares.

    a. Only draw the squares; do not write the numbers.

    b. The size of each square relative to the others should be as
       indicated by the numbers.  The small squares with no number are a 1.

    c. The exact placement of the image on the screen does not matter.

    d. The squares of size one do not need to be drawn as they will be
       completed by the drawing of other squares.


    + - - - - - - + - - - - + - - - - - +
    |             |         |           |
    |             |    4    |           |
    |      6      |         |     5     |
    |             + - - - + +           |
    |             |       + + - - - - - +
    + - - - - - + +   3   |             |
    |           + + - - - +             |
    |     5     |         |      6      |
    |           |    4    |             |
    |           |         |             |
    + - - - - - - + - - - - + - - - - - +
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Square of Squares", align="center", font=("Arial", FONT_SIZE, "bold"))

    # TODO 1: Delete the following two lines and draw the specified image using the draw_square function.
    draw_square(artist,-50,-50,50)
    draw_square(artist,0,-50,40)
    draw_square(artist,40,-50,60)
    draw_square(artist,0,-10,10)
    draw_square(artist,-50,0,60)
    draw_square(artist,10,-10,30)
    draw_square(artist,40,10,10)
    draw_square(artist,10,20,40)
    draw_square(artist,50,10,50)

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def draw_square(tom, x, y, size):
    """
    Use the given turtle to draw a square with one corner at coordinate (x,y).

    Note: The orientation of the square is dependent on the heading of the turtle.
    If the turtle's heading is zero, the (x,y) coordinate is the lower-left corner.

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int x: The x-coordinate of one corner of the square.
    :param int y: The y-coordinate of one corner of the square.
    :param int size: The length of one side of the square.
    """
    # Lift the pen and move to the indicated position.
    tom.penup()
    tom.setposition(x, y)
    tom.pendown()

    # Draw the square.
    for _ in range(4):
        tom.forward(size)
        tom.left(90)


def exercise2():
    """
    Use the draw_square function to draw a series of seven squares
    equally spaced across the screen, as shown in the image at right.

    a. The space between squares is the same as the size of a square.

    b. The exact size of the squares does not matter.

    c. The exact placement of the image on the screen does not matter.

    +---+    +---+    +---+    +---+    +---+    +---+    +---+
    |   |    |   |    |   |    |   |    |   |    |   |    |   |
    +---+    +---+    +---+    +---+    +---+    +---+    +---+
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Seven Squares", align="center", font=("Arial", FONT_SIZE, "bold"))

    # TODO 2: Use the draw_square function to draw seven squares equally spaced across the screen.

    draw_square(artist,-240,-20,40)
    draw_square(artist, -160, -20, 40)
    draw_square(artist, -80, -20, 40)
    draw_square(artist, 0, -20, 40)
    draw_square(artist, 80, -20, 40)
    draw_square(artist, 160, -20, 40)
    draw_square(artist, 240, -20, 40)

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise3():
    """
    Create a draw_triangle function and use it to draw a hexagram,
    as shown in this example:
    http://www.wpclipart.com/education/geometry/hexagram.png

    a. Your draw_triangle function should have the same four parameters
       as the draw_square function. (In fact, you may wish to copy and
       paste the draw_square function, but be sure you change the docstring
       comment appropriately!)

    b. The provided code introduces the turtle screen's numinput function
       to obtain user input in a Python turtle graphics program. Read the
       comment carefully to understand the parameters given to the function,
       asking questions if necessary.

    c. You will need to calculate the height of the equilateral triangle in
       order to properly position the two triangles. This can be done using the
       Pythagorean Theorem.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Hexagram", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Use the turtle screen's numinput function to prompt the user for the size of the triangle.
    # The parameters are the dialog title, the message to show the user, the default value (200),
    # the minimum allowed value (30), and the maximum allowed value (500), respectively.
    size = int(screen.numinput("Input", "Enter size of each triangle:", 200, 30, 500))

    # TODO 3b: Use the draw_triangle function to draw the hexagram.
    # Be sure to do TODO 3a first (below)
    distance = (math.sin(60)*(size*2)) #"Magic distance"
    drawTriangle(artist,0,0,size, 120) # first triangle
    artist.right(180) #flip
    drawTriangle(artist,0,distance, size,120) #second triangle

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# TODO 3a: Define a draw_triangle function below this comment, as specified in the lab document.

def drawTriangle(shelly,x, y, length, angle):
    shelly.up()
    shelly.goto(x,y)
    shelly.down()

    for i in range(3):
        shelly.forward(length/2)
        shelly.right(angle)
        shelly.forward(length/2)

def exercise4():
    """
    Use the draw_square and draw_triangle functions to draw a square
    and a triangle evenly spaced and sized on the screen, as shown below:

    +--------------------------------------------+
    | Python Turtle Graphics          _  []  [X] |
    +--------------------------------------------+
    |                                            |
    |   +----------------+           /\          |
    |   |                |          /  \         |
    |   |                |         /    \        |
    |   |                |        /      \       |
    |   |                |       /        \      |
    |   |                |      /          \     |
    |   +----------------+     /____________\    |
    |                                            |
    +--------------------------------------------+

    a. Ensure there are MARGIN number of pixels to the left of the square,
       between the square and the triangle, and to the right of the triangle.

    b. Calculate the size of the square and triangle using the WIDTH and
       MARGIN constants. (Actually use these names in your code; do not write
       the literal values 960 or 32 in your code!)
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # TODO 4: Use the draw_square and draw_triangle functions to draw a square and a triangle
    # TODO 4: evenly spaced on the screen, as described in the lab document.
    MARGIN = HEIGHT/4
    draw_square(artist,(-WIDTH/2)+(MARGIN),(-HEIGHT/2)+(MARGIN),HEIGHT-(2*MARGIN))
    artist.goto((-WIDTH/2)+(MARGIN),(HEIGHT/2)-(MARGIN))
    drawTriangle(artist,2*MARGIN,(HEIGHT/2)-(MARGIN),HEIGHT-(2*MARGIN),120)
    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


"""
Challenge Exercises:

1. Improve your solution to the series of seven squares
   exercise by making them as large as possible based on
   the value of the WIDTH constant (again, actually use
   the WIDTH constant; do not write the literal value 960
   in your code) and space them across the entire screen
   with MARGIN pixels to the left of the first square and
   to the right of the last square.

2. Improve your solution to the hexagram exercise by centering
   the hexagram at the turtle's current position.

3. Add to your solution to the square and triangle exercise
   by adding a second square to the right of the triangle,
   sized and spaced appropriately.
"""


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """
    Sets up the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
