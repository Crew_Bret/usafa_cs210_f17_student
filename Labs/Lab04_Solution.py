#!/usr/bin/env python3
"""
CS 210, Introduction to Programming, Lab 04 - Python Modules.
"""

import math
import random
import turtle

__author__ = "Dr Bower"
__instructor__ = "Dr Bower"
__date__ = "18 Aug 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
- Read Lesson 4: Python Modules from our online textbook
- Watch the videos embedded in the above reading

Lesson Objectives
- Introduce Python modules
- Reinforce Python turtles

Note: Unless specifically requested, functions in lab exercises do not require docstrings.
"""

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.


def main():
    """Main program to call each individual lab exercise."""

    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    # exercise1()
    # exercise2()
    # exercise3()
    # exercise4()
    # exercise5()
    # exercise6()


def exercise0():
    """Example code."""
    print_exercise_name()

    # http://interactivepython.org/runestone/static/cs210_f17/PythonModules/Themathmodule.html
    print( math.pi )
    print( math.e )
    print( math.sqrt( 2.0 ) )
    print( math.sin( math.radians( 90 ) ) )  # sin of 90 degrees

    # http://interactivepython.org/runestone/static/cs210_f17/PythonModules/Therandommodule.html
    print( random.random() )  # random value in range [0, 1)
    print(random.randrange(1, 7))  # random value from [1, 2, 3, 4, 5, 6]
    print( random.random() * 5 )  # random value in range [0, 5)


def exercise1():
    """
    In the workspace below, use the math module to display the following:
        The value of pi.
        The square root of 42 (use the sqrt function).
        The base-2 logarithm of 1000.
        The value of 0, 45, 90, 135, 180, 225, 270, 315, and 360 degrees converted to radians.
            Hint: Use a for loop and the range() function!
        The sin and cos of each of the above values.
    """
    print_exercise_name()

    print( "pi = ", math.pi )
    print( "sqrt( 42 ) = ", math.sqrt( 42 ) )
    print( "log( 1000, 2 ) = ", math.log( 1000, 2 ) )

    print()
    for degrees in range( 0, 361, 45 ):
        rad = math.radians( degrees )
        print( degrees, rad, math.sin( rad ), math.cos( rad ) )


def exercise2():
    """
    In the workspace below, use the random module to display the following
    (using for loops and the range function where appropriate):
        Ten random floating point values in the range [ 0.0, 1.0 ).
        Ten random floating point values in the range [ 1.0, 10.0 ].
        Ten random integer values in the range [ 1, 10 ].
        Ten random integer values in the range [ 1, 30 ] that are multiples of three.
        Ten randomly selected values from the list [ 'red', 'green', 'blue', 'yellow', 'cyan', 'magenta' ].
    """
    print_exercise_name()

    print( "Ten random floating point values in the range [ 0.0, 1.0 )." )
    for _ in range( 10 ):
        print( random.random() )

    print( "Ten random floating point values in the range [ 1.0, 10.0 ]." )
    for _ in range( 10 ):
        print( random.uniform( 1.0, 10.0 ) )

    print( "Ten random integer values in the range [ 1, 10 ]." )
    for _ in range( 10 ):
        print( random.randint( 1, 10 ) )

    print( "Ten random integer values in the range [ 1, 30 ] that are multiples of three." )
    for _ in range( 10 ):
        print( random.randrange( 3, 31, 3 ) )

    print( "Ten randomly selected values from the list [ 'red', 'green', 'blue', 'yellow', 'cyan', 'magenta' ]." )
    for _ in range( 10 ):
        print( random.choice( [ 'red', 'green', 'blue', 'yellow', 'cyan', 'magenta' ] ) )


def exercise3():
    """
    In the workspace below, obtain input from the user for the length of one
    side of a five-pointed star and then draw a five-pointed star with sides
    of the given length inscribed within a circle.
    Note: Ensure the turtle starts and stops exactly in the center of the star.
    In other words, you must calculate the exact placement of the tip of one of
    the star’s points, move the turtle to that point, draw the star and circle,
    and then return the turtle to its original location.
    Hint: Knowing the length of one side is sufficient information to calculate
    the location of the tip of a point.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Let the user specify the length of one side of the star.
    side = int( screen.numinput( "Input", "Length of one side of the star?" ) )

    # Calculate the distance from the turtle to
    # the tip of the top point of the star.
    dist = ( side / 2 ) / math.sin( math.radians( 72 ) )

    # position teh turtle at the top of the top point of the star.
    artist.penup()
    artist.left( 90 )
    artist.forward( dist )
    artist.right( 90 )
    artist.pendown()

    # Turn a bit so the star points up and draw the star.
    artist.right( 72 )
    for _ in range( 5 ):
        artist.forward( side )
        artist.right( 144 )

    # Turn and draw the circle.
    artist.right( 108 )
    artist.circle( dist )

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise4():
    """
    Run the program in the workspace below. Are the results what you expected?
    Discuss with a classmate and add a comment to the end of the code with your thoughts.
    """
    print_exercise_name()

    one_third = 1 / 3
    two_thirds = 1.0 - one_third

    # Should this be True or False?
    print( "1/3 + 1/3 = 2/3 is", one_third + one_third == two_thirds )

    TOLERANCE = 0.0001  # Floats that differ by less than this tolerance are considered equal.
    print( "Difference < tolerance is", math.fabs( (one_third + one_third) - two_thirds ) <= TOLERANCE )


def exercise5():
    """
    Run the program in the workspace below. Are the results what you expected?
    Discuss with a classmate and add a comment to the end of the code with your thoughts.
    """
    print_exercise_name()

    one_tenth = 1 / 10
    total = 0.0
    for index in range( 10 ):
        total += one_tenth

    # Should this be True or False?
    print( "Seems like this should be true, but it is", total == 1.0 )


def exercise6():
    """
    Plotting a sin wave guided lab exercise.

    http://interactivepython.org/runestone/static/cs210_f17/Labs/sinlab.html
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    # Not using turtle_setup so I can do it myself.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")
    # Set the world coordinates to show one period of each wave.
    screen.setworldcoordinates( 0, -1.25, 360, 1.25 )

    # Get two turtles, one for each wave.
    sin_wave = turtle.Turtle()
    cos_wave = turtle.Turtle()

    # Move the turtles to the starting point.
    sin_wave.penup()
    sin_wave.goto( 0, 0 )
    sin_wave.pendown()
    cos_wave.penup()
    cos_wave.goto( 0, 0 )
    cos_wave.pendown()

    for x in range( 0, 361 ):
        y = math.sin( math.radians( x ) )
        sin_wave.goto( x, y )
        y = math.cos( math.radians( x + 90 ) )
        cos_wave.goto( x, y )

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# _____________________________________________________ #
#  _____          _   _       _     ______    _ _ _     #
# |  __ \        | \ | |     | |   |  ____|  | (_) |    #
# | |  | | ___   |  \| | ___ | |_  | |__   __| |_| |_   #
# | |  | |/ _ \  | . ` |/ _ \| __| |  __| / _` | | __|  #
# | |__| | (_) | | |\  | (_) | |_  | |___| (_| | | |_   #
# |_____/ \___/  |_| \_|\___/_\__| |______\__,_|_|\__|  #
#  / ____|        | |      |  _ \     | |               #
# | |     ___   __| | ___  | |_) | ___| | _____      __ #
# | |    / _ \ / _` |/ _ \ |  _ < / _ \ |/ _ \ \ /\ / / #
# | |___| (_) | (_| |  __/ | |_) |  __/ | (_) \ V  V /  #
#  \_____\___/ \__,_|\___| |____/ \___|_|\___/ \_/\_/   #
# _____________________________________________________ #


def turtle_setup():
    """
    Sets up the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
