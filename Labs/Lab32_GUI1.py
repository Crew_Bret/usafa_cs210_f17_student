#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

__author__ = "Bret Crew"
__instructor__ = "Dr Bower"
__date__ = "16 Nov 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 32 on the course site

Lesson Objectives
-   Know about several GUI widget types
-   Be able to lay out GUI widgets

Need help on a widget?  Google it.  You'll probably end up here:

    http://effbot.org/tkinterbook/

"""

import tkinter as tk
from tkinter import ttk


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    exercise1()
    # exercise2()
    # exercise3()


def exercise0():
    """

            +---------------------------------------+
            |  exercise0                [_] [ ] [X] |
            +---------------------------------------+
            | Hello, left.                          |
            |              Hello, center.           |
            |                        Hello, right.  |
            |  [ data entry field here            ] |
            +---------------------------------------+

    Widgets used:
        tk.Label
        tk.Entry

    There are tk.xxx and ttk.xxx versions of most widgets.
    The ttk versions sometimes look more "native" to the
    operating system you are using (Windows, Mac, etc).
    For the most part, you can use either tk.xxx or ttk.xxx.

    """
    print_exercise_name()

    program = Exercise0App()
    program.window.mainloop()


class Exercise0App:
    """A basic hello world app."""

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("exercise0")
        self.create_widgets()

    def create_widgets(self):
        """Create the GUI widgets."""

        # Hello labels
        hello_left_label = tk.Label(self.window, text="Hello, left.")
        hello_left_label.grid(row=1, column=1, sticky=tk.W)

        hello_center_label = tk.Label(self.window, text="Hello, center.")
        hello_center_label.grid(row=2, column=1, sticky=tk.W + tk.E)

        hello_right_label = tk.Label(self.window, text="Hello, right.")
        hello_right_label.grid(row=3, column=1, sticky=tk.E)

        # Data entry
        data_entry = tk.Entry(self.window)
        data_entry.grid(row=4, column=1, sticky=tk.E + tk.W)

        # Give "weight" to column 1 so
        # that it resizes when the window resizes.
        self.window.columnconfigure(1, weight=1)


def exercise1():
    """
    In the space marked "TODO 1" create the gui widgets to layout the design below.
    Have the text entry fields resize with the window (use weights).

            +---------------------------------------+
            |  exercise1                [_] [ ] [X] |
            +---------------------------------------+
            | First name: [                       ] |
            +---------------------------------------+

    Widgets used:
        tk.Label
        tk.Entry

    """
    print_exercise_name()

    program = Exercise1App()
    program.window.mainloop()


class Exercise1App:
    """An app with one label and one text entry."""

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("exercise1")
        self.create_widgets()

    def create_widgets(self):
        """Create the GUI widgets."""

        # TODO 1
        first_name_lbl = tk.Label(self.window, text="First Name:")
        first_name_lbl.grid(row=1, column=1, sticky=tk.W)

        first_name_entry = tk.Entry(self.window)
        first_name_entry.grid(row=1, column=2, sticky=tk.W + tk.E)


def exercise2():
    """
    In the space marked "TODO 2" create the gui widgets to layout the design below.

            +---------------------------------------+
            |  exercise2                [_] [ ] [X] |
            +---------------------------------------+
            | First name: [                       ] |
            | Last name:  [                       ] |
            |  Address:   [                       ] |
            | +----------+   +---------+            |
            | |  Cancel  |   |  Apply  |            |
            | +----------+   +---------+            |
            +---------------------------------------+

    Widgets used:
        tk.Label
        tk.Entry
        tk.Button

    """
    print_exercise_name()

    program = Exercise2App()
    program.window.mainloop()


class Exercise2App:
    """An app with labels, text entry, and buttons."""

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("exercise2")
        self.create_widgets()

    def create_widgets(self):
        """Create the GUI widgets."""

        # TODO 2


def exercise3():
    """
    You can have subframes as well that have their own widgets within them.
    The subframes are placed on their own and then widgets can be placed within those.
    Be generous with your use of frames to organize your GUI.

    In the space marked "TODO 3" create the layout shown below.  Decide which widgets
    you want to stretch and grow with window resizing.

    Use three subframes to contain the personal data, other information, and the buttons.
    The three frames will be on the same row but different columns.

    Within each subframe, the row and column count reset.  That is within a subframe, the
    upper left widget is still row=1, column=1.

        +---------------------------------------------------------------------------------+
        |  exercise3                                                          [_] [ ] [X] |
        +---------------------------------------------------------------------------------+
        |                                                                                 |
        |  +-- Personal Data ----------------+                               +----------+ |
        |  | First name: [                 ] |   +-- Other Information --+   |  Cancel  | |
        |                                    |   |       0               |   +----------+ |
        |  | Last name:  [                 ] |   | Age [<|>         ]    |                |
        |                                    |   | [x] I like Python     |   +---------+  |
        |  | Address:    [                 ] |   +-----------------------+   |  Apply  |  |
        |  +---------------------------------+                               +---------+  |
        +---------------------------------------------------------------------------------+

    Widgets used:
        tk.Label
        tk.Entry
        tk.Button
        tk.LabelFrame
        tk.Frame
        tk.Checkbutton
        tk.Scale  (the sliding age indicator)


    """
    print_exercise_name()

    program = Exercise3App()
    program.window.mainloop()


class Exercise3App:
    """An app that shows off widgets and layouts."""

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("exercise3")
        self.create_widgets()

    def create_widgets(self):
        """Create the GUI widgets."""

        # Personal data frame
        left_frame = tk.LabelFrame(self.window, text="Personal Data")
        left_frame.grid(row=1, column=1, pady=10, padx=10, sticky=tk.E + tk.W)
        self.window.columnconfigure(1, weight=2)
        left_frame.columnconfigure(2, weight=1)

        # First name
        first_name_label = ttk.Label(left_frame, text="First Name:")
        first_name_label.grid(row=1, column=1, pady=6)
        first_name_entry = ttk.Entry(left_frame)
        first_name_entry.grid(row=1, column=2, pady=6, padx=3, sticky=tk.W + tk.E)

        # TODO 3


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
