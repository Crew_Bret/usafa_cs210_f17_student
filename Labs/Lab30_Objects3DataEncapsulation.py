#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""
import sys

__author__ = "Bret Crew"
__instructor__ = "Dr Bower"
__date__ = "03 nov 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-   Read previous lessons on Objects

Lesson Objectives
-   Encapsulate attributes using Python's @property and @xxxx.setter decorators
-   Enhance classes by overriding the __str__ method
-   Write and use additional class methods to manipulate an object



- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                Data Encapsulation - The Theory

There was no textbook reading for this lesson. Our goal for this lab
time is to learn about data encapsulation inside an object. Encapsulation
protects the data inside of an object. Why??? Two main reasons:

  1) Avoid errors:
     We would like an object's "state" to always be consistent and
     never allow invalid manipulation of an object.

  2) Extend functionality, fix bugs, increase efficiency:
     We would like the "idea an object represents" and the internal
     data representation used to store the object's "state" to be
     separate things. If an object's interface (it's methods) is
     separated from an object's attribute values, it might be
     possible in the future to make an object's code more efficient
     and/or add functionality to the object without breaking any
     existing code that already uses the object.

     One of the important ideas behind object-oriented programming is
     object re-use. That means we need a way to enhance and modify
     object definitions (Classes) over time without breaking the code
     that already uses the objects.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

          Data Encapsulation - Python Programming Syntax

Note 1:
Python allows all attributes of an object to be accessed using "dotted
notation." You can't hide object attributes from the rest of the program.

Note 2:
If a variable starts with two underscores, it cannot be accessed outside
the class code itself.  This protects a class from programmers impropertly
manipulating data within an object.

Summary:

Class Foo:
    def __init__(self):  # never call, it is called automatically by Python
        self.x = 5       # "public" variable
        self.__y = 5     # "private" variable

    # Naming convention also applies to methods
    def task1(self):     # a "public" method
        pass

    def __task2(self):   # a "private" method
        pass

"""


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    # exercise2()
    # exercise3()
    # exercise4()
    exercise5()
    # exercise6()


def exercise0():
    """
    The Person class encapsulates two attributes of a person: name and age.
    The values of these attributes can be accessed normally:

    joe = Person("Joe")
    print("I know someone named", joe.name)
    if joe.age < 18:
        print("He is too young for the Academy.")

    Look at how the age attribute is being stored and accessed.  You cannot create
    a person and then mess up by assigning a fake age.  All you can do is call
    have_birthday() to increment their age.

    Data encapsulation can protect data in an object from being changed in an invalid way.

    """
    print_exercise_name()

    carl = Person1("Carl")
    print("Look who was just born:", carl)
    for _ in range(5):
        carl.have_birthday()
    print("Look who's a big boy now:", carl)
    carl.age = 21  # Unrealistic to suddenly change age like this.

    david = Person2("David")
    print("Look who was just born:", david)
    david.age = 21  # Doesn't do what you think!
    david.__age = 21  # Doesn't do what you think!
    print("David is not 21: ", david)


class Person1:
    def __init__(self, name):
        self.name = name
        self.age = 0  # Just born!

    def __str__(self):
        """ Return a string representation of a person. """
        return "Name: {}, Age: {}".format(self.name, self.age)

    def have_birthday(self):
        """ Increments person's age by one year. """
        self.age += 1


class Person2:
    def __init__(self, name):
        self.name = name
        self.__age = 0  # Just born!

    def __str__(self):
        """ Return a string representation of a person. """
        return "Name: {}, Age: {}".format(self.name, self.age)

    @property
    def age(self):
        """
        provides read-access to the age property.
        :return: person's age
        :rtype: int
        """
        return self.__age

    def have_birthday(self):
        """ Increments person's age by one year. """
        self.__age += 1


def exercise1():
    """
    Improper manipulation of Car class - When classes make their data elements exposed,
    they can be manipulated in nonsensical ways.

    Work with a partner to read, discuss, understand and modify the given code.
    Be sure to ask other classmates and/or your instructor if anything is unclear.

    Note: If you provide a __str__ method, you can return a string that Python will use
    when other functions like print try to represent your class as a string.

    a.  In the space "TODO 1a", what is wrong with setting the speed to 10?

    b.  In the space "TODO 1b", what is wrong with putting the car in Park?

    c.  In the space "TODO 1c", add a series of commands that manipulate the car
        in an invalid or nonsensical fashion.
    """
    print_exercise_name()

    car = Car1()
    print(car)
    print()

    # We can manipulate the car in a well-behaved fashion
    print("Good: Put it in drive and accelerate:")
    car.reset()
    car.gear = "D"
    print(car)
    car.speed = 5
    print(car)
    print()

    # TODO 1a: Read, discuss, and understand the following code.
    # We can set the car to some speed even in park.  Astounding!
    print("Bad: Move while stopped:")
    car.reset()
    car.speed = 10
    print(car)
    print()

    # TODO 1b: Read, discuss, and understand the following code.
    # We can put the car in park when it's driving fast!
    print("Bad: Stop while moving:")
    car.reset()
    car.gear = "D"
    car.speed = "60"
    print(car)
    car.gear = "P"
    print(car)
    print()

    # TODO 1c: Write code to use the car class as described
    print("All kinds of bad...")
    car.reset()
    car.gear = "Q"
    car.speed = "10000"
    print(car)
    car.gear = "Extended"
    car.speed = "Takeoff Speed"
    print(car)


class Car1:
    def __init__(self):
        self.gear = "P"
        self.speed = 0

    def __str__(self):
        return "Car: gear={}, speed={}".format(self.gear, self.speed)

    def reset(self):
        """ Resets the car to parked and zero speed. """
        self.gear = "P"
        self.speed = 0


def exercise2():
    """
    Invalid access to Car data - A class can make its attributes protected from
    outside interference.  In Python this is done with the @property decorator.

    You accomplish this transformation in two steps.  First you change the actual
    variable to begin with an underscore.  For example self.gear would become
    self._gear.

    Now the gear attribute is protected.  Second you create a function
    that has the same name as the attribute, and you add @property on the line
    above the function definition.

    This lets you get (read) an attribute, but you still cannot set it.  That's OK for now.

    Work with a partner to read, discuss, and understand the given code.
    Be sure to ask other classmates and/or your instructor if anything is unclear.

    a.  In the space "TODO 2a", discuss what is wrong with setting the gear to "D"?

    b.  In the space "TODO 2b", write code that uses car.gear without generating an error.
    """
    print_exercise_name()

    car = Car2()
    print("Which gear are you in? Answer: {}".format(car.gear))
    print("How fast are you going? Answer: {}".format(car.speed))

    # TODO 2a: Read, discuss, and understand the following code.
    print("Let's go into drive:")
    # car.gear = "D"  # Will cause program to crash.  Why?  Comment out this line when you're done

    # TODO 2b: Write code to use car.gear as described
    print(car.gear)


class Car2:
    def __init__(self):
        self.__gear = "P"  # Double underscore protects value from outside manipulation
        self.__speed = 0  # Double underscore protects value from outside manipulation

    def __str__(self):
        return "Car: gear={}, speed={}".format(self.gear, self.speed)

    @property
    def gear(self):
        """
        Provides read-access to the gear property.
        :return: car's gear
        :rtype: str
        """
        return self.__gear

    @property
    def speed(self):
        """
        Provides read-access to the speed property.
        :return: car's speed
        :rtype: int
        """
        return self.__speed

    def reset(self):
        """ Resets the car to parked and zero speed. """
        self.__gear = "P"
        self.__speed = 0


def exercise3():
    """
    Proper access to Car data - A class can also set special functions for
    setting its attribute values and thereby protect itself from nonsensical changes.
    In Python this is done with the @xxxxx.setter decorator, where xxxxx is the
    name of the attribute.

    After having converted the variable names to something like self.__gear,
    create another function also with the same name as the attribute, and make sure
    this function takes a second parameter, which will be the new value for the
    attribute.

    Next you add the @gear.setter (in this case) decorator to the line directly
    above the function definition.

    Work with a partner to read, discuss, understand and modify the given code.
    Be sure to ask other classmates and/or your instructor if anything is unclear.

    a.  In the space "TODO 3a", what is wrong with setting the speed to 10 initially?
        Is the car ever in an inconsistent (nonsensical) state?

    b.  In the space "TODO 3b", write some lines that modify the car in a nonsensical
        fashion, even though it does not generate any errors.
    """
    print_exercise_name()

    car = Car3()

    print("Let's start driving correctly:")
    car.reset()
    car.gear = "D"
    car.speed = 10
    print(car)
    print()

    # TODO 3a: Read, discuss, and understand the following code.
    print("Let's start driving incorrectly:")
    car.reset()
    car.speed = 10
    car.gear = "D"
    print(car)
    print()

    # TODO 3b: Write code to use car as described
    car.reset()
    car.gear = "Q"
    car.speed = 1000
    print(car)


class Car3:
    def __init__(self):
        self.__gear = "P"  # Double underscore protects value from outside manipulation
        self.__speed = 0  # Double underscore protects value from outside manipulation

    def __str__(self):
        return "Car: gear={}, speed={}".format(self.gear, self.speed)

    @property
    def gear(self):
        """
        Provides read-access to the gear property.
        :return: car's gear
        :rtype: str
        """
        return self.__gear

    @property
    def speed(self):
        """
        Provides read-access to the speed property.
        :return: car's speed
        :rtype: int
        """
        return self.__speed

    @gear.setter
    def gear(self, new_gear):
        """
        Sets a new gear for the car, if the change is valid.
        :param str new_gear: the new gear
        """
        # Usually computer scientists do not like multiple return statements such
        # as in this setter method, but in this case it seems a reasonable approach
        if self.gear == new_gear:
            return  # No change

        if (self.gear == "D" or self.gear == "R") and new_gear == "P" and self.speed > 0:
            print("Error! Cannot shift into park while moving.")
            return  # No change

        self.__gear = new_gear  # Finally, update the gear

    @speed.setter
    def speed(self, new_speed):
        """
        Sets a new speed for the car, if the change is valid.
        :param int new_speed: the new speed
        """
        if self.speed == new_speed:
            return  # No change

        if self.gear == "P" and new_speed > 0:
            print("Error! Cannot change speed while parked.")
            return  # No change

        if new_speed < 0:
            print("Error! Cannot set negative speed.")
            return  # No change

        self.__speed = new_speed  # Finally, update the speed

    def reset(self):
        """ Resets the car to parked and zero speed. """
        self.__gear = "P"
        self.__speed = 0


"""
NOW IT'S YOUR TURN
"""


def exercise4():
    """
    Protect your Point class with good encapsulation.  To avoid the possibility of
    someone setting the point size to a negative value, modify the point class so
    that the attribute "size" is accessed through getters and setters.  If someone
    tries to set the size to a negative value, instead make no change to the size
    and print an error message to the console.

    A stripped down version of the Point class is provided as a starting place.

    a.  In the space "TODO 4a", change the attribute self.size so that it is protected
        from direct manipulation.  Use two underscores.

    b.  In the space "TODO 4b", add getter and setter functions using the @property
        and @size.setter decorators to access and set the size attribute correctly.
    """
    print_exercise_name()

    p1 = Point(10, 20)
    print(p1)
    p1.size = -10
    print(p1)
    if p1.size < 0:
        print("Go back and fix the Point class so size cannot be negative:", p1)


class Point:
    def __init__(self, x=0, y=0):
        """
        Creates a point with default x and y values (0, 0).
        :param float x: x position of center of point
        :param float y: y position of center of point
        """
        self.x = x
        self.y = y
        # TODO 4a: Protect the size attribute as described
        self.__size = 4

    def __str__(self):
        return "({},{}, size={})".format(self.x, self.y, self.size)

    def draw(self, art):
        art.setheading(art.towards(self.x, self.y))
        art.setposition(self.x, self.y)
        art.dot(self.size)

    # TODO 4b: Complete the property and setter functions to access the size attribute
    @property
    def size(self):
        return self.__size

    @size.setter
    def size(self, new_size):
        if new_size >= 0:
            self.__size = new_size
        else:
            print("Error! Size TOO small!")


def exercise5():
    """
    Make your own Rifle class with good encapsulation.

    Now it is your turn to create a Rifle class with attributes, methods, and good encapsulation.

    a.  In the space "TODO 5a", study the __init__ function that has been created for you.

    b.  In the space "TODO 5b", complete the __str__ method to provide a description
        of the rifle's current state, such as

            Rifle: ammo=0, safety=on

    c.  In the space "TODO 5c", provide property methods to access the ammo and safety attributes.

    d.  In the space "TODO 5d", provide a setter method to operate the rifle's safety.  Verify
        that the new safety value is a valid option ("on" or "off").  A docstring for a setter
        function should tell people what are valid options ("on" or "off").

        As a side effect, print to the console the safety status of the rifle.
        You would not normally have objects printing stray messages to the console,
        but for this exercise it will be helpful.

    e.  In the space "TODO 5e", write reload and unload functions that properly modify
        the rifle's attributes.  Reloading should set the ammo to 30.  Unloading, to zero.

        As a side effect, print to the console what just happened and the rifle's new status.

    f.  In the space "TODO 5f", write a fire method that properly responds to and modifies
        the rifle's state.  Check for safety.  Check for ammo.  Decrement ammo.  Print Bang!

        As a side effect, print to the console any error that causes the function to return early
        and the number of rounds remaining after going Bang!

    g.  Finally in the space "TODO 5g", write a sequence of method calls that manipulate
        the rifle, both properly and improperly.
    """
    print_exercise_name()

    rifle = Rifle()
    print("Starting:", rifle)
    rifle.safety = "off"
    rifle.fire()
    rifle.safety = "on"
    rifle.reload()
    # TODO 5g: In the space below used the class as described
    rifle.safety = "off"
    rifle.fire()
    rifle.fire()
    rifle.fire()
    rifle.fire()
    rifle.safety = "on"
    rifle.fire()
    rifle.safety = "off"
    rifle.fire()
    rifle.unload()
    rifle.fire()
    rifle.reload()
    rifle.fire()

    # Put the rifle away
    rifle.safety = "on"
    rifle.unload()


class Rifle:
    # TODO 5a
    def __init__(self):
        """ Creates a rifle that is empty and on safe. """
        self.__ammo = 0
        self.__safety = "on"  # Can be "on" or "off"

    def __str__(self):
        # TODO 5b: In the space below develop the class as described
        return "Rifle: Saftey: {}, Ammo: {}".format(self.__safety, self.__ammo)

    # TODO 5c: In the space below develop the class as described
    @property
    def ammo(self):
        return self.__ammo

    @property
    def safety(self):
        return self.__safety

    # TODO 5d: In the space below develop the class as described
    @safety.setter
    def safety(self, new_pos):
        """
        The only values accepted for safety are 'on' and 'off'
        :return:
        """
        if new_pos.lower() == "on" or new_pos.lower() == "off":
            self.__safety = new_pos
            print(self.__safety)
        else:
            print("Error! Safety must either be 'on' or 'off'!")

    # TODO 5e: In the space below develop the class as described
    def reload(self):
        self.__ammo = 30
        print("You are locked and loaded! 30 rounds!")

    def unload(self):
        self.__ammo = 0
        "Weapon unloaded. No ammo remains."

    # TODO 5f: In the space below develop the class as described

    def fire(self):
        if self.__safety == "off" and self.__ammo != 0:
            self.__ammo -= 1
            print("Bang!")
        elif self.__safety == "on":
            print("Error: Safety on!")
        elif self.__ammo == 0:
            print("Error: No Ammo")

def exercise6():
    """
    Upgrade your Rifle to full auto.

    Copy and paste your Rifle class below and call it FullAutoRifle.
    Modify it to support select fire mode where the safety selector can choose among
    safety on, single round fire, and three round burst ("on", "off", "burst").

    """
    print_exercise_name()

    rifle = FullAutoRifle()
    print("Starting:", rifle)

    rifle.fire()
    rifle.safety = "off"

    rifle.fire()
    rifle.reload()

    rifle.fire()
    rifle.fire()
    rifle.safety = "burst"
    rifle.fire()
    rifle.safety = "off"
    rifle.unload()


# TODO 6
class FullAutoRifle:
    # TODO 5a
    def __init__(self):
        """ Creates a rifle that is empty and on safe. """
        self.__ammo = 0
        self.__safety = "on"  # Can be "on" or "off"

    def __str__(self):
        # TODO 5b: In the space below develop the class as described
        return "Rifle: Saftey: {}, Ammo: {}".format(self.__safety, self.__ammo)

    # TODO 5c: In the space below develop the class as described
    @property
    def ammo(self):
        return self.__ammo

    @property
    def safety(self):
        return self.__safety

    # TODO 5d: In the space below develop the class as described
    @safety.setter
    def safety(self, new_pos):
        """
        The only values accepted for safety are 'on' and 'off'
        :return:
        """
        if new_pos.lower() == "on" or new_pos.lower() == "off":
            self.__safety = new_pos
            print(self.__safety)
        else:
            print("Error! Safety must either be 'on' or 'off'!")

    # TODO 5e: In the space below develop the class as described
    def reload(self):
        self.__ammo = 30
        print("You are locked and loaded! 30 rounds!!!")

    def unload(self):
        self.__ammo = 0
        "Weapon unloaded. No ammo remains."

    # TODO 5f: In the space below develop the class as described

    def fire(self):
        if self.__safety == "off" and self.__ammo != 0:
            self.__ammo -= 1
            print("Bang!")
        elif self.__safety == "on":
            print("Error: Safety on!")
        elif self.__ammo == 0:
            print("Error: No Ammo")



# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
