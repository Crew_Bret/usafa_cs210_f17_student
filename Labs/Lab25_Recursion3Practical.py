# !/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""
import json
import os

__author__ = "Bret Crew"
__instructor__ = "Dr Bower"
__date__ = "20 oct 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-   Read previous lessons on recursion

Lesson Objectives
-	Explore practical applications of recursion

"""


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    exercise1()


def exercise0():
    """
    Besides making pretty pictures and solving computational problems, there are other
    practical areas where recursion is a simpler way to solve a problem.
    Working with tree-based or hierarchical structures often is easiest with recursion.
    It is not uncommon to use a mix of for loops and recursion to make the cleanest code.
    """
    print_exercise_name()

    list_directory_contents("../Data")


def list_directory_contents(pathname, indent=0):
    """
    Prints the contents of a directory and all subdirectories
    :param str dir: the directory where the listing should begin
    :param int indent: used to indent the output according to the recursive depth
    """
    if os.path.isfile(pathname):
        print("\t" * indent, "FILE:", pathname)  # Print directory name

    elif os.path.isdir(pathname):
        print("\t" * indent, "DIRECTORY:", pathname)  # Print directory name
        contents = os.listdir(pathname)
        for child in contents:
            child_path = os.path.join(pathname, child)  # Why do you need this line?
            list_directory_contents(child_path, indent + 1)
    else:
        print("What is this thing?", pathname)


def exercise1():
    """
    Gather file system info.

    In this exercise you create a function that gathers file system information
    from a pathname that you specify.  The function calls itself recursively
    if a directory is encountered, continuing until every last sub-sub-subdirectory
    has been explored.

    The function returns a single record -- a dictionary -- which may in turn contain
    other records if it is a directory that you pass to the function.

    A file record should have three keys: name, size, and type.  An example:

        file_record = {
          "name": "../Data/car.gif",
          "size": 18564,
          "type": "file"
        }

    A directory record should have four keys: name, size, type, and contents.
    The size value should be the sum of all the files in all subdirectories within.
    The contents value will be a list of records of items within that directory,
    including both file and directory records.  An example:

        dir_record = {
            "name": "../Data",
            "size": 10730505,
            "type": "dir",
            "contents":
            [
                {"name": "../Data/car.gif", "size": 18564, "type": "file"},
                {"name": "../Data/Captain.txt", "size": 1171, "type": "file"},
                ...
            ]
        }

    In the space "TODO 1a_part1" build the base case scenario where the pathname
    is a file.  Return an appropriate record, and test the code in the space "TODO 1a_part2"

    In the space "TODO 1b_part1" build the continuation case where you have to
    explore a directory and its contents.  Return an appropriate record, and test the code
    in the space "TODO 1b_part2"
    """
    print_exercise_name()

    # TODO 1a_part2
    # data = gather_filesystem_info("../Data/car.gif")
    # print(json.dumps(data, indent=2))

    # TODO 1b_part2
    data = gather_filesystem_info("../Data")
    print(json.dumps(data, indent=2))


def gather_filesystem_info(pathname):
    # Base case - file - just gather file size and return a record
    if os.path.isfile(pathname):
        # TODO 1a_part1
        return {"name": os.path.basename(pathname), "size": os.path.getsize(pathname), "type": "file"}

    # Continuation case - directory - gather info for each item in contents
    elif os.path.isdir(pathname):
        # TODO 1b_part1
        contents = os.listdir(pathname)
        for child in contents:
            child_path = os.path.join(pathname, child)
            gather_filesystem_info(child_path)
        return {"name": os.path.basename(pathname), "size": os.path.getsize(pathname), "type": "directory", "contents": contents}

    else:
        print("What is this thing?", pathname)

# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
