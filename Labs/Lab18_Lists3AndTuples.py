#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

__author__ = "Bret Crew"
__instructor__ = "Dr Bower"
__date__ = "29 Sep 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 18: List Objects and Tuples from our online textbook

Lesson Objectives
-	Reinforce functions, parameters, return values, selection, and iteration.
-	Illustrate lists as objects/references.
-	Introduce tuples and illustrate similarities and differences with lists.


"""

import easygui
import os
import random


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise00()
    # exercise1()
    # exercise2()
    exercise3()
    # exercise4()


def exercise0():
    """
    Demonstrate tuples.

    Tuples are like lists, but they cannot be modified.  They are sometimes convenient
    when passing around data that naturally come as more than one number, such as
    x, y coordinates or monetary values and currency or many other things.
    """
    print_exercise_name()

    # A tuple is a convenient way to represent things like coordinates
    circle_center = (10, 20)  # Implies English version of x = 10, y = 20
    print("Circle center", circle_center)
    print("Circle x:", circle_center[0])  # Still access elements like a list
    # circle_center[0] = 11  # Error!  (comment this out)

    # A tuple is a convenient way to represent things like currency
    juice_price = (1.0, "USD")
    jerky_price = (4.50, "USD")
    print("Juice", juice_price)
    print("Jerky", jerky_price)
    print("Jerky in Euros:", convert_currency(jerky_price, "€"))

    # You can iterate over tuples just like a list
    a_list = [1, 2, 3]
    for x in a_list:
        print(x)
    a_tuple = (1, 2, 3)
    for x in a_tuple:
        print(x)


def convert_currency(original_price, new_currency):
    """
    Converts money to a new currency - only USD -> Euros is supported for now.
    :param (float, str) original_price: Tuple containing original monetary amount
    :param str new_currency: Name/shortand of new currency
    :return: Tuple representing new currency
    :rtype: (float, str)
    """
    old_amount, old_currency = original_price  # Extracts the two elements
    if new_currency == "€" or new_currency == "EURO" and old_currency == "USD":  # We only new euros for now
        new_amount = old_amount * 0.84  # Estimate
        return (new_amount, new_currency)  # Return as a tuple
    else:
        return original_price  # Else just return what we already had


def exercise00():
    """
    Demonstrate nested lists.

    Lists can themselves contain other lists.  If you have a regular structure
    to these nested lists, you might have yourself a matrix.

    The simplest way to work with a matrix in Python is to think of a list
    as containing a collection of rows.  See the examples below.

    """
    print_exercise_name()

    # Create a matrix with particular values.  Here is a 3x3 matrix:
    matrix_A = [[37, 84, 42, 51],
                [99, 13, 67, 75],
                [29, 32, 16, 64]]
    print_matrix(matrix_A)
    print()

    # Matrices can come in many sizes.  Here we have a function we created
    # below that creates a matrix of arbitrary size and fills it with
    # random values.
    num_rows = 3
    num_cols = 10
    lower_bound = 100
    upper_bound = 999
    matrix_B = rand_matrix(num_rows, num_cols, lower_bound, upper_bound)
    print_matrix(matrix_B)


def print_matrix(matrix):
    """
    Prints a matrix with values right-justified in 8-character columns.

    For example, the following nested list structure:
      [ [ 37, 84, 42, 51 ], [ 99, 13, 67, 75 ], [ 29, 32, 16, 64 ] ]
    would be printed as:
      37      84      42      51
      99      13      67      75
      29      32      16      64

    :param list[list[int]] matrix: The matrix to be printed.
    :return: None
    """

    for row in matrix:
        for value in row:
            # Print the value in an 8-character column, ending
            # with an empty string rather than the default newline.
            print("{:8d}".format(value), end="")
        # Print a newline at the end of each row; flushing any buffered output.
        print(flush=True)

        # Alternate method that uses row and column indices.
        # for r in range( len( matrix ) ):
        #     for c in range( len( matrix[ r ] ) ):
        #         # Print the value in an 8-character column, ending
        #         # with an empty string rather than the default newline.
        #         print( "{:8d}".format( matrix[ r ][ c ] ), end="" )
        #     # Print a newline at the end of each row; flushing any buffered output.
        #     print( flush=True )


def rand_matrix(rows, columns, lower_bound, upper_bound):
    """
    Build and return a matrix of random values.

    For example, given the parameters ( 3, 4, 10, 99 ), the function might
    build and return the following nested list structure:
      [ [ 37, 84, 42, 51 ], [ 99, 13, 67, 75 ], [ 29, 32, 16, 64 ] ]

    :param int rows: How many rows to include in the matrix.
    :param int columns: How many columns to include in the matrix.
    :param int lower_bound: The lower bound of the random values, inclusive.
    :param int upper_bound: The upper bound of the random values, inclusive.
    :return: A matrix with the indicated number of rows and columns.
    :rtype: list[list[int]]
    """
    mat = []
    for r in range(rows):  # Create this many rows
        row = []  # Make a new row
        for c in range(columns):  # Create this many columns
            row.append(random.randint(lower_bound, upper_bound))  # Fill with random
        mat.append(row)
    return mat


def exercise1():
    """
    Midpoint with Tuples.

    The midpoint function accepts two tuples as parameters, "unpacks" the tuples to
    extract the individual (x, y) coordinates, and then returns a tuple.

    In the space "TODO 1" finish the midpoint function so that it calculates
    the mid_x and mid_y values and then returns
    """
    print_exercise_name()

    origin = (0, 0)  # A tuple can be created with explicit parentheses,
    corner = 8.4, 12.6  # but can also be created with implicit parentheses.
    middle = midpoint(origin, corner)
    print("The midpoint between {} and {} is {}.".format(origin, corner, middle))


def midpoint(p1, p2):
    """
    Create and return the midpoint between two points, stored as (x,y) tuples.

    :param (float, float) p1: The first point, stored as an (x,y) tuple.
    :param (float, float) p2: The second point, stored as an (x,y) tuple.
    :return: The midpoint between p1 and p2.
    :rtype: (float, float)
    """
    # Unpack the point tuples into x,y coordinates.
    x1, y1 = p1  # Cool trick for unpacking tuples of known dimensions
    x2, y2 = p2
    # TODO 1: Calculate and return the midpoint as a new (x,y) tuple.
    x_mid = (x2 - x1) / 2
    y_mid = (y2 - y1) / 2
    return (x_mid, y_mid)


def exercise2():
    """
    Tuples to contain file metadata.

    File Info – Recall the file_info function from Lab 15 – Strings, Lists, and Files,
    re-written under the exercise3 function in this lab file.  The function accepts a file
    name as a parameter and returns a string of file information.

    a.	In the space "TODO 2a",
        re-write the file_info function such that instead of returning a single string it
        returns a tuple of three integer values, the number of lines, the number of words,
        and the number of characters.  Be sure to update the docstring!

    b.	In the space "TODO 2b",
        re-write the exercise3 function such that the message shown to the user is exactly
        the same, but the code uses the tuple returned by the re-written file_info function
        to create the message.

        Note: you'll need to move the formatting command out of the file_info() function.
    """
    print_exercise_name()

    # Get the first file name before testing the loop condition.
    filename = easygui.fileopenbox(default="../Data/*.txt")

    # A valid filename (i.e., user did not click Cancel) is longer than one character.
    while filename is not None:
        # Get the base file name to use as the dialog title, then show the results.
        basename = os.path.basename(filename)

        # TODO 2b: Re-write the code below to use the file_info function as described
        # Show the entire string returned from the file_info function in the message box.
        msg = file_info(filename)
        easygui.msgbox("Lines: {}\nWords: {}\nCharacters: {}".format(msg[0], msg[1], msg[2]), basename)

        # Get another file name before testing the loop condition.
        filename = easygui.fileopenbox(default="../Data/*.txt")


def file_info(filename):
    """
    Builds and returns a string with file information (lines, words, and characters).

    The string returned is in the following format:
    Lines: 4, Words: 21, Characters: 104

    :param filename: The file for which the info is to be obtained.
    :return: A tuple with the specified file information.
    :rtype: tuple
    """
    # Open the file and read its contents as a single string.
    with open(filename) as data_file:
        data_string = data_file.read()

    # Split the string into words and lines.
    data_words = data_string.split()
    data_lines = data_string.splitlines()

    # TODO 2a: rewrite the end of the function to return a tuple of three data points
    # Builds and return a string with the file information.
    return len(data_lines), len(data_words), len(data_string)


def exercise3():
    """
    Adding Matrices

    a.	In the space "TODO 3a", complete the add_matrices function such that it builds
        and returns a new matrix that is the result of adding the given matrices.

            http://www.mathsisfun.com/algebra/matrix-introduction.html

    b.	In the space "TODO 3b", write code to create two random matrices and add them,
        printing each matrix and the result in the console window.
    """
    print_exercise_name()

    # TODO 3b: Write code to use the function as described
    a = rand_matrix(3, 3, 0, 15)
    b = rand_matrix(3, 3, 0, 15)
    print(add_matrices(a, b))


def add_matrices(a, b):
    """
    Adds two matrices together.

    http://www.mathsisfun.com/algebra/matrix-introduction.html

    Note: This function does NOT change either of the original matrices!

    :param list[list[int]] a: Matrix to be added.
    :param list[list[int]] b: Matrix to be added.
    :return: The sum of the two matrices.
    :rtype: list[list[int]]
    """
    # TODO 3a: Remove the line below and complete the function as described
    mat = []
    for r in range(len(a)):  # Create this many rows
        row = []  # Make a new row
        for c in range(len(a[r])):  # Create this many columns
            row.append(a[r][c] + b[r][c])
        mat.append(row)
    return mat


def exercise4():
    """
    Magic Square

    a.	In the space "TODO 4a", complete the is_magic function such that it returns
        True if the given matrix is a magic square; False if it is not.

            https://en.wikipedia.org/wiki/Magic_square

        Hint: Recall the built-in sum() function calculates the sum of a single list.

    b.	In the space "TODO 4b", write code to confirm the nested list in the is_magic
        function docstring comment is a magic square and also write code to confirm a
        different list is not a magic square.  Further, write code that generates up
        to one-hundred-thousand 3x3 matrices of single-digit values, stopping and printing
        the first magic square it finds.
    """
    print_exercise_name()

    # TODO 4b: Write code to use the function as described
    square = [[2, 7, 6], [9, 5, 1], [4, 3, 8]]
    print_matrix(square)


def is_magic(square):
    """
    Determines if a matrix of integer values is a magic square.

    A matrix is a magic square if its row and column dimensions are equal and odd
    and the sum of all rows, columns, and diagonals through the center are equal.
    For example, the following matrix is a magic square:
        [ [ 2, 7, 6 ],
          [ 9, 5, 1 ],
          [ 4, 3, 8 ] ]

    Note: This function does NOT change the original matrix!

    :param list[list[int]] square: The matrix to be tested for magic.
    :return: True if the matrix is a magic square; False otherwise.
    :rtype: bool
    """
    # TODO 4a: Remove the line below and complete the function as described
    if len(square) != len(square[0]):
        return False
    elif len(square) % 2 == 0:
        return False
    standard = sum(square[0])
    sum_horz = standard
    index = 0
    while sum_horz == standard:
        row = square[index]
        sum_horz = sum(row)
        if index > len(square)


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
