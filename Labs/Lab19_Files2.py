#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""
import csv

__author__ = "Bret Crew and Maya Slavin"
__instructor__ = "Dr Bower"
__date__ = "03 Oct 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 19: Files from our online textbook

Lesson Objectives
-	Reinforce functions, parameters, return values, selection, and iteration.
-	Reinforce string and list attributes and operations.
-	Introduce more advanced file reading methods and file writing methods.

"""

import easygui
import turtle
# The timeit module provides a simple way to time small bits of Python code.
# https://docs.python.org/3/library/timeit.html
from timeit import timeit

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = False  # Set to True for fast, non-animated turtle movement.


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    # exercise2()
    # exercise3()
    exercise4()
    # exercise5()
    # challenge1()


def exercise0():
    """
    File Writing – The write() method vs. the print() function.

    Read, discuss, and understand the example code.
    """
    print_exercise_name()

    # TODO 0: Read, discuss, and understand the following code.

    # A familiar list of strings to test writing to a file.
    values = ["Integrity First", "Service Before Self", "Excellence In All We Do"]

    # Opening a file to write requires the second parameter, "w", to specify "write" mode.
    # For other modes, see https://docs.python.org/3.4/library/functions.html#open
    with open("../Data/Values_Write.txt", "w") as data_file:
        data_file.write(", ".join(values))

    # Above, the data_file object's write() method was used. However, it can only
    # accept a single string parameter and does not automatically append a newline.
    # The print() function can be used with the file parameter, which then also
    # allows use of the print() function's sep and end parameters.
    with open("../Data/Values_Print.txt", "w") as data_file:
        print(", ".join(values), file=data_file)

    # The easygui.filesavebox (as opposed to fileopenbox) will give a warning
    # if the file selected for writing already exists.
    with open(easygui.filesavebox(default="../Data/*.txt"), "w") as data_file:
        print("\n".join(values), file=data_file)


def exercise1():
    """
    File Reading – The string splitlines() method vs. the file readlines() method.

    Read, discuss, and understand the example code.
    """
    print_exercise_name()

    # TODO 1: Read, discuss, and understand the following code.

    # Recall this method of reading the entire contents
    # of a file into a list of strings, one per line:
    with open("../Data/Test.txt") as data_file:
        data = data_file.read().splitlines()  # splitlines() is a method of a string object.
    print(data)

    # The file object also has a readlines() method ... notice any difference in the output?
    with open("../Data/Test.txt") as data_file:
        data = data_file.readlines()  # readlines() is a method of a file object.
    print(data)


def exercise2():
    """
    File Reading – Reading an entire file into memory at once vs. reading a
    file line-by-line.

    Read, discuss, and understand the example code.
    """
    print_exercise_name()

    # TODO 2: Read, discuss, and understand the following code.

    # What if reading an entire file was not necessary?
    n = 32
    print("Starting timeit with n = {}.".format(n), flush=True)

    # timeit is a function that can execute a string containing
    # Python code and time how long it takes.  Do this n times.
    t1 = timeit("find_in_file_v1( 'silence', '../Data/WarAndPeace.txt' )",
                "from __main__ import find_in_file_v1", number=n)
    print("find_in_file_v1 time = {:.2f}.".format(t1), flush=True)

    t2 = timeit("find_in_file_v2( 'silence', '../Data/WarAndPeace.txt' )",
                "from __main__ import find_in_file_v2", number=n)
    print("find_in_file_v2 time = {:.2f}.".format(t2), flush=True)

    print("Percentage faster = {:.2%}.".format((t1 - t2) / t1), flush=True)


def find_in_file_v1(word, filename):
    """
    Find the line number of the first occurrence of word in filename.

    :param str word: The word to find.
    :param str filename: The file in which to find the word.
    :return: The line number of the first occurrence of the word; -1 if not found.
    :rtype: int
    """
    # Read the entire contents of the data file into a list of lines.
    with open(filename) as data_file:
        data = data_file.read().splitlines()

    # Note the following code is un-indented so the data file is closed.
    line_number = 0
    for line in data:
        if word.lower() in line.lower().split():
            return line_number
        else:
            line_number += 1

    return -1


def find_in_file_v2(word, filename):
    """
    Find the line number of the first occurrence of word in filename.

    :param str word: The word to find.
    :param str filename: The file in which to find the word.
    :return: The line number of the first occurrence of the word; -1 if not found.
    :rtype: int
    """
    # Open the data file, but do not immediately read its entire contents.
    with open(filename) as data_file:
        # Note the following code IS indented so the data file is NOT closed.
        line_number = 0
        for line in data_file:
            if word.lower() in line.lower().split():
                return line_number
            else:
                line_number += 1

    return -1


def find_in_file_v3(word, filename, max_lines=1024):
    """
    Find the line number of the first occurrence of word in filename.

    :param str word: The word to find.
    :param str filename: The file in which to find the word.
    :param int max_lines: The maximum number of lines to read before failing.
    :return: The line number of the first occurrence of the word; -1 if not found.
    :rtype: int
    """
    # Open the data file, but do not immediately read its entire contents.
    with open(filename) as data_file:
        # Note the following code IS indented so the data file is NOT closed.
        line_number = 0
        line = data_file.readline()  # Priming read.
        while line != "" and line_number < max_lines:
            if word.lower() in line.lower().split():
                return line_number
            else:
                line_number += 1
                line = data_file.readline()

    return -1


def exercise3():
    """
    Converting and Sorting Names

    a.	In the space "TODO 3a", complete the last_name_first function such that it
        converts a full name in "First Middle Last" format to an abbreviated name
        in "Last, First, M." format where M is the first letter of the middle name.

    b.	In the space "TODO 3b", write code necessary to read the names in the file
        "../Data/Names.txt", convert each name using the last_name_first function,
        sort the resulting list of names, and write the sorted names to the file
        "../Data/Names_Sorted.txt".
    """
    print_exercise_name()

    # Illustrate the last_name_first function before reading/writing the Names.txt file.
    print(last_name_first("George Washington Carver"))

    # TODO 3b: Write code to use the function as described
    filename = "../Data/Names.txt"
    with open(filename) as data_file:
        data = data_file.read().splitlines()
    name_list = []
    for names in data:
        name_list.append(last_name_first(names))
    name_list.sort()
    with open("../Data/Names_Sorted.txt", "w") as data_file:
        print("\n".join(name_list), file=data_file)


def last_name_first(name):
    """
    Converts a full name, First Middle Last, to Last, First M.

     For example, if the actual parameter value is "George Washington Carver",
     the value returned would be "Carver, George W."

    :param name: The First Middle Last name to be converted.
    :return: The Last, First M. name.
    :rtype: str
    """
    # TODO 3a: In the space below, complete the function as described
    char = 0
    first_name = ""
    middle_name = ""
    last_name = ""
    while name[char] != " ":
        first_name += name[char]
        char += 1
    char += 1
    while name[char] != " ":
        middle_name += name[char]
        char += 1
    char += 1
    while char < len(name):
        last_name += name[char]
        char += 1
    middle_initial = middle_name[0]
    return "{}, {} {}.".format(last_name, first_name, middle_initial)


def exercise4():
    """
    Comma Separated Values (CSV) Files
    Many files with columnar data are stored in CSV files where each field
    in the data is separated by a comma.  There are many complications that
    arise when reading someone else's data, and even CSV files throw in some
    trouble when a field value itself is supposed to contain commas.

    The safest way to read CSV files is with the csv module show below.

    In the space marked "TODO 4", complete the clean_top40 function so that
    it cleans up the data in the top40songs.txt file.  Your result should be
    a list of lists where each inner list is a row from the file.

    Perform the following cleaning actions on the data:

        - Remove the extra whitespace around the fields
        - Remove the parentheses surrounding the number/word in the first column

    """
    print_exercise_name()
    data = clean_top40("../Data/top40songs.txt")
    for row in data:
        print(row)


def clean_top40(filename):
    """
    Cleans a top40-formatted CSV text file and returns a list of lists
    where each inner list is a row from the file.
    :param str filename: the file to read
    :return: the file contents in list form
    :rtype: list[list[str]]
    """
    data = []  # type: list[list[str]]  # Detailed hints about 'data'
    with open(filename, "r") as f:
        csvreader = csv.reader(f, delimiter=' ')
        for row in csvreader:  # type: str  # Provide hints about for loop variable
            # TODO 4: Clean up this data
            print("")
    return dataggh


def exercise5():
    """
    Mad Lib – In the space "TODO 5", write code to read a Mad Lib file, replacing all
    words that begin and end with an underscore with a word entered by the user
    (via an easygui.enterbox), and write the result to a new file.  Save the new
    file with "_Completed" appended to the stem of the file name.
    (That is, "../Data/MadLib.txt" would be saved as "../Data/MadLib_Completed.txt".)

    An example from the original Mad Lib book would be written as follows:

            __________! he said, __________ as he jumped into his convertible
            exclamation	           adverb

            __________ and drove off with his _________ wife.
                  noun                        adjective

    The above example is written in the provided Mad Lib data file format as:

            _exclamation_ ! he said, _adverb_ as he
            jumped into his convertible _noun_ and
            drove off with his _adjective_ wife.

    Include the underscores around the new words to highlight which words were replaced:

            _Yowser_ ! he said, _slimily_ as he
            jumped into his convertible _turnip_ and
            drove off with his _fuzzy_ wife.

    """
    print_exercise_name()

    # TODO 5: Write code to use the function as described
    filename = "../Data/MadLib_Original.txt"
    with open(filename) as data_file:
        data = data_file.read().split()
    word_list = []
    for words in data:
        word_list.append(words)

    for i in range(len(word_list)):
        if "_" in word_list[i]:
            word_list[i] = easygui.enterbox("Choose a(n) {}".format(word_list[i]))
    print(word_list)
    with open("../Data/MadLib_Original_Completed.txt", "w") as data_file:
        print(" ".join(word_list), file=data_file)

"""
Challenge Exercises

Complete challenge1 below to see what gets drawn.
"""


def challenge1():
    """
    Mystery – In the space "TODO C1", write code to read the contents of the file
    "../Data/Mystery.txt" and perform the appropriate actions with the artist turtle.
    Specifically, each line in the data file contains one of three things: the word
    "UP", the word "DOWN", or two integers representing an (x, y) coordinate.  When the
    words "UP" or "DOWN" are read, the turtle should pick up or put down its pen,
    respectively.  When an (x, y) coordinate is read, the turtle should move from its
    current position to the indicated position.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # TODO C1: Write code to use the Mystery.txt file as described
    pass  # Remove the pass statement (and this comment) when writing your own code

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
