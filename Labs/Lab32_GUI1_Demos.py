#!/usr/bin/env python3
"""Examples from GUI reading."""

import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkinter import simpledialog
from tkinter import filedialog
from tkinter import colorchooser
import os


def main():
    """Main program to run each demo."""
    demo0()
    demo1()
    demo2()
    demo3()
    demo4()
    demo5()
    demo6()
    demo7()


def demo0():
    """Event loop provided by operating system (you do not write this code yourself!)"""

    print( "Hypothetical event loop; do not write this code yourself!" )
    # while True:
    #     # Get the next event from the operating system
    #     event = get_next_event()
    #
    #     # Get the function that is assigned to handle this event
    #     a_function_to_handle_the_event = event - handlers[ event ]
    #
    #     # If a function has been assigned to handle this event, call the function
    #     if a_function_to_handle_the_event:
    #         a_function_to_handle_the_event()  # Call the event-handler function
    #
    #     # Stop processing events if the user gives a command to stop the application
    #     if window_needs_to_close:
    #         break  # out of the event-loop


# http://interactivepython.org/runestone/static/cs210_f17/GUIandEventDrivenProgramming/01_intro_gui.html#hello-world
def demo1():
    """Hello, world!"""

    # Create the application window
    window = tk.Tk()

    # Create the user interface
    my_label = ttk.Label( window, text="Hello, World!" )
    my_label.grid( row=1, column=1 )

    # Start the GUI event loop
    window.mainloop()


# http://interactivepython.org/runestone/static/cs210_f17/GUIandEventDrivenProgramming/02_standard_dialog_boxes.html#messages
def demo2():
    """Message boxes."""

    # Create and hide the application window.
    window = tk.Tk()
    window.withdraw()

    messagebox.showinfo( "Information", "This is an information message.", parent=window )
    messagebox.showerror( "Error", "This is an error message.", parent=window )
    messagebox.showwarning( "Warning", "This is a warning message.", parent=window )


# http://interactivepython.org/runestone/static/cs210_f17/GUIandEventDrivenProgramming/02_standard_dialog_boxes.html#yes-no-questions
def demo3():
    """Yes/No/Cancel Questions"""

    # Create and hide the application window.
    window = tk.Tk()
    window.withdraw()

    questions = [ "Do you want to open this file?", "Do you want to try that again?",
                  "Do you like Python?", "Continue playing?" ]

    answers = [ messagebox.askokcancel( "Question", questions[ 0 ], parent=window ),
                messagebox.askretrycancel( "Question", questions[ 1 ], parent=window ),
                messagebox.askyesno( "Question", questions[ 2 ], parent=window ),
                messagebox.askyesnocancel( "Question", questions[ 3 ], parent=window ) ]

    messagebox.showinfo( "Answers", "{} = {}\n{} = {}\n{} = {}\n{} = {}".format(
        questions[ 0 ], answers[ 0 ],
        questions[ 1 ], answers[ 1 ],
        questions[ 2 ], answers[ 2 ],
        questions[ 3 ], answers[ 3 ] ) )


# http://interactivepython.org/runestone/static/cs210_f17/GUIandEventDrivenProgramming/02_standard_dialog_boxes.html#single-value-data-entry
def demo4():
    """Simple data entry."""

    # Create and hide the application window.
    window = tk.Tk()
    window.withdraw()

    answer = simpledialog.askstring( "Input", "What is your first name?", parent=window )
    messagebox.showinfo( "Answer", "answer = {}\nand is a {}".format( answer, type( answer ) ), parent=window )

    answer = simpledialog.askinteger( "Input", "What is your age?",
                                      minvalue=0, maxvalue=100,
                                      parent=window )
    messagebox.showinfo( "Answer", "answer = {}\nand is a {}".format( answer, type( answer ) ), parent=window )

    answer = simpledialog.askfloat( "Input", "What is your salary?",
                                    minvalue=0.0, maxvalue=100000.0,
                                    parent=window )
    messagebox.showinfo( "Answer", "answer = {}\nand is a {}".format( answer, type( answer ) ), parent=window )


# http://interactivepython.org/runestone/static/cs210_f17/GUIandEventDrivenProgramming/02_standard_dialog_boxes.html#file-chooser
def demo5():
    """File dialogs."""

    # Create and hide the application window.
    window = tk.Tk()
    window.withdraw()

    # Build a list of tuples for each file type the file dialog should display
    file_types = [ ('Text Files', '.txt'), ('Python Files', '.py'), ('All Files', '.*') ]

    # Ask the user to select a folder.
    file_name = filedialog.askdirectory( title="Please select a folder:", parent=window,
                                         initialdir=os.path.expanduser( "~" ) + "\Documents" )
    messagebox.showinfo( "File Dialog Result", file_name, parent=window )

    # Ask the user to select a single file name.
    file_name = filedialog.askopenfilename( title="Please select a file:", parent=window,
                                            initialdir=os.getcwd(), filetypes=file_types )
    messagebox.showinfo( "File Dialog Result", file_name, parent=window )

    # Ask the user to select a one or more file names.
    file_name = filedialog.askopenfilenames( title="Please select one or more files:", parent=window,
                                             initialdir=os.getcwd() + "\..\Data", filetypes=file_types )
    messagebox.showinfo( "File Dialog Result", file_name, parent=window )

    # Ask the user to select a single file name for saving.
    file_name = filedialog.asksaveasfilename( title="Please select a file name for saving:", parent=window,
                                              initialdir=os.getcwd(), filetypes=file_types )
    messagebox.showinfo( "File Dialog Result", file_name, parent=window )


# http://interactivepython.org/runestone/static/cs210_f17/GUIandEventDrivenProgramming/02_standard_dialog_boxes.html#color-chooser
def demo6():
    """Color chooser."""

    # Create and hide the application window.
    window = tk.Tk()
    window.withdraw()

    rgb, web = colorchooser.askcolor( parent=window, initialcolor=(255, 0, 0) )
    messagebox.showinfo( "Color Chooser Result", "rgb = {}\nweb = {}".format( rgb, web ), parent=window )


# https://docs.python.org/3/library/tkinter.html#a-simple-hello-world-program
def demo7():
    """Hello world from Python docs."""
    root = tk.Tk()
    app = HelloWorldApplication( master=root )
    app.mainloop()


class HelloWorldApplication( tk.Frame ):
    """Hello world application from python docs."""

    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.hi_there = tk.Button(self)
        self.hi_there["text"] = "Hello World\n(click me)"
        self.hi_there["command"] = self.say_hi
        self.hi_there.pack(side="top")

        self.quit = tk.Button(self, text="QUIT", fg="red", command=self.master.destroy)
        self.quit.pack(side="bottom")

    def say_hi(self):
        messagebox.showinfo( "", "hi there, everyone!", parent=self.master )


# If not imported, run main after all definitions.
if __name__ == "__main__":
    main()
