#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""
import random
import easygui

__author__ = "Bret Crew"
__instructor__ = "Dr. Bower"
__date__ = "07 Sep 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 10: Selection – Nested and Chained from our online textbook

Lesson Objectives
-	Reinforce functions, parameters, return values, and selection statements
-	Introduce nested and chained selection
-	Introduce Boolean functions


"""

"""
Before Starting

To allow a more focused effort on selection statements, the template for today's lab includes
all of the necessary code in the exercise functions and stubs for the remaining functions.
Begin your work on today's lab by running the provided template and observing the results.
Then, as you work, look carefully at the TODO comments and focus your efforts as indicated.
"""


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    # exercise2()
    # exercise3()
    # exercise4()
    exercise5()


def exercise0():
    """Example code."""
    print_exercise_name()

    # Pick a random number and then do something different depending on that number
    n = random.randint(1, 3)
    if n == 1:
        print("We're number one!  We're number e^(2πi)!")
    elif n == 2:
        for i in range(2, 9, 2):
            print(i, " ", end="")
        print("Who do we appreciate?!")
    elif n == 3:
        print("A cord of three strands is not quickly broken.")
    else:
        print("I don't know what to do with any other numbers.")


def exercise1():
    """
    Interact with the user and test the exemplar function.

    Exemplar – In the space "TODO 1", write a chained selection statement to return
    one of the exemplar last names based on the given class year.
    Note the class year is entered as a two-digit number, 18, 19, 20, etc
    """
    print_exercise_name()

    # Get a year from the user.
    year = easygui.integerbox("Enter two-digit class year (eg 18):", "Input", None, 17, 20)

    # Continue until the user clicks the Cancel button.
    while year is not None:
        # Show the exemplar for the given year.
        easygui.msgbox("Class of 20{}'s exemplar is {}.".format(year, exemplar(year)), "Result")

        # Get another year from the user.
        year = easygui.integerbox("Enter two-digit class year (eg 18):", "Input", None, 00, 99)


def exemplar(class_year):
    """Determine the exemplar for the classes currently at USAFA.

    Since the class of '19 has not yet chosen their exemplar, the only valid
    input values for this function are 16, 17, and 18.

    :param int class_year: The class year, [16-18].
    :return: The exemplar for the given year.
    :rtype: str
    """
    # TODO 1: Implement the selection statement as described
    if class_year == 17:
        return "BUD!"

    if class_year == 18:
        return "ZAMP!"

    if class_year == 19:
        return "Strong!"

    if class_year == 20:
        return "Bob!!!"
    return "Unknown"


def exercise2():
    """
    Interact with the user and test the drink_size function.

    Drink Size – In the space "TODO 2", write a chained selection statement to return
    one of the drink sizes, "Small", "Medium", "Large", or "Ridiculous", based on the
    number of ounces in the drink. See the docstring comment for specific drink size values.
    """
    print_exercise_name()

    # Get a number of ounces from the user.
    oz = easygui.integerbox("Enter drink ounces:", "Input", None, 1, 144)

    # Continue until the user clicks the Cancel button.
    while oz is not None:
        # Show the drink size for the given number of ounces.
        easygui.msgbox("A {}oz drink is {}.".format(oz, drink_size(oz)), "Result")

        # Get another drink size from the user.
        oz = easygui.integerbox("Enter drink ounces:", "Input", None, 1, 144)


def drink_size(ounces):
    """
    Determine the descriptive size for a drink with the given number of ounces.

    A drink with 12 or fewer ounces is considered to be "Small".
    A drink with more than 12 but 20 or fewer ounces is considered to be "Medium".
    A drink with more than 20 but 32 or fewer ounces is considered to be "Large".
    A drink with more than 32 ounces is considered to be "Ridiculous".

    :param int ounces: The drink size, in ounces.
    :return: The description of the drink size, "Small", "Medium", "Large", or "Ridiculous".
    :rtype: str
    """
    # TODO 2: Implement the selection statement as described

    if ounces <= 12:
        return "smol"
    elif ounces <=20:
        return "medi-yum"
    elif ounces <= 32:
        return "yuge"
    else:
        return "ridonculous"



def exercise3():
    """
    Interact with the user and test the quadrant function.

    Quadrant - In the space "TODO 3", write a nested selection statement to return
    the quadrant in which the given (x, y) coordinate lies, 1, 2, 3, or 4. If the coordinate
    is on an axis, return zero.

                ^ +y
                |
            2   |   1
                |
        --------+--------> +x
                |
            3   |   4
                |
    """
    print_exercise_name()

    # Get a coordinate from the user.
    x = easygui.integerbox("Enter x coordinate:", "Input", None, -2 ** 31, 2 ** 31)
    y = easygui.integerbox("Enter y coordinate:", "Input", None, -2 ** 31, 2 ** 31)

    # Continue until the user clicks the Cancel button.
    while x is not None and y is not None:
        # Show the quadrant for the given coordinate.
        easygui.msgbox("({},{}) is in quadrant {}.".format(x, y, quadrant(x, y)), "Result")

        # Get another coordinate from the user.
        x = easygui.integerbox("Enter x coordinate:", "Input", None, -2 ** 31, 2 ** 31)
        y = easygui.integerbox("Enter y coordinate:", "Input", None, -2 ** 31, 2 ** 31)


def quadrant(x, y):
    """Determine the quadrant in which the coordinate (x,y) lies; zero if on an axis.

    :param int x: The x coordinate.
    :param int y: The y coordinate.
    :return: The quadrant that contains (x,y); zero if on an axis.
    :rtype: int
    """
    # TODO 3: Implement the selection statement as described
    if x > 0:
        if y > 0:
            return 1
        elif y < 0:
            return 4
        else:
            return 0
    elif x < 0:
        if y > 0:
            return 2
        elif y < 0:
            return 3
        else:
            return 0
    else:
        return 0


def exercise4():
    """
    Interact with the user and test the days_in_month function.

        Thirty days hath September,
        April, June, and November.
        All the rest have thirty-one,
        Except for February alone,
        Which hath but twenty-eight days clear,
        And twenty-nine in each leap year.
            - Mother Goose
            - Other variants at https://en.wikipedia.org/wiki/Thirty_days_hath_September

    As seen in a previous lab exercise (lab 9, exercise 4), a leap year is more
    than simply divisible by four.

    a.	In the space "TODO 4a", remove the True value from the provided stub and replace
        it with the necessary condition for a leap year.  Note you do not need to write a
        selection statement, but simply write the necessary condition in place of the
        value True. (Hint: You can find the necessary condition in lab 9)

    b.	In the space "TODO 4b", write the necessary selection statement to determine and
        return the number of days in the given month/year.  Use the is_leap function where
        appropriate (actually call the function, don't just re-write the condition)!
    """
    print_exercise_name()

    # Get a month from the user.
    m = easygui.integerbox("Enter month (1-12):", "Input", None, 1, 12)

    # Continue until the user clicks the Cancel button.
    while m is not None:
        # Show the number of days in the given month.
        if m == 2:
            # Prompt the user for the year if the month is February.
            y = easygui.integerbox("Enter year (1582-):", "Input", None, 1582, 2 ** 31)
            easygui.msgbox("{} of {} has {} days.".format(m, y, days_in_month(m, y)), "Result")
        else:
            # Use a known non-leap year for any month besides February.
            easygui.msgbox("{} has {} days.".format(m, days_in_month(m, 2015)), "Result")

        # Get another month from the user.
        m = easygui.integerbox("Enter month (1-12):", "Input", None, 1, 12)


def is_leap(year):
    """Determines if the given year is a leap year.

    :param int year: The year to be tested for leap-ness.
    :return: True if the given year is a leap year; False otherwise.
    :rtype: bool
    """
    # TODO 4a: Remove True from the line below and write the leap year condition as described
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)



def days_in_month(month, year):
    """Determine the days in the given month during the given year.

    :param int month: The number of the month (1 = January, 2 = February, etc).
    :param int year: The year during which the month occurs.
    :return: The number of days in the given month, including leap years.
    :rtype: int
    """
    # TODO 4b: Implement the selection statement as described
    if month == 4 or month == 6 or month == 9 or month == 11:
        return 30
    if month == 2:
        if is_leap(year):
            return 29
        else:
            return 28
    else:
        return 31


def exercise5():
    """
    Interact with the user and test the passed_pft function for someone less than 30 years of age.

    PFT – In the space "TODO 5", write the necessary code to return True if the parameters
    specify a passed PFT; False otherwise.

    You will need to look up the official Air Force fitness tables.  Include a reference
    to where you got the tables in a comment in the passed_pft docstring.

    Note the function is only to be used for a person less than 30 years of age!
    """
    print_exercise_name()

    # Get a set of PFT results from the user.
    g = easygui.buttonbox(msg="Select gender:", title="Select", choices=["Female", "Male"])
    r = easygui.integerbox("Enter run time (in seconds):", "Input", None, 300, 3000)
    s = easygui.integerbox("Enter situps completed:", "Input", None, 0, 180)
    p = easygui.integerbox("Enter pushups completed:", "Input", None, 0, 180)

    # Continue until the user clicks the Cancel button.
    while g is not None and r is not None and s is not None and p is not None:
        # Show the Pass/Fail result of the PFT.
        if passed_pft(g, r, s, p):
            easygui.msgbox("Pass!", "Result")
        else:
            easygui.msgbox("Fail.", "Result")

        # Get another set of PFT results from the user.
        g = easygui.buttonbox(msg="Select gender:", title="Select", choices=["Female", "Male"])
        r = easygui.integerbox("Enter run time (in seconds):", "Input", None, 300, 3000)
        s = easygui.integerbox("Enter situps completed:", "Input", None, 0, 180)
        p = easygui.integerbox("Enter pushups completed:", "Input", None, 0, 180)


def passed_pft(gender, run_time, situps, pushups):
    """
    Determines if the specified parameters pass the PFT for someone less than 30 years of age.

    Source:

    :param str gender: The person's gender, "Male" or "Female".
    :param int run_time: The 1.5 mile run time, in seconds.
    :param int situps: The number of situps completed in one minute.
    :param int pushups: The number of pushups completed in one minute.
    :return: True if the scores pass; False otherwise.
    :rtype" bool
    """
    # TODO 5: Implement the Boolean function as described
    if gender == "Male":
        if run_time <= 750:
            if pushups >= 33:
                if situps >= 40:
                    return True
                else: return False
            else: return False
        else: return False
    elif gender == "Female":
        if run_time <= 870:
            if pushups >= 18:
                if situps >= 35:
                    return True
                else: return False
            else: return False
        else: return False


"""
Challenge Exercises:

1.  Extend the PFT exercise to include other ages for which the Air Force
    has published fitness charts.

2.	Complete unfinished exercises from any previous lab.
"""


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
