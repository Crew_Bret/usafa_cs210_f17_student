#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

__author__ = "Bret Crew"
__instructor__ = "Dr Crew"
__date__ = "18 oct 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read the Lesson 24 – Recursion of our online text

Lesson Objectives
-	Introduce recursive programming

"""

import easygui
import random
import string


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    # exercise2()
    # exercise3()
    exercise4()
    # exercise3_challenge()


def exercise0():
    """
    Sum up a list of numbers with loops and recursion.

    Study the two functions sum_loop and sum_recursion to see how they work.

    The next few examples help you learn HOW to use recursion to solve problems,
    although you might think that some of these would be easier to solve with loops.
    You would be right.  Exercises later in the lab will illustrate where recursive
    solutions are more natural.
    """
    print_exercise_name()

    numbers = [37, 84, 42, 51]  # Sum is 214
    loop_answer = sum_loop(numbers)
    recursion_answer = sum_recursion(numbers)
    print("Numbers:", numbers)
    print("Loop:", loop_answer)
    print("Recursion:", recursion_answer)


def sum_loop(numbers):
    """
    Sums numbers in a list using a loop.

    :param [int] numbers: the list of numbers to sum
    :return: sum of the numbers
    :rtype: int
    """
    sum = 0
    for n in numbers:
        sum = sum + n
    return sum


def sum_recursion(numbers):
    """
    Sums numbers in a list using recursion.

    :param [int] numbers: the list of numbers to sum
    :return: sum of the numbers
    :rtype: int
    """
    if len(numbers) == 0:  # Base case: empty list
        return 0
    else:
        # Add first number to sum of remaining list
        return numbers[0] + sum_recursion(numbers[1:])


def exercise1():
    """
    Calculate factorials with loops and recursion.

        https://en.wikipedia.org/wiki/Factorial

    a.	In the space "TODO 1a", complete the function factorial_recursion, which receives
        an integer and returns the factorial of that integer.

    b.  In the space "TODO 1b", add three more tests to those provided that show
        that your function works as expected.
    """
    print_exercise_name()

    n = 6  # 6! = 720
    loop_answer = factorial_loop(n)
    recursion_answer = factorial_recursion(n)
    print("n:", n)
    print("Loop:", loop_answer)
    print("Recursion:", recursion_answer)

    # TODO 1b: Add three more examples to the two below that test your function
    tests = [
        [4, 24],  # Test 1
        [6, 720],  # Test 2
        [5, 120],
        [7, 5040],
        [12, 479001600]
    ]
    for n, expected_value in tests:  # Pull out a single test
        actual_value = factorial_recursion(n)  # See what our function gets
        if actual_value != expected_value:  # Was it right?
            print("Incorrect: {}! Expected: {}. Got: {}".format(
                n, expected_value, actual_value))


def factorial_loop(n):
    """
    Calculates a factorial using a loop.

    :param int n: the number to calculate n!
    :return: the factorial of n!
    :rtype: int
    """
    fact = 1
    for x in range(1, n + 1):
        fact = fact * x
    return fact


def factorial_recursion(n):
    """
    Calculates a factorial using recursion.

    :param int n: the number to calculate n!
    :return: the factorial of n!
    :rtype: int
    """
    # TODO 1a: Remove the line below and complete the function as described
    if n <= 1:
        return 1
    else:
        return n * factorial_recursion(n - 1)


def exercise2():
    """
    Calculate the nth term of the Fibonacci sequence with loops and recursion.

        https://en.wikipedia.org/wiki/Fibonacci

    The Fibonacci sequence is defined as the series 1, 1, 2, 3, 5, 8, 13, ...
    where each successive number is the sum of the previous two numbers.

    The nth term in the sequence can be defined by the relationship
    fib(n) = fib(n-1) + fib(n-2).

    Assume that fib(1) is equal to 1 and fib(2) is equal to 1.

    Note: You can assume that your function will always receive a positive
    integer as an input argument.

    a.	In the space "TODO 2a", complete the function fibonacci_recursion, which receives
        an integer, n, and returns the nth term of the Fibonacci sequence.

        Note: This will be a much more elegant solution than the loop version.

    b.  In the space "TODO 2b", add three more tests to those provided that show
        that your function works as expected.
    """
    print_exercise_name()

    n = 6  # fib(6): 1, 1, 2, 3, 5, 8: 8
    loop_answer = fibonacci_loop(n)
    recursion_answer = fibonacci_recursion(n)
    print("n:", n)
    print("Loop:", loop_answer)
    print("Recursion:", recursion_answer)

    # TODO 2b: Add three more examples to the two below that test your function
    tests = [
        [4, 3],  # Test 1
        [6, 8],  # Test 2
        [7, 13],
        [8, 21],
        [9, 34]
    ]
    for n, expected_value in tests:  # Pull out a single test
        actual_value = fibonacci_recursion(n)  # See what our function gets
        if actual_value != expected_value:  # Was it right?
            print("Incorrect: {}! Expected: {}. Got: {}".format(
                n, expected_value, actual_value))


def fibonacci_loop(n):
    """
    Calculates the nth term of the Fibonacci sequence using a loop.

    :param int n: the term to find
    :return: the nth term of the Fibonacci sequence
    :rtype: int
    """
    term0 = 1
    term1 = 1
    next_term = 1
    for term in range(3, n + 1):
        next_term = term0 + term1
        term0 = term1
        term1 = next_term

    return next_term


def fibonacci_recursion(n):
    """
    Calculates the nth term of the Fibonacci sequence using recursion.

    :param int n: the term to find
    :return: the nth term of the Fibonacci sequence
    :rtype: int
    """
    # TODO 2a: Remove the line below and complete the function as described
    if n <= 2:
        return 1
    else:
        return fibonacci_recursion(n - 1) + fibonacci_recursion(n - 2)


def exercise3():
    """
    Determine if a string is a palindrome.

        https://en.wikipedia.org/wiki/Palindrome

    A string is a palindrome if it is spelled the same both forward
    and backward. For example "radar" is a palindrome.

    a.	In the space "TODO 3a", complete the function is_palindrome_recusion, which
        receives a string and returns True if the string is a palindrome, False otherwise.

        Note: You do not need to convert upper/lower case for this problem or strip
        white space or punctuation.  Consider a string a palindrome if it strictly
        mirrors itself about the middle.

    b.  In the space "TODO 3b", add three more tests to those provided that show
        that your function works as expected.
    """
    print_exercise_name()

    word = "radar"
    loop_answer = is_palindrome_loop(word)
    recursion_answer = is_palindrome_recursion(word)
    print("Word:", word)
    print("Loop:", loop_answer)
    print("Recursion:", recursion_answer)

    # TODO 3b: Add three more examples to the three below that test your function
    tests = [
        ["radar", True],  # Test 1
        ["anna", True],  # Test 2
        ["cat", False],  # Test 3
        ["aibohphobia", True],
        ["racecar", True],
        ["help_me!!!!", False]
    ]
    for n, expected_value in tests:  # Pull out a single test
        actual_value = is_palindrome_recursion(n)  # See what our function gets
        if actual_value != expected_value:  # Was it right?
            print("Incorrect: {}! Expected: {}. Got: {}".format(
                n, expected_value, actual_value))


def is_palindrome_loop(word):
    """
    Determines if a string is a palindrome using a loop.

    :param str word: the word or phrase to check
    :return: whether or not the word is a palindrome
    :rtype: bool
    """
    for i in range(len(word) // 2):
        if word[i] != word[-i - 1]:
            return False
    return True


def is_palindrome_recursion(word):
    """
    Determines if a string is a palindrome using recursion.

    :param str word: the word or phrase to check
    :return: whether or not the word is a palindrome
    :rtype: bool
    """
    # TODO 3a: Remove the line below and complete the function as described
    if len(word) <= 1:
        return True
    if word[0] == word[-1] and is_palindrome_recursion(word[1:-1]):
        return True
    else:
        return False


def exercise4():
    """
    Print outline data with proper indentation.

    In this exercise you will implement both loop-based and recursion-based
    functions to print an outline to the console.  You do not need to add
    level counters like I., II., III., a), b), c), etc., but you do need to
    indent each new level four spaces.

    The outline is contained in a nested list.  As an example the following
    outline data structure would appear as shown below.

    outline = ["Fruit", ["Apple", "Banana", "Pear"], "Vegetable", ["Broccoli", "Carrot"]]

    Human readable:

    Fruit
        Apple
        Banana
        Pear
    Vegetable
        Broccoli
        Carrot

    a.	In the space "TODO 3a", complete the function print_outline_loop, which
        prints an outline using loops.

        Note: If you spend more than five minutes on the loop version, comment
        out your non-working code and move on to the recursion version.

    b.	In the space "TODO 3a", complete the function print_outline_recursion, which
        prints an outline using recursion.

        Hint: Functions can have optional parameters that take on a default value
        when unspecified.

            def cadet(name, age=18):
                print(name, age)

            cadet("John")
            cadet("Jane", 24)

        This code fragment would output the following:

            John 18
            Jane 24

        Hint: Python has a function isinstance() that can be used to check what kind
        of data is stored in a variable.  The example below would print Yes twice.

            a = [1, 2, 3]
            b = "cat"
            if isinstance(a, list):
                print("Yes")
            if isinstance(b, str):
                print("Yes")
    """
    print_exercise_name()

    outline = ["Fruit", ["Apple", "Banana", "Pear"], "Vegetable", ["Broccoli", "Carrot"]]
    print("Loop:")
    print_outline_loop(outline)
    print("Recursion:")
    print_outline_recursion(outline)

    outline = [  # https://www.teachervision.com/writing/essays/1779.html
        "Introduction",
        "Background Information",
        ["Location of Mt. Everest",
         "Geography of the Surrounding Area",
         "Facts about Mt. Everest",
         ["Height of the mountain",
          "How the mountain was named",
          ["Peak XV",
           "Joloungma (Tibetan name)",
           "Sagarmatha (Nepalese name)"
           ],
          "The number of people who have climbed Everest to date"
          ]
         ],
        "Major Explorers Covered in this Paper",
        ["Sir Edmund Hillary",
         ["First to reach the summit (1953)",
          "Led a team of experienced mountain climbers who worked together"
          ],
         "Tenzing Norgay and the Sherpas",
         ["Norgay was an experienced climber and guide who accompanied Hillary",
          "Sherpas still used to guide expeditions"
          ],
         "Rob Hall",
         ["Leader of the failed 1996 expedition",
          "Led group of (mainly) tourists with little mountain climbing experience"
          ]
         ],
        "The Impact Expeditions have had on Mt. Everest and Local Community",
        ["Ecological Effects",
         ["Loss of trees due to high demand for wood for cooking and heating for tourists.",
          "Piles of trash left by climbing expeditions"
          ],
         "Economic Effects",
         ["Expedition fees provide income for the country",
          "Expeditions provide work for the Sherpas, contributing to the local economy."
          ],
         "Cultural Effects",
         ["Introduction of motor vehicles",
          "Introduction of electricity"
          ]
         ],
        "Conclusion"
    ]
    print("Loop:")
    print_outline_loop(outline)
    print("Recursion:")
    print_outline_recursion(outline)


def print_outline_loop(outline):
    """
    Prints an outline to the console using loops.

    :param [] outline: the outline as a nested list
    :return: None
    """
    # TODO 4a: Remove the line below and complete the function as described
    for paragraph in outline:
        if isinstance(paragraph, str):
            print(paragraph)
        else:
            for topic in paragraph:
                if isinstance(topic, str):
                    print("    {}".format(topic))
                else:
                    for var in topic:
                        if isinstance(var, str):
                            print("        {}".format(var))
                        else:
                            for thing in var:
                                print("            {}".format(thing))


def print_outline_recursion(outline, indent=0):
    """
    Prints an outline to the console using recursion.

    :param [] or str outline: the outline as a nested list
    :param int indent: optional amount of indentation
    :return: None
    """
    # TODO 4b: Remove the line below and complete the function as described
    if isinstance(outline[0], str) == False and isinstance(outline[0], list) == False:
        return ""
    else:
        print_outline_recursion(outline[1:])

"""
Challenge Exercises

1.  Upgrade exercise 3 and your is_palindrome_recursion function to account
    for, ie, ignore, whitespace and punctuation.  Add unit tests to include
    more complex palindromes such as those listed here:

        http://www.palindromelist.net/

2.  Upgrade exercise 4 and your print_outline_recursion function to include
    decimal counters:

        1.  Fruit
            1.1 Apple
            1.2 Banana
            1.3 Pear
        2.  Vegetable
            2.1 Broccoli
            2.2 Carrot
"""


def exercise3_challenge():
    """
    Demonstrated improved version of palindrome checker
    """
    print_exercise_name()

    # TODO: Challenge


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
