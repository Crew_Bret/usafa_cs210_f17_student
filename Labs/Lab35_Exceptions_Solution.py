#!/usr/bin/env python3
"""
Lab32 Try/Except Solution
CS 210, Introduction to Programming
"""

__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read this article on exception handling in Python
        http://www.pythonforbeginners.com/error-handling/exception-handling-in-python
-	Watch this video on exception handling in Python
        https://www.youtube.com/watch?v=hrR0WrQMhSs&list=PLirBjzN2aCJlPHizH6KIpzCA5IRDtiB50&index=5

Lesson Objectives
-	Introduce the try statement and exceptions
        https://docs.python.org/3/reference/compound_stmts.html#the-try-statement
        https://docs.python.org/3/library/exceptions.html

"""


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    exercise1()
    exercise2()
    exercise3()
    exercise4()
    exercise5()


def exercise0():
    """
    Obtain user input, convert to integer, and display results.

    By now you have all experienced an error or exception.  This is the red text, sometimes
    difficult to read, that appears in the console window when your program encounters an
    error from which it cannot recover.  Run the given code and type a non-integer value
    for the answer to see such an error.
    """
    print_exercise_name()

    # TODO 0: Read, discuss, and understand the following code.

    # Obtain user input as a string.
    s = input("Enter the answer, which must be an integer: ")
    # Convert the user input to an integer.
    answer = int(s)
    # Display the results. What could go wrong?
    print("The answer is {}.".format(answer))


def exercise1():
    """
    Demonstrate a basic try/except statement.

    The basic try/except statement allows you to write code to be executed when an error or
    exception occurs.  Run the given code, type non-integer input, and discuss the results.
    """
    print_exercise_name()

    # TODO 1: Read, discuss, and understand the following code.

    # Obtain user input as a string.
    s = input("Enter the answer, which must be an integer: ")

    try:
        # Convert the user input to an integer.
        answer = int(s)

    except ValueError:
        # If there is an exception, do this instead.
        answer = 42

    print("The answer is {}.".format(answer))


def exercise2():
    """
    Demonstrate the full try/except statement.

    The try/except statement has many clauses.  Run the given code several times, providing
    input values to cause each clause of the try/except statement execute, and discuss the results.
    """
    print_exercise_name()

    # TODO 2: Read, discuss, and understand the following code.

    try:
        # If any of the next three lines raises an exception,
        # the program will jump directly to the except clause.
        total = int(input("Enter sum of all scores: "))
        count = int(input("Enter number of scores:  "))
        avg = total / count

        # If any of the above lines raise an exception,
        # this line of codes does not execute.
        print("{} / {} = {}".format(total, count, avg))

    except ValueError:
        print("Integer values only!")

    except ZeroDivisionError:
        print("Zero? Really?!")

    except:
        print("Something bad happened.")

    else:
        print("Only happens if NO exceptions are raised.")

    finally:
        print("Always happens after the code in the try block, no matter what.")


def exercise3():
    """
    Input validation is a common use of the try/except statement.

    A common use of the try/except statement is input validation.  Run the given code several
    times, discussing the results.  Note: This is not the most robust input validation code
    possible; it is provided as an example of the try/except statement.
    """
    print_exercise_name()

    # TODO 3: Read, discuss, and understand the following code.

    # Initial, unused values to make PyCharm happy.
    a = b = c = 0

    # Loop until the valid_input flag is set to True.
    valid_input = False
    while not valid_input:
        try:
            s = input("Enter three integers, separated by spaces: ")
            data = s.split()
            a = int(data[0])
            b = int(data[1])
            c = int(data[2])

        except (ValueError, IndexError):
            print("Invalid input; please try again.")

        else:
            # The code reaches this point if no exceptions were raised.
            valid_input = True

    # When the code gets to here, a, b, and c are valid integers.
    if a ** 2 + b ** 2 == c ** 2:
        print("{}, {}, and {} form a right triangle.".format(a, b, c))
    else:
        print("{}, {}, and {} do not form a right triangle.".format(a, b, c))


def exercise4():
    """
    Demonstrates raising an error or exception.

    A function can raise an exception; in this case the average function raises an
    exception if it is given an empty list.

    a.	Run the given code and discuss the results.

    b.	What other errors could be raised by the average function?  Add code to the exercise4
        function that would cause additional errors and add except clauses to handle them.
    """
    print_exercise_name()

    # TODO 4: Read, discuss, and understand the following code.

    try:
        # Calculating the average of an empty list raises an exception.
        # What other exceptions could be raised by the average function?
        # print( average( [] ) )

        # All list items must support addition or the sum function will raise a TypeError.
        print(average([1, 2, "three", 4]))

    except RuntimeError as e:
        print("ERROR: {}".format(e))

    except TypeError:
        print("ERROR: The sum of the list could not be calculated.")


def average(data):
    """
    Calculates and returns the average of a list of numeric values.

    :param list data: The list of numeric values to be averaged.
    :raises RuntimeError: If the list of numeric values is empty.
    :return: The average of the list of numeric values.
    :rtype: float
    """
    if len(data) == 0:
        raise RuntimeError("No values to average.")

    # If the above code raises an exception, this code does not execute.
    return sum(data) / len(data)


def exercise5():
    """
    A class can also raise an exception.

    a.	Run the given code and discuss the results.

    b.	What possible errors might be associated with the name attribute?  Add code to
        the exercise5 function that would cause additional errors and add except clauses
        to handle them.
    """
    print_exercise_name()

    # TODO 5: Read, discuss, and understand the following code.

    try:
        p = Person("Douglas Adams", 42)
        print(p)

        p = Person("Yoda", 900)
        print(p)

    except ValueError as e:
        print("ERROR: {}".format(e))


class Person:
    """A class to represent a person."""

    def __init__(self, name, age):
        """
        Create the Person object.

        :param str name: The person's name.
        :param int age: The person's age.
        :raises ValueError: If the age is invalid.
        """
        if name is None or len(name) == 0:
            raise ValueError("Must provide name.")
        elif len(name.split()) < 2:
            raise ValueError("Must provide at least first and last name.")
        else:
            self.name = name

        if age < 0:
            # Barring time travel, a negative age does not make sense.
            raise ValueError("Age must be positive.")
        elif age <= 125:
            # Between 0 and 125 seems more or less reasonable.
            self.age = age
        else:
            # https://en.wikipedia.org/wiki/Oldest_people
            raise ValueError("Age must be reasonable.")

    def __str__(self):
        """Build and return a string representation of the person."""
        return "{} is {} years old.".format(self.name, self.age)


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
