#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

__author__ = "Bret Crew"
__instructor__ = "Dr Bower"
__date__ = "30 Cct 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 28: Classes & Objects from our online textbook
-	Watch this video
    https://www.youtube.com/watch?v=trOZBgZ8F_c&list=PLirBjzN2aCJlPHizH6KIpzCA5IRDtiB50&index=8

Lesson Objectives
-	Create simple classes with multiple attributes
-	Create multiple instances of simple classes
-	Access and manipulate object attributes


"""

import math
import random
import turtle

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = False  # Set to True for fast, non-animated turtle movement.

COLORS = ["red", "green", "blue", "yellow", "cyan", "magenta", "white", "black"]


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    exercise2()


def exercise0():
    """
    Point Class Demo – Work with a partner to read, discuss, and understand the given code.
    Be sure to ask other classmates and/or your instructor if anything is unclear.

    a.	In the space "TODO 0a", is a definition of a Point class similar to the author's
        Point class.  However, rather than always creating a new Point object at the origin,
        random values are used for the x and y attributes.  Read, discuss, and understand this code.

    b.	In the space "TODO 0b", is code that creates a list of Point objects and then uses the
        attributes of these Point objects to write a message about each Point object, moves the
        turtle to the location specified by the Point object, and draws a dot on each point.
        Read, discuss, and understand this code.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # TODO 0b: Read, discuss, and understand the following code.
    points = []  # An empty list to be filled with Point objects.
    for _ in range(8):
        p = Point()  # Calls the Point object's __init__ method to create a point object.
        points.append(p)  # Appends the point to the list of point objects.

    # Loop through the list of Point objects and use the artist turtle to draw them.
    for p in points:
        # Have the writer turtle display the Point object's x and y values.
        writer.clear()
        writer.write("Moving to point ({}, {})...".format(p.x, p.y),
                     align="center", font=("Times", FONT_SIZE, "bold"))

        # Use the Point object's x and y values to set the heading.
        artist.setheading(artist.towards(p.x, p.y))
        # Use the Point object's x and y values to move the turtle.
        artist.setposition(p.x, p.y)
        # Draw a dot at the point.
        artist.dot(4)

    # Put things back when finished.
    writer.clear()
    artist.home()
    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# TODO 0a: Read, discuss, and understand the following code.
class Point:
    """Point class for representing (x,y) coordinates."""

    def __init__(self):
        """Create a new Point with random x and y values."""
        # Uses random.randint to create values within the margins of the turtle screen.
        self.x = random.randint(-WIDTH // 2 + MARGIN, WIDTH // 2 - MARGIN)
        self.y = random.randint(-HEIGHT // 2 + MARGIN, HEIGHT // 2 - MARGIN)


def exercise1():
    """
    Spot Class – In this exercise you will create a Spot class similar to the Point class
    with an additional attribute.

    a.	In the space "TODO 1a", define a Spot class with an __init__ method that defines
        random x and y attributes and also a color attribute.  Use the random.choice method
        to select a random color from the COLORS list defined at the top of the file.

    b.	In the space "TODO 1b", write code that creates a list of Spot objects and then uses
        the attributes of these Spot objects to write a message about each Spot object,
        moves the turtle to the location specified by the Spot object, and draws a dot
        of the appropriate color with a 32 pixel diameter.  (Recall the turtle's dot()
        method can accept a second parameter specifying the color.)
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # TODO 1b: In the space below, use the class as described
    spots = []
    for i in range(8):
        s = Spot()
        spots.append(s)
    for spot in spots:
        writer.write("Moving to point ({}, {}) to draw a {} dot...".format(spot.x, spot.y, spot.color),
                     align="center", font=("Times", FONT_SIZE, "bold"))
        artist.setheading(artist.towards(spot.x, spot.y))
        artist.setposition(spot.x, spot.y)
        artist.dot(32, spot.color)
        writer.clear()

    # Put things back when finished.
    writer.clear()
    artist.home()
    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# TODO 1a: In the space below this comment, write the class as described
class Spot:
    """Point class for representing (x,y) coordinates."""

    def __init__(self):
        """Create a new Point with random x and y values."""
        # Uses random.randint to create values within the margins of the turtle screen.
        self.x = random.randint(-WIDTH // 2 + MARGIN, WIDTH // 2 - MARGIN)
        self.y = random.randint(-HEIGHT // 2 + MARGIN, HEIGHT // 2 - MARGIN)
        self.color = random.choice(COLORS)


def exercise2():
    """
    Raindrop Class – In this exercise you will create a Raindrop class similar to the Point
    class with an additional attribute.

    a.	In the space "TODO 2a", define a Raindrop class with an __init__ method that defines
        random x and y attributes and also a radius attribute.  The radius should be a random
        value between MARGIN and MARGIN * 2. (Recall the parameter to the turtle's dot()
        method is a diameter.)

    b.	In the space "TODO 2b", write code that creates Raindrop objects, moves the turtle and
        draws them with a blue dot, and appends them to a list of Raindrop objects. This should
        continue until the total area of the Raindrop objects is greater than the total area of
        the turtle screen (though the entire screen will not likely be covered as Raindrop objects
        will overlap).

    Additionally, when a new Raindrop object is created, check all existing Raindrop objects to see
    if there is overlap.  When the new Raindrop object overlaps an existing Raindrop object,
    modify the existing Raindrop object's radius such that its area becomes the sum of the two
    Raindrop object's areas.  Then move the turtle and re-draw the existing Raindrop object and
    update the total raindrop area appropriately.  Still keep the new Raindrop.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # TODO 2b: In the space below, use the class as described
    raindrops = []
    area = 0
    while area <= HEIGHT * WIDTH:
        r = Raindrop()
        area += math.pi * (r.radius / 2) * (r.radius / 2)
        writer.write("Moving to point ({}, {}) to draw a {} pixel drop...".format(r.x, r.y, r.radius),
                     align="center", font=("Times", FONT_SIZE, "bold"))
        artist.setheading(artist.towards(r.x, r.y))
        artist.setposition(r.x, r.y)
        artist.dot(2 * r.radius, "blue")
        writer.clear()
        for drop in raindrops:
            if artist.distance(drop.x, drop.y) <= r.radius + drop.radius:
                combined = math.pi * (r.radius) * (r.radius) + math.pi * (drop.radius) * (drop.radius)
                rad = math.sqrt(combined / math.pi)
                drop.radius = rad
                artist.setheading(artist.towards(drop.x, drop.y))
                artist.goto(drop.x, drop.y)
                artist.dot(2*rad, "blue")
                area += math.pi * (rad **2)

        raindrops.append(r)

    # Put things back when finished.
    writer.clear()
    artist.home()
    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()

    # TODO 2a: In the space below this comment, write the class as described


class Raindrop:
    def __init__(self):
        """Create a new Point with random x and y values."""
        # Uses random.randint to create values within the margins of the turtle screen.
        self.x = random.randint(-WIDTH // 2 + MARGIN, WIDTH // 2 - MARGIN)
        self.y = random.randint(-HEIGHT // 2 + MARGIN, HEIGHT // 2 - MARGIN)
        self.radius = random.randint(MARGIN, MARGIN * 2)


"""
Challenge Exercises

1.	Make a copy of your exercise2() function and name it exercise3().  Modify this copy
    such that when a new Raindrop object overlaps an existing Raindrop object, the two
    Raindrop objects are "combined" and only one is kept in the list.  Combine the Raindrop
    objects however you see fit.

2.	Complete unfinished exercises from any previous lab; in particular, the Dictionaries lab.
"""


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")
    # Lift the artist's pen and slow it down to see the movements from object to object.
    artist.penup()
    artist.speed("slowest")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
