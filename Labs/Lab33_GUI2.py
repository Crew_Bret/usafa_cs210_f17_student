#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

__author__ = "Bret Crew"
__instructor__ = "Dr Bower"
__date__ = "14 nov 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 33: Model View Controller

Lesson Objectives
-	Identify the model (data) used in a program
-   Manipulate the model through controllers
-   Reflect changes to the model through viewers

"""

import tkinter as tk
from tkinter import scrolledtext  # For use with challenge problem


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    exercise1()
    # exercise2()


def exercise0():
    """
    Creating a MadLib app - This exercise helps you understand how to create a GUI app
    by stepping you through the code in the order it was created.  This sequence of
    steps for creating a GUI app is worth mimicking when creating your own apps.

    Best Practice: The following is a recommended sequence of steps when building a
    GUI application.

        1. Sketch out what you want the GUI to look like.
        2. Define the Model that represents your application.
        3. Create the View and Controller widgets in the GUI.
        4. Stub out callback functions for any Control widgets.
        5. Stub out callback functions for changes to model data.
        6. Connect model data to their callback functions
           (called "traces" in Python and Tk).
        7. Set initial values for your model.
        8. Flesh out the functions for complete functionality.

    """
    print_exercise_name()

    # Create the entire GUI program
    # program = MadLibAppTogether()
    program = MadLibAppInClass()

    # Start the GUI event loop
    program.window.mainloop()


class MadLibAppInClass:
    """
    STEP 1: Sketch out what you want the GUI to look like.

            +-----------------------------------------------------------+
            |  MadLib                                       [_] [ ] [X] |
            +-----------------------------------------------------------+
            | Noun:       [ banana                   ]                  |
            | Scooby Doo tripped over the banana and tackled the ghost. |
            |  +--------+           +--------+                          |
            |  |  Clear |           |  Quit  |                          |
            |  +--------+           +--------+                          |
            +-----------------------------------------------------------+

    NOTE: At this point you do not need to have alignment and all the details
    perfect, especially if you are sketching it out in code comment as with
    the above example.  For more complicated apps, you may want to sketch
    out your app in PowerPoint or some other drawing tool.

    """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("MadLib")

        # STEP 2: Data (model)
        self.madlib_blank = "Scooby Doo tripped over the _{}_ and tackled the ghost."
        self.madlib_full = tk.StringVar()  # Contains filled-in madlib
        self.word1 = tk.StringVar()

        # View / Control
        self.create_widgets()

        # STEP 6: Connect model data to their callback functions
        pass

        # STEP 7: Set initial values for your model.
        pass

    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.
        lbl_noun = tk.Label(self.window, text="Noun:")
        lbl_noun.grid(row=0, column=0, sticky=tk.W)
        txt_noun = tk.Entry(self.window, textvariable=self.word1)
        txt_noun.grid(row=0, column=1, sticky=tk.W + tk.E)

        lbl_full = tk.Label(self.window, textvariable=self.madlib_full)
        lbl_full.grid(row=1, column=0, columnspan=2)

        clear_button = tk.Button(self.window, text="Clear", command=self.clear_button_clicked)
        clear_button.grid(row=3, column=0)
        quit_button = tk.Button(self.window, text="Quit", command=self.window.destroy)
        quit_button.grid(row=3, column=1)

        # STEP 4: Stub out callback functions for control widgets
        # def clear_button_clicked:
        #     print("Hel2")
        # definitiojle-r- # <-- here is where I passed out.

    # STEP 5: Stub out callback functions for changes to model data
    pass


class MadLibAppCompleted():
    """
    STEP 1: Sketch out what you want the GUI to look like.

            +-----------------------------------------------------------+
            |  MadLib                                       [_] [ ] [X] |
            +-----------------------------------------------------------+
            | Noun:       [ banana                   ]                  |
            | Scooby Doo tripped over the banana and tackled the ghost. |
            |  +--------+           +--------+                          |
            |  |  Clear |           |  Quit  |                          |
            |  +--------+           +--------+                          |
            +-----------------------------------------------------------+

    NOTE: At this point you do not need to have alignment and all the details
    perfect, especially if you are sketching it out in code comment as with
    the above example.  For more complicated apps, you may want to sketch
    out your app in PowerPoint or some other drawing tool.

    """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("MadLib")

        # STEP 2: Data (model)
        self.madlib_blank = "Scooby Doo tripped over the _{}_ and tackled the ghost."
        self.madlib_full = tk.StringVar()  # Will contain the filled-in madlib
        self.word1 = tk.StringVar()  # Will contain the word the user types in

        # View / Control
        self.create_widgets()

        # STEP 6: Connect model data to their callback functions
        self.word1.trace("w", self.word1_changed)

        # STEP 7: Set initial values for your model.
        self.word1.set("NOUN")  # Initial value

    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.

        # User input
        lbl_noun = tk.Label(self.window, text="Noun:")
        lbl_noun.grid(row=0, column=0, sticky=tk.W)
        txt_noun = tk.Entry(self.window, textvariable=self.word1)
        txt_noun.grid(row=0, column=1, sticky=tk.W + tk.E)

        # Completed madlib
        lbl_full = tk.Label(self.window, textvariable=self.madlib_full)
        lbl_full.grid(row=1, column=0, columnspan=2)

        # Buttons
        clear_button = tk.Button(self.window, text="Clear", command=self.clear_button_clicked)
        clear_button.grid(row=3, column=0)
        quit_button = tk.Button(self.window, text="Quit", command=self.window.destroy)
        quit_button.grid(row=3, column=1)
        # NOTE:
        # The destroy function that is built in to the tk.Tk() window quits an application,
        # and is a convenient callback function for a quit button IF you do not want
        # to first prompt the user to save any work, etc.
        # NOTE:
        # No parentheses after the command= callback function.  You are telling the
        # button which function you want to get called sometime in the future, not actually
        # calling the function this very moment.
        # NOTE:
        # These widgets are not saved with something like self.lbl_noun.  These are local
        # variables that disappear after the create_widgets() function ends.  Only save
        # widgets with a self.xxxx if you need to manipulate those widgets somewhere else
        # in your application.

    # STEP 4: Stub out callback functions for control widgets
    # NOTE: Use meaningful function names such as this "clear_button_clicked" that
    # communicate WHAT was clicked and WHEN it was clicked (past tense) in relation
    # to when the function is called.
    def clear_button_clicked(self):
        # print("clear_button_clicked")  # Stubbed out initially
        self.word1.set("___")

    # STEP 5: Stub out callback functions for changes to model data
    # NOTE: When a trace callback function is called, it passes three arguments
    # that do not tend to be of much use.  Feel free to use argument names
    # such as the underscores shown below, to indicate to someone reading your
    # code that you do not care about those parameters.
    # If you're curious about them, read this Stack Overflow posting:
    # https://stackoverflow.com/questions/29690463/what-are-the-arguments-to-tkinter-variable-trace-method-callbacks
    def word1_changed(self, _, __, ___):
        # print("word1_changed")  # Stubbed out initially
        word = self.word1.get()  # Retrieve the string value from the tk.StringVar
        madlib_filled = self.madlib_blank.format(word)  # Plug in the new word
        self.madlib_full.set(madlib_filled)  # Set the final text


def exercise1():
    """
    Counter Application - In this exercise you will be building a graphical app that counts
    up and down as a user clicks on add/subtract buttons.  Here is a sketch of the GUI (STEP 1):

            +---------------------------------------+
            |  Count: 0                 [_] [ ] [X] |
            +---------------------------------------+
            |                   0                   |
            |        +----------------------+       |
            |        |  Add one to counter  |       |
            |        +----------------------+       |
            |    +-----------------------------+    |
            |    |  Subtract one from counter  |    |
            |    +-----------------------------+    |
            |               +--------+              |
            |               |  Quit  |              |
            |               +--------+              |
            +---------------------------------------+

    Functionality:
    When the button "Add one to counter" is clicked, the counter should increment by one,
    the label above the button should reflect the new value, and the title bar should
    change to read "Count: x" where x is the new value.  When the button "Subtract one
    from counter" is clicked, the counter should decrement by one, and the same changes
    should be reflected.

    Hint: Although a tk.Label can be automatically bound to a tk.IntVar (thank you, tkinter),
    the title bar cannot be similarly bound.  You will need to update the title bar with
    self.window.title("new title here") when the counter changes.  You can put a trace
    on the model's tk.IntVar to be notified of changes.

    Remember the steps:

        1. Sketch out what you want the GUI to look like.
        2. Define the Model that represents your application.
        3. Create the View and Controller widgets in the GUI.
        4. Stub out callback functions for any Control widgets.
        5. Stub out callback functions for changes to model data.
        6. Connect model data to their callback functions
           (called "traces" in Python and Tk).
        7. Set initial values for your model.
        8. Flesh out the functions for complete functionality.

    Complete each of the seven steps in order in the spaces marked below.
    """
    print_exercise_name()

    # Create the entire GUI program
    program = CounterApp()

    # Start the GUI event loop
    program.window.mainloop()


class CounterApp:
    """ An app that counts up and down. """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Counter")

        # STEP 2: Data (model)
        self.number = 0
        self.num = tk.IntVar()

        # View / Control
        self.create_widgets()

        # STEP 6: Connect model data to their callback functions
        self.num.trace("w", self.num_changed)


        # STEP 7: Set initial values for your model.
        self.num.set(0)

    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.
        self.lbl_num = tk.Label(self.window, text=str(self.num.get()))
        self.lbl_num.grid(row=1, column=0)
        # Buttons
        btn_add = tk.Button(self.window, text="Add one to counter", command=self.btn_add_clicked)
        btn_add.grid(row=2, column=0)
        btn_sub = tk.Button(self.window, text="Subtract one from counter", command=self.btn_add_clicked)
        btn_sub.grid(row=3, column=0)
        quit_button = tk.Button(self.window, text="Quit", command=self.window.destroy)
        quit_button.grid(row=4, column=0)

    # STEP 4: Stub out callback functions for control widgets
    def btn_add_clicked(self):
        self.number +=1
        self.num.set(self.number)
        print(self.num.get())



    def btn_sub_clicked(self):
        self.number -= 1
        self.num.set(self.number)
    # STEP 5: Stub out callback functions for changes to model data
    def num_changed(self, _, __, ___):
        number = self.num.get()
        number += 1
        self.num.set(number)


def exercise2():
    """
    Grow App - In this exercise you will be building a graphical app that
    expands the window when the grow button is clicked.  The grow effect
    is achieved by changing the padding of the grow button.

    Here is a sketch of the GUI (STEP 1):

            +------------------------------------+
            |  Grow                  [_] [ ] [X] |
            +------------------------------------+
            |    +--------------------------+    |
            |    |  Make the window bigger  |    |
            |    +--------------------------+    |
            |            Padding: 5              |
            |            +--------+              |
            |            |  Quit  |              |
            |            +--------+              |
            +------------------------------------+

    Functionality:
    When the "Make the window bigger" button is clicked, the app should increase the value
    of a model variable representing the desired padding.  When the model variable changes,
    that should cause the app to reassign a padding value to the button.

    Hints:
        1. Model: Use a tk.IntVar to represent the padding value.
        2. View: Let a tk.Label be bound to this value (textvariable=self.xxxx) to reflect
           the current value ("5" in the sketch above)
        3. Control: When the grow button is clicked, only modify the model's tk.IntVar.
           Do not make changes to the padding here.
        3. View: Use a trace on the tk.IntVar to notice when the model has changed, and update
           the button padding then.

           Example of how to change padding:
            self.grow_button.grid(pady=10, padx=10)

    Complete each of the seven steps in order in the spaces marked below.
    """
    print_exercise_name()

    # Create the entire GUI program
    program = GrowWindowApp()

    # Start the GUI event loop
    program.window.mainloop()


class GrowWindowApp:
    """ App whose window grows when the button is clicked. """

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Grow")

        # STEP 2: Data (model)
        pass

        # View / Control
        self.create_widgets()

        # STEP 6: Connect model data to their callback functions
        pass

        # STEP 7: Set initial values for your model.
        pass

    def create_widgets(self):
        # STEP 3. Create the View and Controller widgets in the GUI.
        pass

    # STEP 4: Stub out callback functions for control widgets
    pass

    # STEP 5: Stub out callback functions for changes to model data
    pass


"""
Challenge Exercise

Go back to the MadLib app and upgrade it to be able to read in MadLibs from
your Data folder and support multiple words being entered and the entire
MadLib being displayed.

Hint: For a multiline text box, use the BindableTextArea widget that we
have created below.  It supports multiline text and can be bound to a tk.StringVar.
If you do not want people editing the text, you can disable it like so:

    text_area = BindableTextArea(some_frame, width=40, height=5,
                                 state=tk.DISABLED, textvariable=some_variable)

"""


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

class BindableTextArea(tk.scrolledtext.ScrolledText):
    """
    A multi-line tk widget that is bindable to a tk.StringVar.

    You will need to import ScrolledText like so:

        from tkinter import scrolledtext
    """

    class _SuspendTrace:
        """ Used internally to suspend a trace during some particular operation. """

        def __init__(self, parent):
            self.__parent = parent  # type: BindableTextArea

        def __enter__(self):
            """ At beginning of operation, stop the trace. """
            if self.__parent._trace_id is not None and self.__parent._textvariable is not None:
                self.__parent._textvariable.trace_vdelete("w", self.__parent._trace_id)

        def __exit__(self, exc_type, exc_val, exc_tb):
            """ At conclusion of operation, resume the trace. """
            self.__parent._trace_id = self.__parent._textvariable.trace("w", self.__parent._variable_value_changed)

    def __init__(self, parent, textvariable: tk.StringVar = None, **kw):
        tk.scrolledtext.ScrolledText.__init__(self, parent, **kw)
        self._textvariable = None  # type: tk.StringVar
        self._trace_id = None
        if textvariable is None:
            self.textvariable = tk.StringVar()
        else:
            self.textvariable = textvariable
        self.bind("<KeyRelease>", self._key_released)

    @property
    def textvariable(self):
        return self._textvariable

    @textvariable.setter
    def textvariable(self, new_var):
        # Delete old trace if we already had a bound textvariable
        if self._trace_id is not None and self._textvariable is not None:
            self._textvariable.trace_vdelete("w", self._trace_id)

        # Set up new textvariable binding
        self._textvariable = new_var
        self._trace_id = self._textvariable.trace("w", self._variable_value_changed)

    def _variable_value_changed(self, _, __, ___):

        # Must be in NORMAL state to respond to delete/insert methods
        prev_state = self["state"]
        self["state"] = tk.NORMAL

        # Replace text
        text = self._textvariable.get()
        self.delete("1.0", tk.END)
        self.insert(tk.END, text)

        # Restore previous state, whatever that was
        self["state"] = prev_state

    def _key_released(self, evt):
        """ When someone types a key, update the bound text variable. """
        text = self.get("1.0", tk.END)
        with BindableTextArea._SuspendTrace(self):  # Suspend trace to avoid infinite recursion
            if self.textvariable:
                self.textvariable.set(text)


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
