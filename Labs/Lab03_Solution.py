#!/usr/bin/env python3
"""
CS 210, Introduction to Programming, Lab 03 - More Turtles.
"""

import turtle

__author__ = "Dr Bower"
__instructor__ = "Dr Bower"
__date__ = "16 Aug 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
- Read Lesson 3: More Turtles from our online textbook
- Watch the videos embedded in the above reading

Lesson Objectives
- Introduce the range function
- Reinforce Python turtles

Note: Unless specifically requested, functions in lab exercises do not require docstrings.
"""

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.


def main():
    """Main program to call each individual lab exercise."""

    # Print the docstring at the top of the file so your instructor can see your name.
    print( __author__, __doc__ )

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    # exercise1()
    # exercise2()
    # exercise3()
    # exercise4()
    # exercise5()
    # exercise6()
    # exercise7()


def exercise0():
    """Example code."""
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    artist.up()
    for size in range( 5, 90, 2 ):  # start with size = 5 and grow by 2
        artist.stamp()  # leave an impression on the canvas
        artist.forward( size )  # move along
        artist.right( 24 )  # and turn her

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise1():
    """
    In the workspace below, write a for loop with the range() function
    to print the numbers 37 through 42, each on a separate line.
    """
    print_exercise_name()

    for i in range( 37, 43 ):
        print( i )


def exercise2():
    """
    In the workspace below, write a for loop with the range() function
    to print the numbers 42 through 37, each on a separate line.
    """
    print_exercise_name()

    for i in range( 42, 36, -1 ):
        print( i )


def exercise3():
    """
    In the workspace below, write a for loop with the range() function
    to print the multiples of three between 1 and 30, inclusive.
    """
    print_exercise_name()

    for i in range( 3, 31, 3 ):
        print( i )


def exercise4():
    """
    In the workspace below, draw a five-pointed star without strokes through the center.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    artist.left( 36 )
    for side in range( 5 ):
        # Draw each inner corner of the star.
        artist.forward( 64 )
        artist.right( 72 )
        artist.forward( 64 )
        artist.left( 144 )

    artist.penup()
    artist.back( 300 )
    artist.pendown()
    for side in range( 5 ):
        # Draw five straight lines, lifting the pen in the middle.
        artist.forward( 70 )
        artist.penup()
        artist.forward( 40 )
        artist.pendown()
        artist.forward( 70 )
        artist.left( 144 )

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise5():
    """
    In the workspace below, draw square inscribed in a circle.

    Note: This was the exercise in 2015; this year the website shows
    the five pointed star inscribed in a circle. Not sure why.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Allow the user to enter the length of a side of the square.
    side = int( screen.numinput( "Input", "Size of the inner square?" ) )

    # Move the turtle so the square will be centered.
    artist.penup()
    artist.backward( side // 2 )
    artist.left( 90 )
    artist.backward( side // 2 )
    artist.right( 90 )
    artist.pendown()

    # Draw the square.
    for _ in range( 4 ):
        artist.forward( side )
        artist.left( 90 )

    # Calculate the diameter of the circle based on the square's side length.
    diameter = (side * side + side * side) ** 0.5

    # Turn and draw the circle (using radius not diameter).
    artist.right( 45 )
    artist.circle( diameter // 2 )
    artist.left( 45 )

    # Move the turtle back to the center.
    artist.penup()
    artist.forward( side // 2 )
    artist.left( 90 )
    artist.forward( side // 2 )
    artist.right( 90 )
    artist.pendown()

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise6():
    """
    In the workspace below, draw a stop sign.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    artist.color( "black", "red" )

    # Use a Pythagorean triple so everything looks pretty.
    a, b, c = 30, 40, 50

    # Move and draw the inner, filled octagon.
    artist.penup()
    artist.right( 90 )
    artist.forward( a + b )
    artist.left( 90 )
    artist.pendown()
    artist.begin_fill()
    for _ in range( 8 ):
        artist.forward( b )
        artist.left( 45 )
        artist.forward( b )
    artist.end_fill()

    # Move and draw the outer octagon.
    artist.penup()
    artist.right( 90 )
    artist.forward( 8 )
    artist.left( 90 )
    artist.pendown()
    for _ in range( 8 ):
        artist.forward( b + 3 )
        artist.left( 45 )
        artist.forward( b + 3 )

    # Use the writer turtle to draw the STOP text.
    writer.color( "white" )
    writer.setposition( 0, -16 )
    writer.write( "STOP", align="center", font=("Arial", 48, "bold") )

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise7():
    """
    In the workspace below, draw concentric squares.
    Use the for loop as much as possible.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()
    artist.penup()

    # Base all drawing in the size variable.
    size = 256

    # Draw the head.
    artist.color( "yellow" )
    artist.dot( size )
    # Draw the smile with overlapping dots.
    artist.color( "black" )
    artist.dot( size * 3 // 5 )
    artist.color( "yellow" )
    artist.setposition( 0, size // 12 )
    artist.dot( size * 3 // 5 )
    # Draw the nose.
    artist.color( "black" )
    artist.home()
    artist.dot( size // 10 )
    # Draw the eyes.
    artist.setposition( -size // 6, size // 4 )
    artist.dot( size // 8 )
    artist.setposition( size // 6, size // 4 )
    artist.dot( size // 8 )
    # Say hello.
    font_size = size // 8
    writer.setposition( 0, -HEIGHT // 2 + font_size )
    writer.write( "Hello, World!", align="center", font=("Arial", font_size, "bold") )

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# _____________________________________________________ #
#  _____          _   _       _     ______    _ _ _     #
# |  __ \        | \ | |     | |   |  ____|  | (_) |    #
# | |  | | ___   |  \| | ___ | |_  | |__   __| |_| |_   #
# | |  | |/ _ \  | . ` |/ _ \| __| |  __| / _` | | __|  #
# | |__| | (_) | | |\  | (_) | |_  | |___| (_| | | |_   #
# |_____/ \___/  |_| \_|\___/_\__| |______\__,_|_|\__|  #
#  / ____|        | |      |  _ \     | |               #
# | |     ___   __| | ___  | |_) | ___| | _____      __ #
# | |    / _ \ / _` |/ _ \ |  _ < / _ \ |/ _ \ \ /\ / / #
# | |___| (_) | (_| |  __/ | |_) |  __/ | (_) \ V  V /  #
#  \_____\___/ \__,_|\___| |____/ \___|_|\___/ \_/\_/   #
# _____________________________________________________ #


def turtle_setup():
    """
    Sets up the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup( WIDTH, HEIGHT, MARGIN, MARGIN )
    screen.bgcolor( "SkyBlue" )

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape( "turtle" )

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay( 0 )
        artist.hideturtle()
        artist.speed( "fastest" )
        writer.hideturtle()
        writer.speed( "fastest" )

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading( 90 )  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition( 0, HEIGHT // 2 - FONT_SIZE * 2 )  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
