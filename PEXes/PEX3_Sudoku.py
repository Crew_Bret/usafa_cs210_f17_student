#!/usr/bin/env python3
"""
Sudoku manipulator - PEX 3
CS 210, Introduction to Programming
"""

import easygui  # For easygui.fileopenbox.
import os  # For os.path.basename.

__author__ = "C3C Bret Crew"  # Your name. Ex: John Doe
__section__ = "M6A"  # Your section. Ex: M1
__instructor__ = "Dr. Bower"  # Your instructor. Ex: Lt Col Doe
__date__ = "10 Oct 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """ No help received in the creation of this PEX """  # Multiple lines OK with triple quotes

DATA_DIRECTORY = "../Data/Sudoku"


def main():
    """
    Main program to do Sudoku, so here's a Sudoku haiku:

    One through nine in place
    To open the matrix door
    Let logic guide you
    """
    filename = easygui.fileopenbox("../Data/Sudoku")
    while filename is not None:
        sudoku_data = open_sudoku(filename)
        if is_solved(sudoku_data):
            solved = True
        elif not is_solved(sudoku_data):
            solved = False
        if is_valid(sudoku_data):
            valid = True
        elif not is_solved(sudoku_data):
            valid = False
        print_sudoku(sudoku_data)
        if valid and solved:
            print("The puzzle IS valid and IS solved.")
        elif valid and not solved:
            print("The puzzle IS valid and is NOT solved.")
        elif not valid and not solved:
            print("The puzzle is NOT valid and is NOT solved.")
        filename = easygui.fileopenbox()

        # TODO 1b: Demonstrate the str_sudoku function with the given puzzles.
        # print( str_sudoku( PUZZLE ) )
        # print( str_sudoku( PUZZLE_SOLVED ) )

        # TODO 2b: Demonstrate the print_sudoku function with the given puzzles.
        # print_sudoku(PUZZLE)
        # print_sudoku(PUZZLE_SOLVED)

        # TODO 3b: Demonstrate the create_sudoku puzzle with the given data.
        # print_sudoku(create_sudoku(DATA))
        # print_sudoku(create_sudoku(DATA_SOLVED))

        # TODO 4b: Demonstrate the open_sudoku function with the given files.
        # for test_filename in ["Sudoku_Blank.txt", "Sudoku00.txt", "Sudoku01.txt", "Sudoku02.txt", "Sudoku03.txt"]:
        #     fullpath = os.path.join(DATA_DIRECTORY, test_filename)
        #     sudoku_data = open_sudoku(fullpath)
        #     print_sudoku(sudoku_data)


        # TODO 5b: Demonstrate the is_solved function with the given puzzles.
        # print(is_solved(PUZZLE), is_solved(PUZZLE_SOLVED))

        # TODO 6b: Demonstrate the is_valid function with the given puzzles.
        # print( is_valid( PUZZLE ), is_valid( PUZZLE_SOLVED ), is_valid( PUZZLE_INVALID ) )

        # TODO 7: Implement the main program as described


# TODO 1a: Implement the str_sudoku function as described
def str_sudoku(puzzle):
    """
    Creates a string of puzzle values suitable for printing to a file.

    The string created by this function is formatted such that it could be
    passed to create_sudoku function and recreate the same puzzle.

    Note: This function DOES NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: A string suitable for printing to a file or passing to create_sudoku.
    :rtype: str
    """
    s = ""
    for row in puzzle:
        for char in row:
            s = s + str(char)
            s += " "
    return s


# TODO 2a: Implement the print_sudoku function as described
def print_sudoku(puzzle):
    """
    Prints the nested list structure in pretty rows and columns.

    For example, PUZZLE (bottom of this file) would print as follows:
    +===+===+===+===+===+===+===+===+===+
    # 1 | 8 |   # 6 |   | 9 #   | 5 | 7 #
    +---+---+---+---+---+---+---+---+---+
    # 5 |   |   #   |   |   #   |   | 3 #
    +---+---+---+---+---+---+---+---+---+
    #   |   | 2 #   | 8 |   # 4 |   |   #
    +===+===+===+===+===+===+===+===+===+
    # 7 |   |   # 8 | 4 | 5 #   |   | 1 #
    +---+---+---+---+---+---+---+---+---+
    #   |   | 3 # 2 |   | 1 # 9 |   |   #
    +---+---+---+---+---+---+---+---+---+
    # 2 |   |   # 9 | 6 | 3 #   |   | 5 #
    +===+===+===+===+===+===+===+===+===+
    #   |   | 1 #   | 5 |   # 8 |   |   #
    +---+---+---+---+---+---+---+---+---+
    # 8 |   |   #   |   |   #   |   | 4 #
    +---+---+---+---+---+---+---+---+---+
    # 9 | 4 |   # 3 |   | 8 #   | 7 | 2 #
    +===+===+===+===+===+===+===+===+===+

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: None
    """
    # print("+===+===+===+===+===+===+===+===+===+")
    j = 0

    for row in puzzle:
        one_row = row
        for i in range(9):
            if one_row[i] == 0:
                one_row[i] = " "
        s = ""
        k = 0
        if j == 0 or j % 3 == 0:
            print("+===+===+===+===+===+===+===+===+===+")  # Draws a line every third row.
        else:
            print("+---+---+---+---+---+---+---+---+---+")
        j += 1
        for char in one_row:  # This code draws the correct separation characters
            if k == 0 or k % 3 == 0:
                s += "# {} ".format(char)
            else:
                s += "| {} ".format(char)
            k += 1
        s += "#"
        print(s)
    print("+===+===+===+===+===+===+===+===+===+")

    print()


# TODO 3a: Implement the create_sudoku function as described
def create_sudoku(data):
    """
    Creates a 9x9 nested list of integers from a string with 81 separate values.

    :param str data: A string with 81 separate integer values, [0-9].
    :return: The Sudoku puzzle as a 9x9 nested list of integers.
    :rtype: list[list[int]]
    """
    row_list = []
    row = []
    i = 1
    data = data.split()
    for num in data:
        row.append(int(num))
        if i == 9:
            i = 0
            row_list.append(row)
            row = []
        i += 1
    return row_list


# TODO 4a: Implement the open_sudoku function as described
def open_sudoku(filename):
    """
    Opens the given file, parses the contents, and returns a Sudoku puzzle.

    This function prints to the console any comment lines in the file.

    :param str filename: The file name.
    :return: The Sudoku puzzle as a 9x9 nested list of integers.
    :rtype: list[list[int]]
    """
    row = []
    puzzle = []
    numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    with open(filename) as data_file:  # Get the file data
        print(filename)
        for line in data_file:
            chars = line.split()
            if chars[0] in numbers:  # If there are other characters as the first line, it will ignore them.
                i = 1
                for num in chars:
                    row.append(int(num))  # Add each consecutive value (int) to the row
                    if i % 9 == 0:
                        puzzle.append(row)  # Add each row to the overall puzzle
                        row = []
                    i += 1
            else:
                line = line.strip("#")
                print(line)
            if row != []:
                puzzle.append(row)
                row = []
    return puzzle


# TODO 5a: Implement the is_solved function as described
def is_solved(puzzle):
    """
    Determines if a Sudoku puzzle is valid and complete.

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: True if the puzzle is valid and complete; False otherwise.
    :rtype: bool
    """
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]  # numbers is the list of valid values
    for row in puzzle:
        for i in range(1,10):
            if i not in row:
                return False
    for c in range(9):
        col = []
        for r in range(9):
            col.append(puzzle[r][c])
        for j in range(1,10):
            if j not in col:
                return False
    box_coords_x = [0, 3, 6]
    box_coords_y = [0, 3, 6]
    for x in box_coords_x:
        for y in box_coords_y:
            box = []
            for i in range(3):
                box.append(puzzle[y + i][x])
                box.append(puzzle[y + i][x + 1])
                box.append(puzzle[y + i][x + 2])
            for j in range(1,10):
                if j not in box:
                    return False
    return True


# TODO 6a: Implement the is_valid function as described
def is_valid(puzzle):
    """
    Determines if a Sudoku puzzle is valid, but not necessarily complete.

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: True if the puzzle is valid; False otherwise.
    :rtype: bool
    """
    print(puzzle)
    for row in puzzle:
        for char in row:
            if char != 0:
                if row.count(char) > 1:
                    return False
    for c in range(9): # This loop goes through each column, making sure there are no more than one of each variable.
        col = []
        for r in range(9):
            col.append(puzzle[r][c])
        for char in col:
            if char != 0:
                if col.count(char) > 1:
                    return False

    box_coords_x = [0, 3, 6] # The upper left coordinate of each box
    box_coords_y = [0, 3, 6]
    for x in box_coords_x:
        for y in box_coords_y: # This loop goes through each box looking for repeat values
            box = []
            for i in range(3):
                box.append(puzzle[y + i][x])
                box.append(puzzle[y + i][x + 1])
                box.append(puzzle[y + i][x + 2])
            for char in box:
                if char != 0:
                    if box.count(char) > 1:
                        return False

    return True


# TODO 8: Implement the solve_sudoku function as discussed in class
def solve_sudoku(puzzle):
    """
    Recursive function to solve a Sudoku puzzle with brute force.

    Note: This function DOES modify the puzzle!!!

    :param list[list[int]] puzzle:  The 9x9 nested list of integers.
    :return: None
    """
    return None


"""
The following puzzles in list and string form are provided to help you
test and demonstrate your code.
"""

PUZZLE = [[1, 8, 0, 6, 0, 9, 0, 5, 7],
          [5, 0, 0, 0, 0, 0, 0, 0, 3],
          [0, 0, 2, 0, 8, 0, 4, 0, 0],
          [7, 0, 0, 8, 4, 5, 0, 0, 1],
          [0, 0, 3, 2, 0, 1, 9, 0, 0],
          [2, 0, 0, 9, 6, 3, 0, 0, 5],
          [0, 0, 1, 0, 5, 0, 8, 0, 0],
          [8, 0, 0, 0, 0, 0, 0, 0, 4],
          [9, 4, 0, 3, 0, 8, 0, 7, 2]]

PUZZLE_SOLVED = [[1, 8, 4, 6, 3, 9, 2, 5, 7],
                 [5, 9, 7, 1, 2, 4, 6, 8, 3],
                 [6, 3, 2, 5, 8, 7, 4, 1, 9],
                 [7, 6, 9, 8, 4, 5, 3, 2, 1],
                 [4, 5, 3, 2, 7, 1, 9, 6, 8],
                 [2, 1, 8, 9, 6, 3, 7, 4, 5],
                 [3, 7, 1, 4, 5, 2, 8, 9, 6],
                 [8, 2, 5, 7, 9, 6, 1, 3, 4],
                 [9, 4, 6, 3, 1, 8, 5, 7, 2]]

PUZZLE_INVALID = [[1, 8, 0, 6, 3, 9, 0, 5, 7],
                  [5, 0, 0, 0, 0, 0, 0, 0, 3],
                  [0, 0, 2, 0, 8, 0, 4, 0, 0],
                  [7, 0, 0, 8, 4, 5, 0, 0, 1],
                  [0, 0, 3, 2, 3, 1, 9, 0, 0],
                  [2, 0, 0, 9, 6, 3, 0, 0, 5],
                  [0, 0, 1, 0, 5, 0, 8, 0, 0],
                  [8, 0, 0, 0, 0, 0, 0, 0, 4],
                  [9, 4, 0, 3, 0, 8, 0, 7, 2]]

DATA = """1 8 0 6 0 9 0 5 7
          5 0 0 0 0 0 0 0 3
          0 0 2 0 8 0 4 0 0
          7 0 0 8 4 5 0 0 1
          0 0 3 2 0 1 9 0 0
          2 0 0 9 6 3 0 0 5
          0 0 1 0 5 0 8 0 0
          8 0 0 0 0 0 0 0 4
          9 4 0 3 0 8 0 7 2"""

DATA_SOLVED = """1 8 4 6 3 9 2 5 7
                 5 9 7 1 2 4 6 8 3
                 6 3 2 5 8 7 4 1 9
                 7 6 9 8 4 5 3 2 1
                 4 5 3 2 7 1 9 6 8
                 2 1 8 9 6 3 7 4 5
                 3 7 1 4 5 2 8 9 6
                 8 2 5 7 9 6 1 3 4
                 9 4 6 3 1 8 5 7 2"""

# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# fec1fd438decfb225824c85c69f1ec3a37fec3cc3608d3c4a7cf1da39a740e00 poergperj
