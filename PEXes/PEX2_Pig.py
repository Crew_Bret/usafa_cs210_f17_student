#!/usr/bin/env python3
"""
In this programming exercise I will write a program to implement the game Pig. This is a two-player
game played with a single six-sided die with players alternating turns:
CS 210, Introduction to Programming
"""

import easygui
import random
import turtle

__author__ = "C3C Bret Crew"  # Your name. Ex: John Doe
__section__ = "M6"  # Your section. Ex: M1
__instructor__ = "Dr. Bower"  # Your instructor. Ex: Lt Col Doe
__date__ = "12 Sep 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """No outside help was received in the creation of this program, and I collaborated with no one."""  # Multiple lines OK with triple quotes

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.


def main():
    # Create the turtle screen and two turtles (leave this as the first line of main).
    screen, artist, writer = turtle_setup()
    # TODO: Implement your main program here.
    screen.bgcolor("darkgreen")
    player_one_score = 0
    player_two_score = 0
    while player_one_score < 100 and player_two_score < 100:
        player_one_score = player_one_score + take_one_turn(artist, 1, player_one_score)
        screen_refresh(writer, player_one_score, player_two_score)
        if player_two_score < 100 and player_one_score < 100:
            player_two_score = player_two_score + take_one_turn(artist, 2, player_two_score)
            screen_refresh(writer, player_one_score, player_two_score)
    screen_refresh(writer, player_one_score, player_two_score)  # If the code reaches this point, the game is over.
    writer.goto(0, 0)
    if player_one_score > player_two_score:  # Determine which player won.
        player = 1
    else:
        player = 2
    writer.write("GAME OVER: Player {} Wins!".format(player), align="center", font=("Ariel", 18, "normal"))
    screen.exitonclick()


def screen_refresh(writer, player_one_score, player_two_score):
    """ Refreshes the screen, clearing previous scores and die rolls.

    :param Turtle writer: a master with the pen...
    :param int player_one_score: The score belonging to player one
    :param int player_two_score: The score belonging to player two
    :return: nada
    """
    writer.color("darkgreen")
    writer.down()
    writer.goto(-WIDTH / 2, -HEIGHT / 2)
    writer.begin_fill()
    for i in range(4):  # Draws a big green box over all graphics in the window.
        writer.forward(WIDTH)
        writer.right(90)
    writer.end_fill()
    writer.up()
    writer.color("white")  # This code re-draws the current game score.
    writer.goto(-1 / 2 * WIDTH + MARGIN, -1 / 2 * HEIGHT + MARGIN)
    writer.write("Player 1: {}".format(player_one_score), font=("Arial", 14, "normal"))
    writer.goto(1 / 2 * WIDTH - MARGIN - 130, -1 / 2 * HEIGHT + MARGIN)
    writer.write("Player 2: {}".format(player_two_score), font=("Arial", 14, "normal"))


def take_one_turn(artist, player, score):
    """ Takes one turn from a player. Calls the draw_die function for each roll of the die until the player holds or
        a 1 is rolled.

    :param Turtle artist: one artsy shelly boi
    :param int player: player number (1 or 2)
    :param int score: the score of the player whose turn it is
    :return:
    """
    # TODO: Implement your take_one_turn function here.
    x = -1 / 2 * WIDTH - WIDTH / 12
    y = 1 / 2 * HEIGHT - MARGIN
    turn_score = 0  # Score earned by a specific player this turn.
    choice = "Roll"  # Decision to roll or hold, set to roll by default for first roll.
    die_in_row = 0  # Keeps track of the die in each row.
    row = 0
    while choice == "Roll" and turn_score + score < 100:  # Ends the game mid-turn if win condition is reached.
        num = random.randint(1, 6)  # Range on the die.
        die_in_row = die_in_row + 1
        if die_in_row > 8:  # New line for each 8 die, as specified.
            row = row + 1
            x = -1 / 2 * WIDTH + MARGIN
            y = 1 / 2 * HEIGHT - (row + 1) * MARGIN - row * WIDTH / 12
            die_in_row = 1
        else:
            x = x + MARGIN + WIDTH / 12

        draw_die(artist, num, x, y)  # Draw a die in the location determined above.
        if num == 1:
            turn_score = 0
            choice = "Pig"  # "Pig" is neither "Roll" nor "Hold". Therefore it exits the while loop and awards no points.
            easygui.msgbox(msg="Sorry, player {}, looks like your turn is over!".format(player))
        else:
            turn_score = turn_score + num

            choice = easygui.buttonbox(
                msg="Player {}: You have earned {} points this turn. Roll or hold?:".format(player, turn_score),
                title="Roll?", choices=["Roll", "Hold"])
    if choice == "Hold":
        easygui.msgbox(msg="Player {}: You earned {} points this turn.".format(player, turn_score))

    return turn_score


def draw_die(tom, pips, x, y):
    """ Draws an individual die at specified x-y coordinates

    :param Turtle tom: my lil' turtle
    :param int pips: int for # of pips
    :param float x: x-position
    :param float y: y-position
    :return:
    """
    # TODO: Implement your draw_die function here.
    tom.up()
    tom.goto(x, y)
    tom.down()
    tom.color("white")
    tom.begin_fill()
    SIDE = WIDTH / 12  # Side is a constant, but no need for it to be global. PEP 8 has an issue with all-caps.
    for i in range(4):
        tom.forward(SIDE)
        tom.right(90)
    tom.end_fill()
    tom.up()
    tom.color("black")
    if pips > 1:
        draw_pip(tom, int(x + 16), int(y - 24))  # top left pip
        draw_pip(tom, int(x + SIDE - 16), int(y - SIDE + 8))  # bottom right pip

    if pips > 3:
        draw_pip(tom, int(x + SIDE - 16), int(y - 24))  # top right pip
        draw_pip(tom, int(x + 16), int(y - SIDE + 8))  # bottom left pip
    if pips == 1 or pips == 3 or pips == 5:
        draw_pip(tom, int(x + 1 / 2 * SIDE), int(y - 1 / 2 * SIDE - 8))  # center pip
    if pips == 6:
        draw_pip(tom, int(x + 16), int(y - 1 / 2 * SIDE - 8))  # center left pip
        draw_pip(tom, int(x + SIDE - 16), int(y - 1 / 2 * SIDE - 8))  # center right pip


def draw_pip(tom, x, y):
    """ Draws the pips on each die. Separated from the draw_die function for simplicity

    :param Turtle tom: one shelly boi
    :param int x: x-pos
    :param int y: y-pos
    :return:
    """
    tom.goto(x, y)
    tom.down()
    tom.begin_fill()
    tom.circle(8)
    tom.end_fill()
    tom.up()


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    b = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    try:
        import base64

        eval(compile(base64.b64decode(b), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# fec1fd438decfb225824c85c69f1ec3a37fec3cc3608d3c4a7cf1da39a740e00 poergperj
