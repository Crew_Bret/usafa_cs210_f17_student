Programming Exercises (PEXes) should be stored in this folder named like so:

    PEX1.py
    PEX2.py

If multiple files are required for the PEX, create a folder with the PEX name and number, and put PEX files within.  The files within the PEX# folder can have any name:

    PEX3/
        PEX3_main.py
        extra_functions.py
        some_data.txt
        etc
