#!/usr/bin/env python3
"""
Tic, Tac, Toe. Gimme an X, gimme an O, gimme a three in a row.
CS 210, Introduction to Programming
"""

__author__ = "Bret Crew"  # Your name. Ex: John Doe
__section__ = "M6"  # Your section. Ex: M1
__instructor__ = "Dr. Bower"  # Your instructor. Ex: Lt Col Doe
__date__ = "7 Nov 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """ C3C Mendenhall and I talked through our main() functions
 on 13 Nov 2017, coming to the conclusion that a While loop 
 within a while loop would do the trick to keep track of the games"""  # Multiple lines OK with triple quotes
import easygui  # for inputs


def main():
    player_one = Player("player1", "X")
    player_one.name = easygui.enterbox("Player 1: What shall I call you?")
    player_one.marker = easygui.enterbox("One character callsign?")
    player_two = Player("player2", "O")
    player_two.name = easygui.enterbox("Player 2: What shall I call you?")
    player_two.marker = easygui.enterbox("One character callsign?")
    print("Best two out of three! Begin!")
    set_over = False
    games = 0
    my_tic = TicTacToe(player_one, player_two)

    while set_over == False:
        print(board_string(my_tic))
        games += 1
        while games <= 2 and my_tic.current_player.wins < 2 and my_tic.other_player.wins < 2:

            selection = easygui.integerbox("What's your move, {}?".format(my_tic.current_player.name), "", None, 1, 10)
            row = (
                  selection - 1) // 3 + 1  # The following code converts a position presented by the user into rows and columns
            if selection % 3 == 0:
                col = 3
            elif selection == 8 or selection == 5 or selection == 2:
                col = 2
            else:
                col = 1
            my_tic.play_move(row, col)  # plays the move at the specified location.
            print(board_string(my_tic))  # prints the new board.
            if check_for_win(my_tic):  # if true, there is a winner and the game is over.
                my_tic = TicTacToe(player_one, player_two)  # resets the game.
                print(board_string(my_tic))
        if my_tic.current_player.wins > my_tic.other_player.wins: # This code determines an ultimate winner or declares a tie.
            print("{} is the ultimate champion!!!".format(my_tic.current_player.name))
        if my_tic.current_player.wins < my_tic.other_player.wins:
            print("{} is the ultimate champion!!!".format(my_tic.other_player.name))
        if my_tic.current_player.wins == my_tic.other_player.wins:
            print("Tie Game!!!".format(my_tic.current_player.name))
        print(my_tic.current_player)
        print(my_tic.other_player)
        set_over = True


def check_for_win(board):
    """

    :param TicTacToe board:
    :return:
    """
    for row in board.game_board:
        if row[0] == row[1] == row[2]:
            declare_win(board)
            return True

    for column in range(3):
        if board.game_board[0][column] == board.game_board[1][column] == board.game_board[2][column]:
            declare_win(board)
            return True
    if board.game_board[0][0] == board.game_board[1][1] == board.game_board[2][2]:
        declare_win(board)
        return True
    if board.game_board[0][2] == board.game_board[1][1] == board.game_board[2][0]:
        declare_win(board)
        return True
    if board.turns_played >= 9:
        board.current_player.record_tie()
        board.other_player.record_tie()
        print("This one's a tie!")
        return True


def declare_win(board):
    """
    Used to declare a winner for each game.
    :param board:
    :return:
    """
    board.other_player.record_win()
    board.current_player.record_loss()
    print(board.current_player)
    print(board.other_player)
    print("{} Wins!".format(board.other_player.name))


def board_string(board):
    """
    Turns the list of lists into a useable string and prints. Unit Test doesn't agree for some reason...
    :param board:
    :return:
    """
    """

    :param TicTacToe board:
    :return:
    """
    board_string = ""
    for i in range(1, 4):
        for j in range(1, 4):
            board_string = board_string + str(board.game_board[i - 1][j - 1])
            if j <= 2:
                board_string += " "
        board_string = board_string + "\n"
    return board_string


class TicTacToe:
    """ A Tic Tac Toe game model """

    def __init__(self, player1, player2):
        """
        Initializes a new TicTacToe game.
        :param Player player1: the first player to take a turn
        :param Player player2: the second player to take a turn
        """
        self.__current_player = player1
        self.__other_player = player2
        self.__player1 = player1
        self.__player2 = player2
        self.__players = (player1, player2)
        self.__winner = None  # read only as per instruction, but therefor pointless.
        self.__loser = None  # read only as per instructions, but therefor pointless.
        self.__turns_played = 0
        self.__game_board = [[1, 2, 3], [4, 5, 6],
                             [7, 8, 9]]  # Uses a list of lists to keep better track of rows and columns

    @property
    def turns_played(self):
        return self.__turns_played

    @property
    def loser(self):
        return self.__loser

    @property
    def winner(self):
        return self.__winner

    @property
    def game_board(self):
        return self.__game_board

    @game_board.setter
    def game_board(self, new_board):
        """
        Sets the game board.
        :param new_board:
        :return:
        """
        self.__game_board = new_board

    @property
    def other_player(self):
        return self.__other_player

    @property
    def current_player(self):
        return self.__current_player

    def play_move(self, row, col):
        """
        Executes a move in the game
        :param row:
        :param col:
        :return:
        """
        self.game_board[row - 1][col - 1] = self.__current_player.marker
        self.__turns_played += 1
        if self.__current_player == self.__player1:
            self.__current_player = self.__player2
        else:
            self.__current_player = self.__player1
        if self.__current_player == self.__player1:
            self.__other_player = self.__player2
        else:
            self.__other_player = self.__player1

    def player_at(self, row, col):
        """
        Determines which player, if any, is at a specified location.
        :param row:
        :param col:
        :return:
        """
        if self.game_board[row - 1][col - 1] == self.__player1.marker:
            return self.__player1
        elif self.game_board[row - 1][col - 1] == self.__player2.marker:
            return self.__player2
        else:
            return None


class Player:
    """ A player in the Tic Tac Toe game. """

    def __init__(self, name, marker):
        """
        Initializes a new Player with a name and a marker.
        :param str name: the player's name
        :param str marker: the marker to use on the board
        """
        self.__name = name
        self.marker = marker
        self.__wins = 0
        self.__losses = 0
        self.__ties = 0
        self.__games_played = 0

    @property
    def wins(self):
        return self.__wins

    @property
    def games_played(self):
        return self.__games_played

    @property
    def ties(self):
        return self.__ties

    @property
    def losses(self):
        return self.__losses

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, new_name):
        """
        Set the name.
        :param self:
        :param str new_name:
        """
        self.__name = new_name

    @property
    def marker(self):
        return self.__marker

    @marker.setter
    def marker(self, new_marker):
        """
        Set the marker only if it meets criteria.
        :param str new_marker:

        """
        if new_marker is not None:

            if len(new_marker) == 1:
                self.__marker = new_marker
            elif new_marker == "":
                self.__marker = "!"
            elif len(new_marker) > 1:
                self.__marker = new_marker[0]
        else:
            self.__marker = "!"

    def record_loss(self):
        self.__losses += 1
        self.__games_played += 1

    def record_win(self):
        self.__wins += 1
        self.__games_played += 1

    def record_tie(self):
        self.__ties += 1
        self.__games_played += 1

    def __str__(self):
        return "{}: {}, {} wins, {} losses, {} ties".format(self.__name, self.marker, self.__wins, self.__losses,
                                                            self.__ties)


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# fec1fd438decfb225824c85c69f1ec3a37fec3cc3608d3c4a7cf1da39a740e00 poergperj
