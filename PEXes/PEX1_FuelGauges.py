#!/usr/bin/env python3
"""
PEX 1
CS 210, Introduction to Programming
"""

import turtle
import math
# import formatter
__author__ = "C3C Bret Crew"  # Your name. Ex: John Doe
__section__ = "M6A"  # Your section. Ex: M1
__instructor__ = "Dr. Bower"  # Your instructor. Ex: Lt Col Doe
__date__ = "29 August 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """ None """  # Multiple lines OK with triple quotes

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.
SIDE = (WIDTH-4*MARGIN)*1/3


def main():
    # Create the turtle screen and two turtles (leave this as the first line of main).
    screen, artist, writer = turtle_setup()
    fill = screen.numinput("Fuel percentage", "What is the current fuel percentage?", 75, minval=50, maxval=100)
    # TODO: Replace this hello world code with your code
    # Draw a circle and say hello (remove these lines and implement your own turtle code here).
    writer.write("Fill Percentage: {:.1f}%".format(fill), align="center", font=("Arial", 16, "normal"))
    artist.up()
    artist.goto(WIDTH*(-1/2)+MARGIN, HEIGHT/4*-1)
    drawtriangle(artist, fill)
    artist.forward(SIDE+MARGIN)
    drawsquare(artist, fill)
    artist.forward(SIDE*(3/2)+MARGIN)
    artist.down()
    artist.circle(SIDE/2)
    artist.up()
    drawline(artist, fill)
    drawfill(artist, fill)

    # Wait for the user to click before closing the window (leave this as the last line of main).
    screen.exitonclick()


def drawfill(t, fill):

    # for the triangle area
    t.goto(WIDTH*(-1/2)+MARGIN+1/2*SIDE, (HEIGHT/4*-1)-MARGIN)
    area = (SIDE*SIDE/2-(((100-fill)/100)*SIDE*((100-fill)/100)*SIDE)/2) # triangle
    t.write("{:,.2f}".format(area), align="center", font=("Arial", 16, "normal"))

    # for the square area
    t.goto(0, (HEIGHT/4*-1)-MARGIN)
    area = (SIDE*SIDE*(fill/100))
    t.write("{:,.2f}".format(area), align="center", font=("Arial", 16, "normal"))

    # for the circle
    a = SIDE / 2 - ((100 - fill) / 100) * SIDE
    c = SIDE / 2
    b = math.sqrt(c * c - a * a)

    if a == 0:
        theta = math.pi/2
    else:
        theta = 2*math.atan(b/a)
    sector = math.degrees(theta/360)*math.pi*(SIDE/2)*(SIDE/2)
    segment = sector-(a*b)
    area = math.pi*(SIDE/2)*(SIDE/2)-segment

    t.goto(MARGIN+SIDE,(HEIGHT/4*-1)-MARGIN)
    t.write("{:,.2f}".format(area), align="center", font=("Arial", 16, "normal"))


def drawsquare(t, fill):
    t.down()
    for i in range(4):
        t.forward(SIDE)
        t.left(90)
    t.up()


def drawtriangle(t, fill):
    t.down()
    t.forward(SIDE)
    t.left(135)
    t.forward(math.sqrt(SIDE*SIDE + SIDE*SIDE))
    t.left(135)
    t.forward(SIDE)
    t.left(90)
    t.up()


def drawline(t, fill):
    t.color("red")
    t.goto(WIDTH*(-1/2)+MARGIN,HEIGHT/(4*-1)+(fill/100)*SIDE)
    t.down()
    t.forward(((100-fill)/100)*SIDE)
    t.up()
    t.forward((fill/100)*SIDE+MARGIN)
    t.down()
    t.forward(SIDE)
    t.up()
    a = SIDE/2-((100-fill)/100)*SIDE
    c = SIDE/2
    b = math.sqrt(c*c-a*a)

    t.forward((MARGIN+SIDE/2)-b)
    t.down()
    t.forward(2*b)
    t.color("black")
    t.up()

# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.goto(0, SIDE/2+MARGIN)  # Centered at top of the screen.

    return screen, artist, writer


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    b = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    try:
        import base64

        eval(compile(base64.b64decode(b), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# fec1fd438decfb225824c85c69f1ec3a37fec3cc3608d3c4a7cf1da39a740e00 poergperj
