# CS210 Introduction to Programming

Fall 2017

United States Air Force Academy

## Purpose

The purpose of this source code repository is for students to have a
standardized structure for working on files in the course and to aid
the instructors in collecting student work.
