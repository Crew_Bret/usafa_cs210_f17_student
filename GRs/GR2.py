#!/usr/bin/env python3
"""
GR2, CS210, Fall 2017
"""
import os
import random
import string
import easygui

# Fill in the metadata
__author__ = "Bret Crew"  # Your name. Ex: John Doe
__section__ = "M6"  # Your section. Ex: M1
__version__ = "A"  # Test version. Ex: A
__date__ = "26 Oct 2017"  # Today's date. Ex: 25 Dec 2017

""" The following code segments are provided for your reference.
    You are free to copy/paste them into your answers below.

    # Use an easygui.fileopenbox to get a filename from the user.
    filename = easygui.fileopenbox(default="../Data/*.txt")

    # Read the entire contents of a file into a single string.
    with open(filename, "r") as data_file:
        data_string = data_file.read()

    # Read the entire contents of a file into a list of strings, one per word.
    with open(filename, "r") as data_file:
        data_words = data_file.read().split()

    # Read the entire contents of a file into a list of strings, one per line.
    with open(filename, "r") as data_file:
        data_lines = data_file.read().splitlines()

    # Split a filename into its base and extension; build a new filename.
    base, ext = os.path.splitext(filename)
    output_filename = "{}_Output{}".format(base, ext)

    # Write a string to a file.
    with open(output_filename, "w") as output_file:
        output_file.write( "This is the output.\n" )
"""


# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    # problem1()
    # problem2()
    # problem3()
    # problem4()
    # problem5()
    problem6()


# ======================================================================
# Problem 1                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem1():
    """Problem 1 from the GR."""
    print_problem_name()

    # TODO 1b
    ticket = generate_lotto(24)
    winner = generate_lotto(24)
    count = 0
    for num in ticket:
        count += winner.count(num)
    print("Ticket = {}".format(ticket))
    print("Winner = {}".format(winner))
    print("Count = {}".format(count))


# TODO 1a
def generate_lotto(max):
    nums = []
    while len(nums) <= 6:
        new_num = random.randint(1, max)
        if new_num not in nums:
            nums.append(new_num)
    nums.sort()
    return nums


# ======================================================================
# Problem 2                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem2():
    """Problem 2 from the GR."""
    print_problem_name()

    board1 = [list("aa.bocc"),
              list("aa.bbcc"),
              list("qq.abb."),
              list("qqcaabb"),
              list("occaabb"),
              list("ccaacoo")]

    board2 = [list("q.c.oo.qb."),
              list("baa.boccbb"),
              list("baa.bbccba"),
              list("aqq.abb.ab"),
              list("aqqcaabbcq"),
              list("c.ccaabbcc"),
              list("cccaacooqc")]

    # TODO 2b
    print("Board 1, a: ", count_pattern(board1, "a"))
    print("Board 1, b: ", count_pattern(board1, "b"))
    print("Board 1, c: ", count_pattern(board1, "c"))

    print("Board 2, a: ", count_pattern(board2, "a"))
    print("Board 2, b: ", count_pattern(board2, "b"))
    print("Board 2, c: ", count_pattern(board2, "c"))


# TODO 2a
def count_pattern(board, letter):
    count = 0
    for i in range(len(board)-1):
        for j in range(len(board[0])-1):
            if board[i][j] == letter and board[i+1][j] == letter and board[i+1][j+1] == letter:
                count += 1
    return count


# ======================================================================
# Problem 3                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem3():
    """Problem 3 from the GR."""
    print_problem_name()

    # TODO 3b
    filename = easygui.fileopenbox(default="../Data/*.txt")
    base, ext = os.path.splitext(filename)
    output_filename = "{}_madlib{}".format(base, ext)
    nouns = ["sister", "book", "rabbit", "pictures"]
    convert_to_madlib(filename, output_filename, nouns)

# TODO 3a
def convert_to_madlib(orig_file, new_file, noun_list):

    with open(orig_file, "r") as data_file:
        data_words = data_file.read().split()
        print(data_words)
        for i in range(len(data_words)):
            if data_words[i] in noun_list:
                data_words[i] = "_noun_"
            if data_words[i][-1:-3] == "ing":
                data_words[i] = "_verb_ing_"
        print(data_words)
    str = ""
    for word in data_words:
        str += word
        str += " "

    with open(new_file, "w") as output_file:
        output_file.write(str)
        print(str)


# ======================================================================
# Problem 4                                                 ____ / 5 pts
# Grades: All or nothing
# ======================================================================
def problem4():
    """Problem 4 from the GR."""
    print_problem_name()

    # TODO 4
    answer = " G E A B F C D"  # Put your answer here, eg, "L M N O P"
    print("Sequence:", answer)


# ======================================================================
# Problem 5                                                ____ / 20 pts
# Grades: A: 20, B: 16, C: 14, D: 12, F: 10
# ======================================================================
def problem5():
    """Problem 5 from the GR."""
    print_problem_name()

    # TODO 5b
    if recursive_extract_vowels("apepipopu") == "aeiou":
        print("True")
    else:
        print("nope")

# TODO 5a
def recursive_extract_vowels(str):
    vowels = ["a", "e", "i", "o", "u"]
    my_vowels = ""
    if str != "":
        if str[-1] in vowels:
            my_vowels += recursive_extract_vowels(str[0:-1])
            return str[-1]




# ======================================================================
# Problem 6                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem6():
    """Problem 6 from the GR."""
    print_problem_name()

    # TODO 6c
    updates = load_updates("../Data/update_inventory.txt")
    inventory = {"apples": 1000, "grapes": 700}
    print("Before:", inventory)
    update_inventory(inventory, updates)
    print("After:", inventory)
# TODO 6b
def update_inventory(dic, update_list):
    for update in (update_list):
        print(update)



# TODO 6a
def load_updates(filename):
    with open(filename, "r") as data_file:
        data_lines = data_file.read().splitlines()
        list_tuples = []
        for line in data_lines:
            data_rows = line.split()
            list_tuples += tuple(data_rows)
    return list_tuples

# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()

# fec1fd438decfb225824c85c69f1ec3a37fec3cc3608d3c4a7cf1da39a740e00 poergperj
