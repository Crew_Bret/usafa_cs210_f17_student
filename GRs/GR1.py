#!/usr/bin/env python3
"""
GR1, CS210, Fall 2017
"""
import math
import random
import easygui
import turtle

# Fill in the metadata
__author__ = "C3C Bret Crew"  # Your name. Ex: John Doe
__section__ = "M6A"  # Your section. Ex: M1
__version__ = ""  # Test version. Ex: A
__date__ = "19 Sep 2017"  # Today's date. Ex: 25 Dec 2017

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, non-animated turtle movement.

""" The following code segments are provided for your reference.
    You are free to copy/paste them into your answers below.

    # Use the writing turtle to write a message, centered at its location, in a bold font.
    writer.write( "Hello, World!", align="center", font=( "Courier", FONT_SIZE, "bold" ) )

    # An easygui message box to display a formatted string.
    easygui.msgbox( "pi = {:.2f}".format( 22 / 7 ), "Result" )

    # An easygui enter box for entering a string.
    s = easygui.enterbox( "Enter a string:", "Input" )

    # An easygui integer box for entering a positive integer.
    n = easygui.integerbox( "Enter a positive integer:", "Input", 42, 1, 2 ** 31 )

    # Read the entire contents of a file into a single string.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_string = data_file.read()

    # Read the entire contents of a file into a list of strings, one per word.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_words = data_file.read().split()

    # Read the entire contents of a file into a list of strings, one per line.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_lines = data_file.read().splitlines()
"""


# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    # problem1()
    # problem2()
    # problem3()
    # problem4()
    problem5()


# ======================================================================
# Problem 1                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem1():
    """Problem 1 from the GR."""
    print_problem_name()
    # TODO 1b
    print("Hands   Feet")
    print("=====   =====")
    for hands in range(10, 21, 1):
        feet = float(hands_to_feet(hands))
        print("{}".format(hands), "     {:.2f}".format(feet))


# TODO 1a
def hands_to_feet(hands):
    feet = hands / 3
    return feet


# ======================================================================
# Problem 2                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem2():
    """Problem 2 from the GR."""
    print_problem_name()

    # TODO 2b
    num_rolls = easygui.integerbox("How many times (1-1000 should I roll the two dice?", "Input", None, 1, 1000)
    target = easygui.integerbox("What number (2-12), are we hoping for?", "Input", None, 2, 12)
    count = count_dice(num_rolls, target)
    easygui.msgbox("In {} rolls, a {} came up {} times".format(num_rolls, target, count))


# TODO 2a
def count_dice(num_rolls, target):
    count = 0
    for i in range(num_rolls):
        die_one = random.randint(1, 6)
        die_two = random.randint(1, 6)
        if die_one + die_two == target:
            count += 1
    return count


# ======================================================================
# Problem 3                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# Including 5 pts for good docstring
# ======================================================================
def problem3():
    """Problem 3 from the GR."""
    print_problem_name()

    # TODO 3b
    num1 = easygui.integerbox("Choose 4 numbers to find the greatest common divisor.", None, 42)
    num2 = easygui.integerbox("Choose 4 numbers to find the greatest common divisor.", None, 28)
    num3 = easygui.integerbox("Choose 4 numbers to find the greatest common divisor.", None, 63)
    num4 = easygui.integerbox("Choose 4 numbers to find the greatest common divisor.", None, 84)
    divisor1 = gcd(num1, num2)
    divisor2 = gcd(num3, num4)
    final_divisor = gcd(divisor1, divisor2)
    easygui.msgbox("The greatest common divisor of {}, {}, {}, and {} is {} "
                   .format(num1, num2, num3, num4, final_divisor))


# TODO 3a
def gcd(num1, num2):
    """ This function will take two integers and return an integer that is their greatest
    common divisor.
    :param int num1: The first integer for consideration.
    :param int num2: The second integer for consideration.
    :return: This function will return the greatest common divisor of the two integers gi    :rtype: int
    """
    while num1 != num2:
        if num1 > num2:
            num1 = num1 - num2
        else:
            num2 = num2 - num1
    return num1


# ======================================================================
# Problem 4                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem4():
    """Problem 4 from the GR."""
    print_problem_name()

    # Reading in a file followed by split() creates a list of words
    # such as you have used in labs. For example:
    # data_words = ["It", "was", "the", "worst", "of", "times"]
    with open(easygui.fileopenbox(default="../Data/*.txt")) as data_file:
        data_words = data_file.read().split()

    # TODO 4b (no longer indented under the with statement above)
    he_count = count_word("he", data_words)
    him_count = count_word("him", data_words)
    male_count = he_count + him_count
    she_count = count_word("she", data_words)
    her_count = count_word("her", data_words)
    # Variables above were added for simplicity of math in the following comparisons.
    female_count = she_count + her_count
    if male_count > female_count:
        easygui.msgbox("There are more male than female pronouns.")
    elif female_count > male_count:
        easygui.msgbox("There are more female than male pronouns.")
    else:
        easygui.msgbox("There are equal female and male pronouns")


# TODO 4a
def count_word(my_word, data_words):
    """
    :param str word:
    :param list word_list:
    :return: return the number of times the specified word appears.
    :rtype: int
    """
    count = 0
    for word in data_words:
        if word == my_word:
            count += 1
    return count


# ======================================================================
# Problem 5                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem5():
    """Problem 6 from the GR."""
    print_problem_name()

    # Create the turtle screen and two turtles
    screen, artist, writer = turtle_setup()

    # TODO 5
    big_r = HEIGHT / 2 - MARGIN
    lil_r = big_r / 2
    shots = 0
    misses = 0
    score = 0
    artist.up()
    artist.goto(0, -big_r)
    artist.down()
    artist.circle(big_r)
    artist.up()
    artist.goto(0, -lil_r)
    artist.down()
    artist.circle(lil_r)
    artist.up()
    while shots < 10 and misses < 7:
        x = random.randint(-WIDTH / 2, WIDTH / 2)
        y = random.randint(-HEIGHT / 2, HEIGHT / 2)
        if math.sqrt(x * x + y * y) > big_r:
            color = "red"
            misses += 1
        elif math.sqrt(x * x + y * y) > lil_r:
            color = "yellow"
            score +=1
        else:
            color = "green"
            score += 2
        artist.goto(x, y)
        artist.dot(MARGIN, color)
        shots += 1
        writer.goto(-70,-25)
    if shots == 10:
        writer.write("Shots Taken: {}\nFinal   Score: {}".format(shots, score),
                     font=("Ariel", 14, "normal"))
    else:
        writer.write("Too many misses.\nTry again.",
                     font=("Ariel", 14, "normal"))

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# fec1fd438decfb225824c85c69f1ec3a37fec3cc3608d3c4a7cf1da39a740e00 poergperj
