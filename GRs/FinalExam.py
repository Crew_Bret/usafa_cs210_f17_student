#!/usr/bin/env python3
"""
Final Exam, CS210, Fall 2017
"""

# Fill in the metadata
__author__ = "Bret Crew"  # Your name. Ex: John Doe
__section__ = "M6A"  # Your section. Ex: M1
__version__ = "A"  # Test version. Ex: A
__date__ = "12 Dec 2017"  # Today's date. Ex: 25 Dec 2017


# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    # problem1()
    # problem2()
    # problem3()
    # problem4()
    # problem5()
    problem6()
    # problem7()
    # problem8()


import easygui


# ======================================================================
# Problem 1                                                ____ / 30 pts
# ======================================================================
def problem1():
    print_problem_name()

    # TODO 1b
    print(nautical_miles_to_feet(1))
    print(nautical_miles_to_feet(200))
    print(nautical_miles_to_feet(6000))


# TODO 1a

def nautical_miles_to_feet(naut):
    """
    Converts nautical miles to feet
    :param float naut:
    :return:
    """
    answer = 0
    if naut > 347.590238 or naut < 99.93219:
        return float(answer)
    else:
        answer = naut * 6076.12
        return float(answer)


# ======================================================================
# Problem 2                                                ____ / 30 pts
# ======================================================================
def problem2():
    print_problem_name()

    # TODO 2
    filename = "../Data/alice_in_wonderland.txt"
    with open(filename) as data_file:
        data = data_file.read().splitlines()
        printed_text = []
        word_string = ""
        for line in data:
            line_words = line.split(" ")  # list
            num_of_words = len(line_words)
            key_word = ""
            for i in range(num_of_words):
                length = len(line_words[i])
                if len(line_words[i]) > len(key_word):
                    key_word = line_words[i]
            printed_text.append(key_word)
    for word in printed_text:
        word_string = word_string + "{} ".format(word)
    with open("../Data/alicewords.txt", "w") as data_file:
        print("".join(word_string), file=data_file)


# ======================================================================
# Problem 3                                                ____ / 30 pts
# ======================================================================
def problem3():
    print_problem_name()

    # TODO 3b
    test = [3, 67, 45, 23, 18, 4, 89]
    print(find_min(test, 3, 5))
    test = [42, 78, 14, 13, 85, 27]
    print(find_min(test, 2, 7))
    test = [22, 58, 12, 10]
    print(find_min(test, -1, 2))


# TODO 3a
def find_min(int_list, first, last):
    """
    Finds the minimum value in a list given an index window to search.
    :param list int_list:
    :param int first:
    :param int last:
    :return value:
    :rtype int:
    """

    if first < 0:
        first = 0
    if last > len(int_list):
        last = len(int_list) - 1
    value = 200
    for i in range(first, last + 1):
        if int_list[i] < value:
            value = int_list[i]
    return value


# ======================================================================
# Problem 4                                                ____ / 30 pts
# ======================================================================
def problem4():
    print_problem_name()

    # TODO 4c
    print_checkers(create_checkers(6))
    print()
    print()
    print_checkers(create_checkers(9))


# TODO 4a
def create_checkers(size):
    """

    :param int size:
    :return list array:
    """
    array = []
    row = []

    def make_spot(x, y):
        if y % 2 == 0:
            if x % 2 == 0:
                row.append("-")
            else:
                row.append("B")
        else:
            if (x + 1) % 2 == 0:
                row.append(("-"))
            else:
                row.append("B")

    for y in range(size):
        row = []
        for x in range(size):
            make_spot(x, y)
        # print(row)
        array.append(row)
    for i in range(3):
        for j in range(len(array[size - 1 - i])):
            if array[size - 1 - i][j] == "B":
                array[size - 1 - i][j] = "R"
    for i in range(3, size - 3):
        for j in range(len(array[size - 1 - i])):
            if array[size - 1 - i][j] == "B":
                array[size - 1 - i][j] = "-"

    return array


# TODO 4b
def print_checkers(board):
    row_string = ""
    for row in board:
        for i in range(len(row)):
            row_string = row_string + row[i] + " "
        print(row_string)
        row_string = ""


# ======================================================================
# Problem 5                                                ____ / 30 pts
# ======================================================================
def problem5():
    print_problem_name()

    # TODO 5b
    b1 = Brick(5, 5, 5)
    print(b1)
    b2 = Brick(5, 6, 7)
    print(b2)
    b3 = Brick(5, 5, 5)
    print(b3)

    print(b1.equals(b2))
    print(b3.equals(b1))


# TODO 5a
class Brick():
    def __init__(self, length, width, height):
        self.__length = length
        self.__width = width
        self.__height = height
        self.__volume = 0

    @property
    def length(self):
        return self.__length

    @property
    def width(self):
        return self.__width

    @property
    def height(self):
        return self.__height

    @length.setter
    def length(self, new_length):
        self.__length = new_length

    @width.setter
    def width(self, new_width):
        self.__width = new_width

    @height.setter
    def height(self, new_height):
        self.__height = new_height

    @property
    def volume(self):
        return self.__length * self.__width * self.__height

    def __str__(self):
        return "Length: {} \nWidth: {} \nHeight: {} \nVolume: {}".format(self.length, self.width, self.height,
                                                                         self.volume)

    def equals(self, other):
        """

        :param Brick other:
        :return:
        """
        if self.length == other.length:
            if self.width == other.width:
                if self.height == other.height:
                    return True
        return False


# ======================================================================
# Problem 6                                                ____ / 30 pts
# ======================================================================
def problem6():
    print_problem_name()

    # TODO 6b
    print(remove_duplicates("ATCG"))
    print(remove_duplicates("AAATCG"))




# TODO 6a
def remove_duplicates(dna):
    if len(dna) == 1:
        return dna
    elif dna[0] == dna[1]:
        return "{}{}".format(dna[1], remove_duplicates(dna[3:]))
    elif dna[0] != dna[1]:
        return "{}{}".format(dna[0], remove_duplicates(dna[1:]))








# ======================================================================
# Problem 7                                                ____ / 30 pts
# ======================================================================
def problem7():
    print_problem_name()

    # TODO 7
    perfect_numbers(1, 100)
    perfect_numbers(1, 10000)


def perfect_numbers(start, end):
    print("The perfect numbers between {} and {}".format(start, end))
    for i in range(start, end + 1):
        if sum(get_factors(i)) == i:
            print(i)


def get_factors(num):
    factors = []
    for i in range(1, num):
        if num % i == 0:
            factors.append(i)
    return factors


# ======================================================================
# Problem 8                                                ____ / 40 pts
# ======================================================================
def problem8():
    print_problem_name()

    # TODO 8b
    dict = (load_families("../Data/Names.txt"))
    print(dict)

    # TODO 8c
    for family in dict:
        if len(dict[family]) > 1:
            print(family)
            for person in dict[family]:
                print("     {} {}.".format(person["first_name"], person["middle_name"][0]))


# TODO 8a
def load_families(filename):
    my_dictionary = {}  # type: dict[str, list[dict[str,str]]]

    with open(filename) as data_file:
        names = data_file.read().splitlines()
        name_list = []
        for name in names:
            name_parts = name.split()
            name_dic = {"first_name": name_parts[0], "middle_name": name_parts[1], "last_name": name_parts[2]}
            name_list.append(name_dic)
        last_names = [" "]
        for dic in name_list:
            grouping_key = "last_name"
            last_names.append(dic[grouping_key])
        pure_last_names = []
        for i in range(1, len(last_names) - 1):
            if last_names[i] != last_names[i - 1]:
                pure_last_names.append(last_names[i])

        for last_name in pure_last_names:
            family = []
            for person in name_list:
                if person["last_name"] == last_name:
                    family.append(person)
            my_dictionary[last_name] = family
    return my_dictionary


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name]) or "No docstring"
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# fec1fd438decfb225824c85c69f1ec3a37fec3cc3608d3c4a7cf1da39a740e00 poergperj
